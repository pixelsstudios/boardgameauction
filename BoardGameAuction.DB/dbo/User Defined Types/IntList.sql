CREATE TYPE [dbo].[IntList] AS TABLE (
    [Value] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Value] ASC));






GO
GRANT EXECUTE
    ON TYPE::[dbo].[IntList] TO [bgaapi];

