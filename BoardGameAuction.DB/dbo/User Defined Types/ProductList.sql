CREATE TYPE [dbo].[ProductList] AS TABLE (
    [BGGId]       INT            NOT NULL,
    [VersionId]   INT            NOT NULL,
    [ParentBGGId] INT            NULL,
    [Name]        NVARCHAR (100) NOT NULL,
    [VersionName] NVARCHAR (100) NOT NULL,
    [ImageUrl]    NVARCHAR (125) NULL,
    [Year]        INT            NOT NULL,
    [Rating]      DECIMAL (3, 1) NOT NULL,
    [Weight]      DECIMAL (3, 2) NOT NULL,
    [LastChecked] DATE           NOT NULL);






GO
GRANT EXECUTE
    ON TYPE::[dbo].[ProductList] TO [bgaapi];

