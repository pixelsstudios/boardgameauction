﻿CREATE TABLE [dbo].[Auction] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Code]        NCHAR (4)      NOT NULL,
    [Start]       DATETIME2 (0)  NOT NULL,
    [End]         DATETIME2 (0)  NOT NULL,
    [Password]    NVARCHAR (100) NOT NULL,
    [Type]        SMALLINT       NOT NULL,
    [ActiveLotId] INT            NULL,
    CONSTRAINT [PK_Auction] PRIMARY KEY CLUSTERED ([Id] ASC)
);



