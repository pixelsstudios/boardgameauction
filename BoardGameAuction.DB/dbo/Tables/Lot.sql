﻿CREATE TABLE [dbo].[Lot] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [AuctionId]  INT            NOT NULL,
    [SellerId]   INT            NOT NULL,
    [StartPrice] DECIMAL (6, 2) NOT NULL,
    [EndPrice]   DECIMAL (6, 2) NOT NULL,
    [SalePrice]  DECIMAL (6, 2) NULL,
    CONSTRAINT [PK_Lot] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_LotSellerId_AccountId] FOREIGN KEY ([SellerId]) REFERENCES [dbo].[Account] ([Id])
);



