﻿CREATE TABLE [dbo].[Product] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [BGGId]       INT            NOT NULL,
    [VersionId]   INT            NOT NULL,
    [ParentBGGId] INT            NULL,
    [Name]        NVARCHAR (100) NOT NULL,
    [VersionName] NVARCHAR (100) NOT NULL,
    [ImageUrl]    NVARCHAR (125) NULL,
    [Year]        INT            NOT NULL,
    [Rating]      DECIMAL (3, 1) NOT NULL,
    [Weight]      DECIMAL (3, 2) NOT NULL,
    [LastChecked] DATE           NOT NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Product_BGGId+VersionId]
    ON [dbo].[Product]([BGGId] ASC, [VersionId] ASC);

