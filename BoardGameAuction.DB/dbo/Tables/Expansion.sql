﻿CREATE TABLE [dbo].[Expansion] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [ItemId]        INT            NOT NULL,
    [ProductId]    INT            NOT NULL,
    [Notes]         NVARCHAR (100) NULL,
    CONSTRAINT [PK_Expansion] PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_Expansion_ProductId_To_Product_Id] FOREIGN KEY ([ProductId]) REFERENCES [Product]([Id])
);



