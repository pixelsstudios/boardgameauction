﻿CREATE TABLE [dbo].[Item] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [LotId]         INT            NOT NULL,
    [ProductId]     INT            NOT NULL,
    [Notes]         NVARCHAR (100) NULL,
    CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_ProductId_To_Product_Id] FOREIGN KEY ([ProductId]) REFERENCES [Product]([Id])
);



