-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Gets a user account by name
-- =============================================
CREATE PROCEDURE [dbo].[AccountGetByName] 
	@Name NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Id], [Name], [Password] AS HashedPassword
	FROM [BGA].[dbo].[Account]
	WHERE [Name] = @Name
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[AccountGetByName] TO [bgaapi]
    AS [dbo];

