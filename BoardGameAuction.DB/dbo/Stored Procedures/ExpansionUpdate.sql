-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Updates the details of an existing expansion
-- =============================================
CREATE PROCEDURE [dbo].[ExpansionUpdate] 
	@Id INT,
	@Notes NVARCHAR(100)
AS
BEGIN
	UPDATE [Expansion]
	SET [Notes] = @Notes
	WHERE [Id] = @Id;

	SELECT SCOPE_IDENTITY()
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ExpansionUpdate] TO [bgaapi]
    AS [dbo];

