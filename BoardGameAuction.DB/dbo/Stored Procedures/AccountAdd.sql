-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Add a user account
-- =============================================
CREATE PROCEDURE AccountAdd 
	-- Add the parameters for the stored procedure here
	@Name NVARCHAR(50), 
	@PasswordHash NVARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [BGA].[dbo].[Account] ([Name], [Password])
	VALUES (@Name, @PasswordHash);
	SELECT SCOPE_IDENTITY()
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[AccountAdd] TO [bgaapi]
    AS [dbo];

