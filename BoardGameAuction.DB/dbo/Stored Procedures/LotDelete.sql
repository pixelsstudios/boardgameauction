﻿
-- =============================================
-- Author:		Red Morris
-- Create date: 2020-06-03
-- Description:	Deletes a lot
-- =============================================
CREATE PROCEDURE [dbo].[LotDelete] 
	@AuctionId INT,
	@LotId INT
AS
BEGIN
	SET NOCOUNT ON;

	DELETE TOP(1) FROM [Lot]
	WHERE [AuctionId] = @AuctionId AND [Id] = @LotId AND [SalePrice] IS NULL

	SELECT @@ROWCOUNT
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[LotDelete] TO [bgaapi]
    AS [dbo];

