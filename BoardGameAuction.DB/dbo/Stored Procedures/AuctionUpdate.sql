-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Updates an existing auction
-- =============================================
CREATE PROCEDURE [dbo].[AuctionUpdate]
	@Id INT,
	@Password NVARCHAR(100),
	@Type SMALLINT,
	@Start DATETIME2(0),
	@End DATETIME2(0)
AS
BEGIN
	SET NOCOUNT ON;

	IF @Password = '' OR @Password = NULL
		BEGIN
			UPDATE [Auction]
			SET [Type] = @Type, [Start] = @Start, [End] = @End
			WHERE [Id] = @Id
		END
	ELSE
		BEGIN
			UPDATE [Auction]
			SET [Password] = @Password, [Type] = @Type, [Start] = @Start, [End] = @End
			WHERE [Id] = @Id
		END
	
	SELECT * FROM [Auction] WHERE [Id] = @Id
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[AuctionUpdate] TO [bgaapi]
    AS [dbo];

