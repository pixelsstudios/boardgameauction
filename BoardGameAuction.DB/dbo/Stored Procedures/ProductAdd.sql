-- =============================================
-- Author:		Red Morris
-- Create date: 202-05-27
-- Description:	Inserts new Products
/*
	DECLARE @data ProductList
	INSERT INTO @data VALUES (6249, 1999238, 10004, 'Made-up expansion', 'Only Edition', '', 2020, 1.1, 3.0, SYSUTCDATETIME())
	exec ProductAdd @data
*/
-- =============================================
CREATE PROCEDURE [dbo].[ProductAdd]
	@product ProductList READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ids TABLE ([Value] int)

	INSERT INTO [Product] ([ParentBGGId], [BGGId], [VersionId], [Name], [VersionName], [ImageUrl], [Year], [Rating], [Weight], [LastChecked])
	OUTPUT inserted.BGGId INTO @ids
	SELECT paramProduct.[ParentBGGId]
		, paramProduct.[BGGId]
		, paramProduct.[VersionId]
		, paramProduct.[Name]
		, paramProduct.[VersionName]
		, paramProduct.[ImageUrl]
		, paramProduct.[Year]
		, paramProduct.[Rating]
		, paramProduct.[Weight]
		, paramProduct.[LastChecked]
	FROM @product AS paramProduct
		LEFT JOIN [Product] AS existingProduct
			ON existingProduct.[BGGId] = paramProduct.[BGGId] 
				AND existingProduct.[VersionId] = paramProduct.[VersionId]
	WHERE existingProduct.[Id] IS NULL
		AND paramProduct.[BGGId] IS NOT NULL
		AND paramProduct.[VersionId] IS NOT NULL

	SELECT [ParentBGGId]
		,[BGGId]
		,[VersionId]
		,[Name]
		,[VersionName]
		,[ImageUrl]
		,[Year]
		,[Rating]
		,[Weight]
	FROM [Product]
		LEFT JOIN @ids AS itemId ON [BGGId] = itemId.[Value]
		LEFT JOIN @ids AS parentId ON [ParentBGGId] = parentId.[Value]
	WHERE itemId.[Value] IS NOT NULL OR parentId.[Value] IS NOT NULL
	ORDER BY COALESCE([ParentBGGId], [BGGId]), [ParentBGGId], [BGGId], [VersionId], [Year]
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ProductAdd] TO [bgaapi]
    AS [dbo];

