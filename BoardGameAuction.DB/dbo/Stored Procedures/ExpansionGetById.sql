-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Gets an expansion by Id
-- =============================================
CREATE PROCEDURE [dbo].[ExpansionGetById] 
	@Id INT
AS
BEGIN
	SELECT *
	FROM [Expansion]
	WHERE [Id] = @Id;
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ExpansionGetById] TO [bgaapi]
    AS [dbo];

