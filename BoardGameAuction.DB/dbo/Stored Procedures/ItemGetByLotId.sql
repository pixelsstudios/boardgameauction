-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Gets all the items by LotId
-- =============================================
CREATE PROCEDURE [dbo].[ItemGetByLotId] 
	@LotId INT
AS
BEGIN
	SELECT [Item].[Id], 
		[Item].[LotId], 
		[Item].[Notes], 
		[Product].[Id] AS BGGId, 
		[Product].[Name], 
		[Product].[ImageUrl], 
		[Product].[Year], 
		[Product].[Rating], 
		[Product].[Weight]
	FROM [Item]
		INNER JOIN [Product] ON [Product].[Id] = [Item].[ProductId]
	WHERE [Item].[LotId] = @LotId;
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ItemGetByLotId] TO [bgaapi]
    AS [dbo];

