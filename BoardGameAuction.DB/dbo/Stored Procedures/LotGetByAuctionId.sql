-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Gets a lot by Auction Id
-- =============================================
CREATE PROCEDURE [dbo].[LotGetByAuctionId]
	@AuctionId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Lot].*, 
		seller.[Id],
		seller.[Name],
		[Item].[Id], 
		[Item].[LotId], 
		[Item].[Notes], 
		itemDetails.[Id] AS [ProductId],
		itemDetails.[BGGId],
		itemDetails.[Name], 
		itemDetails.[VersionName], 
		itemDetails.[ImageUrl], 
		itemDetails.[Year], 
		itemDetails.[Rating], 
		itemDetails.[Weight],
		[Expansion].[Id], 
		[Expansion].[ItemId], 
		[Expansion].[Notes], 
		expansionDetails.[Id] AS [ProductId],
		expansionDetails.[BGGId],
		expansionDetails.[Name], 
		expansionDetails.[VersionName], 
		expansionDetails.[ImageUrl], 
		expansionDetails.[Year], 
		expansionDetails.[Rating], 
		expansionDetails.[Weight]
	FROM [Lot]
		INNER JOIN [Account] AS seller ON seller.[Id] = [Lot].[SellerId]
		INNER JOIN [Item] ON [Item].[LotId] = [Lot].[Id]
			LEFT JOIN [Product] AS itemDetails ON itemDetails.[Id] = [Item].[ProductId]
		LEFT JOIN [Expansion] ON [Expansion].[ItemId] = [Item].[Id]
			LEFT JOIN [Product] AS expansionDetails ON expansionDetails.[Id] = [Expansion].[ProductId]
	WHERE [AuctionId] = @AuctionId
	ORDER BY [Lot].[Id], [Item].[Id], [Expansion].[Id];
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[LotGetByAuctionId] TO [bgaapi]
    AS [dbo];

