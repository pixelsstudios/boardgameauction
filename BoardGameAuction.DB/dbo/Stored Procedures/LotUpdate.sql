﻿-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Updates an existing lot
/*
	exec LotUpdate 9004, 1002, 3, 5, 4
*/
-- =============================================
CREATE PROCEDURE [dbo].[LotUpdate]
	@Id INT,
	@AuctionId INT,
	@SellerId INT,
	@StartPrice DECIMAL(6,2),
	@EndPrice DECIMAL(6,2)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [Lot]
	SET [AuctionId] = @AuctionId, [SellerId] = @SellerId, [StartPrice] = @StartPrice, [EndPrice] = @EndPrice
	WHERE [Id] = @Id
	
	SELECT @@ROWCOUNT
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[LotUpdate] TO [bgaapi]
    AS [dbo];

