-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Gets a user account by id
-- =============================================
CREATE PROCEDURE [dbo].[AccountGetById] 
	@Id INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Id], [Name], [Password] AS HashedPassword
	FROM [BGA].[dbo].[Account]
	WHERE [Id] = @Id
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[AccountGetById] TO [bgaapi]
    AS [dbo];

