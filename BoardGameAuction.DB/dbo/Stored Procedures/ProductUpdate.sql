-- =============================================
-- Author:		Red Morris
-- Create date: 202-05-27
-- Description:	Updates existing Products
-- =============================================
CREATE PROCEDURE [dbo].[ProductUpdate]
	@product ProductList READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ids TABLE ([Value] int)

	UPDATE [Product]
	SET [ParentBGGId] = paramProduct.[ParentBGGId]
		, [BGGId] = paramProduct.[BGGId]
		, [VersionId] = paramProduct.[VersionId]
		, [Name] = paramProduct.[Name]
		, [VersionName] = paramProduct.[VersionName]
		, [ImageUrl] = paramProduct.[ImageUrl]
		, [Year] = paramProduct.[Year]
		, [Rating] = paramProduct.[Rating]
		, [Weight] = paramProduct.[Weight]
		, [LastChecked] = paramProduct.[LastChecked]
	OUTPUT inserted.BGGId INTO @ids
	FROM @product AS paramProduct
		LEFT JOIN [Product] AS existingProduct
			ON existingProduct.[BGGId] = paramProduct.[BGGId] 
				AND existingProduct.[VersionId] = paramProduct.[VersionId]
	WHERE existingProduct.[Id] IS NOT NULL

		SELECT [ParentBGGId]
		,[BGGId]
		,[VersionId]
		,[Name]
		,[VersionName]
		,[ImageUrl]
		,[Year]
		,[Rating]
		,[Weight]
	FROM [Product]
		LEFT JOIN @ids AS itemId ON [BGGId] = itemId.[Value]
		LEFT JOIN @ids AS parentId ON [ParentBGGId] = parentId.[Value]
	WHERE itemId.[Value] IS NOT NULL OR parentId.[Value] IS NOT NULL
	ORDER BY COALESCE([ParentBGGId], [BGGId]), [ParentBGGId], [BGGId], [VersionId], [Year]
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ProductUpdate] TO [bgaapi]
    AS [dbo];

