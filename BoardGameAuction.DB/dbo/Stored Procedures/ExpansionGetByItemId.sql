-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Gets all expansions for an Item
-- =============================================
CREATE PROCEDURE [dbo].[ExpansionGetByItemId] 
	@ItemId INT
AS
BEGIN
	SELECT *
	FROM [Expansion]
	WHERE [ItemId] = @ItemId;
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ExpansionGetByItemId] TO [bgaapi]
    AS [dbo];

