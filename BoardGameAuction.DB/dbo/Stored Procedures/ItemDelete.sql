﻿-- =============================================
-- Author:		Red Morris
-- Create date: 2020-06-03
-- Description:	Deletes an item
-- =============================================
CREATE PROCEDURE [dbo].[ItemDelete] 
	@LotId INT,
	@ItemId INT
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM [Item]
	WHERE [LotId] = @LotId AND [Id] = @ItemId
	SELECT @@ROWCOUNT
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ItemDelete] TO [bgaapi]
    AS [dbo];

