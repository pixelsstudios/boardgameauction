-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Adds a new auction
-- =============================================
CREATE PROCEDURE [dbo].[AuctionAdd] 
	@Code NVARCHAR(4),
	@Type SMALLINT,
	@Start DATETIME2(0),
	@End DATETIME2(0),
	@Password NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [Auction] ([Code], [Type], [Start], [End], [Password])
	VALUES (@Code, @Type, @Start, @End, @Password);
	SELECT SCOPE_IDENTITY()
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[AuctionAdd] TO [bgaapi]
    AS [dbo];

