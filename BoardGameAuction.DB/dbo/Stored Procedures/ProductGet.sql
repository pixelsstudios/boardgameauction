-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-27
-- Description:	Gets the products matching the ids
--				in the list, or children of those ids
/* 
	DECLARE @ids [dbo].[IntList]
	INSERT INTO @ids SELECT 6249
	exec ProductGet @ids
*/
-- =============================================
CREATE PROCEDURE ProductGet 
	@ids [dbo].[IntList] READONLY
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [ParentBGGId]
		,[BGGId]
		,[VersionId]
		,[Name]
		,[VersionName]
		,[ImageUrl]
		,[Year]
		,[Rating]
		,[Weight]
	FROM [Product]
		LEFT JOIN @ids AS itemId ON [BGGId] = itemId.[Value]
		LEFT JOIN @ids AS parentId ON [ParentBGGId] = parentId.[Value]
	WHERE itemId.[Value] IS NOT NULL OR parentId.[Value] IS NOT NULL
	ORDER BY COALESCE([ParentBGGId], [BGGId]), [ParentBGGId], [BGGId], [VersionId], [Year]
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ProductGet] TO [bgaapi]
    AS [dbo];

