﻿
-- =============================================
-- Author:		Red Morris
-- Create date: 2020-06-23
-- Description:	Sets the ActiveLotId on an Auction
-- =============================================
CREATE PROCEDURE [dbo].[LotMakeActive]
	@Id INT,
	@AuctionId INT
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [Auction]
	SET [ActiveLotId] = @Id
	WHERE [Id] = @AuctionId
	
	SELECT @@ROWCOUNT
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[LotMakeActive] TO [bgaapi]
    AS [dbo];

