-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Adds a new expansion
-- =============================================
CREATE PROCEDURE [dbo].[ExpansionAdd] 
	@ItemId INT,
	@BGGId INT,
	@VersionId INT,
	@Notes NVARCHAR(100)
AS
BEGIN
	INSERT INTO [Expansion] ([ItemId], [ProductId], [Notes])
	SELECT @ItemId AS [ItemId], [Product].[Id] AS ProductId, @Notes AS [Notes]
	FROM [Product] 
	WHERE [Product].[BGGId] = @BGGId AND [Product].[VersionId] = @VersionId;

	SELECT SCOPE_IDENTITY()
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ExpansionAdd] TO [bgaapi]
    AS [dbo];

