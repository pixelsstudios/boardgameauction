-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Returns the list of lots and 
--				items not yet sold in all auctions
-- =============================================
CREATE PROCEDURE [dbo].[AuctionGet] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [Auction].*,
		[Lot].*, 
		seller.[Id],
		seller.[Name],
		[Item].[Id], 
		[Item].[LotId], 
		[Item].[Notes], 
		itemDetails.[Id] AS [ProductId],
		itemDetails.[BGGId],
		itemDetails.[Name], 
		itemDetails.[VersionName], 
		itemDetails.[ImageUrl], 
		itemDetails.[Year], 
		itemDetails.[Rating], 
		itemDetails.[Weight],
		[Expansion].[Id], 
		[Expansion].[ItemId], 
		[Expansion].[Notes], 
		expansionDetails.[Id] AS [ProductId],
		expansionDetails.[BGGId],
		expansionDetails.[Name], 
		expansionDetails.[VersionName], 
		expansionDetails.[ImageUrl], 
		expansionDetails.[Year], 
		expansionDetails.[Rating], 
		expansionDetails.[Weight]
	FROM [Auction]
		LEFT JOIN [Lot] ON [Lot].[AuctionId] = [Auction].[Id]
		LEFT JOIN [Account] AS seller ON seller.[Id] = [Lot].[SellerId]
		LEFT JOIN [Item] ON [Item].[LotId] = [Lot].[Id]
			LEFT JOIN [Product] AS itemDetails ON itemDetails.[Id] = [Item].[ProductId]
		LEFT JOIN [Expansion] ON [Expansion].[ItemId] = [Item].[Id]
			LEFT JOIN [Product] AS expansionDetails ON expansionDetails.[Id] = [Expansion].[ProductId]
	WHERE [SalePrice] IS NULL
	ORDER BY [Auction].[Id], [Item].[Id], [Expansion].[Id]
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[AuctionGet] TO [bgaapi]
    AS [dbo];

