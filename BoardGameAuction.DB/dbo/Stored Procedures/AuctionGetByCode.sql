-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Returns the auction details by the code
-- =============================================
CREATE PROCEDURE [dbo].[AuctionGetByCode] 
	@Code NVARCHAR(4)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Auction].*
	FROM [Auction]
	WHERE [Auction].[Code] = @Code
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[AuctionGetByCode] TO [bgaapi]
    AS [dbo];

