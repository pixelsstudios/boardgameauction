-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Gets an item by Id
-- =============================================
CREATE PROCEDURE [dbo].[ItemGetById] 
	@Id INT
AS
BEGIN
	SELECT [Item].[Id], 
		[Item].[LotId], 
		[Item].[Notes], 
		[Product].[Id] AS BGGId, 
		[Product].[Name], 
		[Product].[ImageUrl], 
		[Product].[Year], 
		[Product].[Rating], 
		[Product].[Weight]
	FROM [Item]
		INNER JOIN [Product] ON [Product].[Id] = [Item].[ProductId]
	WHERE [Item].[Id] = @Id;
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ItemGetById] TO [bgaapi]
    AS [dbo];

