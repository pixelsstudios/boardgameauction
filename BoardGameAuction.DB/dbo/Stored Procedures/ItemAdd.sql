-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Adds a new item
-- =============================================
CREATE PROCEDURE [dbo].[ItemAdd] 
	@LotId INT,
	@BGGId INT,
	@VersionId INT,
	@Notes NVARCHAR(100)
AS
BEGIN
	INSERT INTO [Item] ([LotId], [ProductId], [Notes])
	SELECT @LotId AS [ItemId], [Product].[Id] AS ProductId, @Notes AS [Notes]
	FROM [Product] 
	WHERE [Product].[BGGId] = @BGGId AND [Product].[VersionId] = @VersionId;

	SELECT SCOPE_IDENTITY()
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ItemAdd] TO [bgaapi]
    AS [dbo];

