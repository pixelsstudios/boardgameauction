-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-27
-- Description:	Gets the products matching the ids
--				in the list, or children of those ids,
--				if any of them are older than the 
--				given date
/* 
	DECLARE @ids [dbo].[IntList]
	INSERT INTO @ids SELECT 6249
	exec ProductGetLimitedToDate @ids, N'2020-02-27'
*/
-- =============================================
CREATE PROCEDURE [dbo].[ProductGetLimitedToDate] 
	@ids [dbo].[IntList] READONLY,
	@NoOlderThanDate DateTime2(0)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT p.[ParentBGGId]
			,p.[BGGId]
			,p.[VersionId]
			,p.[Name]
			,p.[VersionName]
			,p.[ImageUrl]
			,p.[Year]
			,p.[Rating]
			,p.[Weight]
			,p.[LastChecked]
			,MIN(oldestProduct.[LastChecked])
	FROM [Product] AS p
		LEFT JOIN @ids AS itemId ON [BGGId] = itemId.[Value]
		LEFT JOIN @ids AS parentId ON [ParentBGGId] = parentId.[Value]
		LEFT JOIN [Product] AS oldestProduct ON COALESCE(oldestProduct.[ParentBGGId], oldestProduct.[BGGId]) = COALESCE(p.[ParentBGGId], p.[BGGId])
	WHERE (itemId.[Value] IS NOT NULL OR parentId.[Value] IS NOT NULL) 
		AND p.[LastChecked] >= @noOlderThanDate
	GROUP BY p.[ParentBGGId]
			,p.[BGGId]
			,p.[VersionId]
			,p.[Name]
			,p.[VersionName]
			,p.[ImageUrl]
			,p.[Year]
			,p.[Rating]
			,p.[Weight]
			,p.[LastChecked]
	ORDER BY COALESCE(p.[ParentBGGId], p.[BGGId]), p.[ParentBGGId], p.[BGGId], p.[VersionId], p.[Year]
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ProductGetLimitedToDate] TO [bgaapi]
    AS [dbo];

