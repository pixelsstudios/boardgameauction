-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Sets an lots SalePrice
-- =============================================
CREATE PROCEDURE [dbo].[LotSell]
	@Id INT,
	@Price DECIMAL(6,2)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [Lot]
	SET [SalePrice] = @Price
	WHERE [Id] = @Id;

	SELECT *
	FROM [Lot]
	WHERE [Id] = @Id;
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[LotSell] TO [bgaapi]
    AS [dbo];

