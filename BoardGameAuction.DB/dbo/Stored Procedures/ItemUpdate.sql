-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Updates the details of an existing item
-- =============================================
CREATE PROCEDURE [dbo].[ItemUpdate] 
	@Id INT,
	@Notes NVARCHAR(100)
AS
BEGIN
	UPDATE [Item]
	SET [Notes] = @Notes
	WHERE [Id] = @Id;

	SELECT SCOPE_IDENTITY()
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ItemUpdate] TO [bgaapi]
    AS [dbo];

