-- =============================================
-- Author:		Red Morris
-- Create date: 2020-05-26
-- Description:	Adds a new lot
-- =============================================
CREATE PROCEDURE [dbo].[LotAdd] 
	@AuctionId INT,
	@SellerId INT,
	@StartPrice DECIMAL(6,2),
	@EndPrice DECIMAL(6,2)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [Lot] ([AuctionId], [SellerId], [StartPrice], [EndPrice])
	VALUES (@AuctionId, @SellerId, @StartPrice, @EndPrice);
	SELECT SCOPE_IDENTITY()
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[LotAdd] TO [bgaapi]
    AS [dbo];

