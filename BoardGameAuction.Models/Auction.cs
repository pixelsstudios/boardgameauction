﻿using BoardGameAuction.Models.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace BoardGameAuction.Models
{
	public class Auction
	{
		public Auction()
		{
			var now = DateTime.UtcNow;
			now = now.AddMinutes(59 - now.Minute).AddSeconds(60 - now.Second);

			Start = now;
			End = now.AddHours(1);
		}

		public int Id { get; set; }

		/// <summary>
		/// A randomly generated 4-letter public auction code
		/// </summary>
		[MaxLength(4)]
		public string Code { get; set; }

		/// <summary>
		/// A password to gain access to AuctionAdmin role
		/// The password is not assigned to an individual
		/// </summary>
		[Required(ErrorMessage = "A password is needed so you can get back in if there's an issue")]
		[MinLength(4, ErrorMessage = "The minimum length for a password is 4 characters")]
		public string Password { get; set; }

		[Display(Name = "Auction Type")]
		public AuctionType Type { get; set; } = AuctionType.Dutch;

		[Display(Name = "Start Time")]
		[DataType(DataType.DateTime)]
		[DisplayFormat(DataFormatString = "{0:d MMM yyyy hh:mm}")]
		public DateTime Start { get; set; }

		[Display(Name = "End Time")] 
		[DataType(DataType.DateTime)]
		[DisplayFormat(DataFormatString = "{0:d MMM yyyy hh:mm}")]
		public DateTime End { get; set; }
		public AuctionStatus Status => 
			Start == new DateTime() || Start > DateTime.UtcNow ? AuctionStatus.NotStarted :
			End > DateTime.UtcNow ? AuctionStatus.Active : 
			AuctionStatus.Ended;
		public List<Lot> Lots { get; set; } = new List<Lot>();
		public Nullable<int> ActiveLotId { get; set; }
		
		/// <summary>
		/// Removes data that should not be seen by users without the
		/// AuctionAdmin or SiteAdmin roles
		/// </summary>
		/// <returns>An Auction without private info</returns>
		public Auction ForNonAdmins(int sellerId)
		{
			return new Auction
			{
				Id = Id,
				Code = Code,
				Start = Start,
				End = End,
				Lots = Lots.Select(x => x.ForNonAdmins(sellerId)).ToList(),
				ActiveLotId = ActiveLotId
			};
		}
	}
}