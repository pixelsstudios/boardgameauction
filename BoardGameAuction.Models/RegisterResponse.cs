﻿namespace BoardGameAuction.Models
{
	public class RegisterResponse
	{
		public int Id { get; set; }
		public string Message { get; set; }
	}
}