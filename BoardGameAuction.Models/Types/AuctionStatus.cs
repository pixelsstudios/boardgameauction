﻿namespace BoardGameAuction.Models.Types
{
	public enum AuctionStatus
	{
		NotStarted = 0,
		Active = 1,
		Ended = 2
	}
}