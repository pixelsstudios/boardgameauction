﻿namespace BoardGameAuction.Models.Types
{
	public enum RoleType
	{
		None = 0,
		SiteAdmin = 1,
		AuctionAdmin = 2,
		Seller = 3,
		Attendee = 4
	}
}