﻿namespace BoardGameAuction.Models.Types
{
	public enum AuctionType
	{
		Dutch = 0,
		Regular = 1
	}
}