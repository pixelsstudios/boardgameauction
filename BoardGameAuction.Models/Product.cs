﻿using System.Collections.Generic;

namespace BoardGameAuction.Models
{
	/// <summary>
	/// A specific version of a boardgame or boardgame expansion
	/// Aligns closely with a BoardGameGeek 'thing'
	/// https://boardgamegeek.com/wiki/page/BGG_XML_API2#toc3
	/// </summary>
	public class Product : ProductBase
	{
		public int Id { get; set; }
		public int VersionId { get; set; }
		public string VersionName { get; set; }
		public decimal Rating { get; set; }
		public decimal Weight { get; set; }

		/// <summary>
		/// Creates a shallow copy clone of the current product
		/// This is used when a list of versions need to create
		/// a parent concept of a game for them to be versions of
		/// </summary>
		/// <returns>A Product object with all simple variable 
		/// values copied from the original</returns>
		public Product Clone()
		{
			return new Product
			{
				BGGId = this.BGGId,
				Name = this.Name,
				Year = this.Year,
				ImageUrl = this.ImageUrl,
				Rating = this.Rating,
				Weight = this.Weight,
				VersionId = this.VersionId,
				VersionName = this.VersionName,
				Versions = new List<Product>(),
				Expansions = new List<Product>()
			};
		}
	}
}