﻿using System.Collections.Generic;

namespace BoardGameAuction.Models
{
	/// <summary>
	/// The lowest form concept of a boardgame or boardgame expansion
	/// </summary>
	public class ProductBase
	{
		public int BGGId { get; set; }
		public string Name { get; set; }
		public int Year { get; set; }
		public string ImageUrl { get; set; }

		public List<Product> Versions { get; set; } = new List<Product>();
		public List<Product> Expansions { get; set; } = new List<Product>();

		public virtual bool ShouldSerializeExpansions() => true;
		public virtual bool ShouldSerializeVersions() => true;
	}
}