﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace BoardGameAuction.Models.BGG
{
	[XmlType]
	public class XmlItem
	{
		[XmlAttribute(AttributeName = "id")]
		public string Id { get; set; }

		[XmlAttribute(AttributeName = "type")]
		public string Type { get; set; }

		[XmlAttribute(AttributeName = "value")]
		public string Value { get; set; }

		[XmlText]
		public string Text { get; set; }

		public object _dummy; // Force deserialzer to not treat this as simpleContext
	}

	[XmlType("item")]
	public class Item : XmlItem
	{
		[XmlElement(ElementName = "yearpublished")]
		public XmlItem YearPublished { get; set; }

		[XmlElement(ElementName = "name")]
		public XmlItem Name { get; set; }

		[XmlElement(ElementName = "thumbnail")]
		public string Thumbnail { get; set; }

		[XmlElement(ElementName = "versions")]
		public Versions Versions { get; set; }

		[XmlElement(ElementName = "link")]
		public List<XmlItem> Link { get; set; }

		[XmlElement(ElementName = "statistics")]
		public Statistics Statistics { get; set; }

		//[XmlElement(ElementName = "image")]
		//public string Image { get; set; }
		//[XmlElement(ElementName = "productcode")]
		//public Productcode Productcode { get; set; }
		//[XmlElement(ElementName = "width")]
		//public Width Width { get; set; }
		//[XmlElement(ElementName = "length")]
		//public Length Length { get; set; }
		//[XmlElement(ElementName = "depth")]
		//public Depth Depth { get; set; }
	}

	public class Versions
	{
		[XmlElement(ElementName = "item")]
		public List<Item> Item { get; set; }
	}

	public class Statistics
	{
		[XmlElement(ElementName = "ratings")]
		public Ratings Ratings { get; set; }
	}

	public class Ratings
	{
		[XmlElement(ElementName = "average")]
		public XmlItem Rating { get; set; }

		[XmlElement(ElementName = "averageweight")]
		public XmlItem Weight { get; set; }
	}
}