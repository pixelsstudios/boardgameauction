﻿using System.Collections.Generic;

namespace BoardGameAuction.Models
{
	/// <summary>
	/// An object to map to the response from the undocumented
	/// BGG text search https://boardgamegeek.com/search/boardgame?q={0}&showcount={1}
	/// This endpoint is used over others because it returns
	/// data in JSON format, and it has some image data I
	/// hope to be able to utilise once I work out how
	/// </summary>
	public class IdSearchResponse
	{
		public List<Product> Items { get; set; } = new List<Product>();
	}
}