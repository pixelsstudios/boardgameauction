﻿using System.ComponentModel.DataAnnotations;

namespace BoardGameAuction.Models
{
	public class RegisterRequest : LoginRequest
	{
		[Required(ErrorMessage = "A password is needed so you can get back in if there's an issue")]
		[MinLength(4, ErrorMessage = "The minimum length for a password is 4 characters")]
		[Display(Name = "Confirm Password")]
		public string ConfirmPassword { get; set; }
	}
}