﻿using System.ComponentModel.DataAnnotations;

namespace BoardGameAuction.Models
{
	public class LoginRequest : PasswordRequest
	{
		[Required(ErrorMessage = "A forename is required. There's a lot of Daves")]
		[RegularExpression(@"^.{2,}$", ErrorMessage = "An initial is not a name")]
		[StringLength(25, ErrorMessage = "There's a 25 charachter limit on names")]
		public string Forename { get; set; }

		[Required(ErrorMessage = "A surname is required. There's a lot of Johnsons about")]
		[RegularExpression(@"^.{2,}$", ErrorMessage = "A letter isn't a surname")]
		[StringLength(25, ErrorMessage = "There's a 25 charachter limit on names")]
		public string Surname { get; set; }
	}
}