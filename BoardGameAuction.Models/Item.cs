﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BoardGameAuction.Models
{
	/// <summary>
	/// An Item that is being sold as part of a Lot. Usually a board game
	/// </summary>
	public class Item : Product
	{
		public Item() { }

		public Item(Product product)
		{
			this.BGGId = product.BGGId;
			this.Name = product.Name;
			this.Year = product.Year;
			this.ImageUrl = product.ImageUrl;
			this.Rating = product.Rating;
			this.Weight = product.Weight;
			this.VersionId = product.VersionId;
			this.VersionName = product.VersionName;
			
		}

		public int ProductId { get; set; }
		public int LotId { get; set; }

		[StringLength(100, ErrorMessage = "There's a 100 charachter limit on notes")]
		public string Notes { get; set; }

		public new List<Expansion> Expansions { get; set; } = new List<Expansion>();

		/// <summary>
		/// Creates a shallow copy clone of the current item
		/// This is used when a list of items is to be stored
		/// with differing properties such as removing redundant
		/// url roots
		/// </summary>
		/// <returns>An Item object with all variable 
		/// values copied from the original</returns>
		public new Item Clone()
		{
			return new Item
			{
				BGGId = this.BGGId,
				Name = this.Name,
				Year = this.Year,
				ImageUrl = this.ImageUrl,
				Rating = this.Rating,
				Weight = this.Weight,
				VersionId = this.VersionId,
				VersionName = this.VersionName,
				ProductId = this.ProductId,
				LotId = this.LotId,
				Notes = this.Notes,
				Versions = this.Versions,
				Expansions = this.Expansions
			};
		}
	}
}