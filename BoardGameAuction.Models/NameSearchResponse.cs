﻿using System.Collections.Generic;

namespace BoardGameAuction.Models
{
    public class NameSearchResponse
	{
		public List<ProductBase> Items { get; set; }
	}
}