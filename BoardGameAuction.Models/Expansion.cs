﻿using System.ComponentModel.DataAnnotations;

namespace BoardGameAuction.Models
{
	public class Expansion : Product
	{
		public Expansion() { }

		public Expansion(Product product)
		{
			this.BGGId = product.BGGId;
			this.Name = product.Name;
			this.Year = product.Year;
			this.ImageUrl = product.ImageUrl;
			this.Rating = product.Rating;
			this.Weight = product.Weight;
			this.VersionId = product.VersionId;
			this.VersionName = product.VersionName;
		}

		public int ProductId { get; set; }
		public int ItemId { get; set; }

		[StringLength(100, ErrorMessage = "There's a 100 charachter limit on notes")]
		public string Notes { get; set; }
	}
}
