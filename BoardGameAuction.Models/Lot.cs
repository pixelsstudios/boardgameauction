﻿using System.Collections.Generic;

namespace BoardGameAuction.Models
{
	/// <summary>
	/// A singular or collection of Items being sold together
	/// </summary>
	public class Lot
	{
		private double _startPrice;
		private double _endPrice;

		public int Id { get; set; }
		public int AuctionId { get; set; }
		public List<Item> Items { get; set; } = new List<Item>();
		public Person Seller { get; set; } = new Person();

		/// <summary>
		/// The starting price. 
		/// In a Dutch Auction this will be the higher number the seller suggests starting at. In a regular auction it will be the reserve
		/// </summary>
		public double StartPrice { get { return _startPrice; } set { _startPrice = value; if (EndPrice > 0) { SetSalesOptions(); } } }

		/// <summary>
		/// The ending price.
		/// In a Dutch Auction this will be the lowest price the seller will accept. In a regular auction it will be a Buy It Now price
		/// </summary>
		public double EndPrice { get { return _endPrice; } set { _endPrice = value; if (StartPrice > 0) { SetSalesOptions(); } } }

		/// <summary>
		/// The price the item actually sold for. -1 indicates a no sale
		/// </summary>
		public double? SalePrice { get; set; }
		public List<double> SalesOptions { get; set; } = new List<double>();

		// Return a restricted object without private info
		public Lot ForNonAdmins(int sellerId)
		{
			return new Lot
			{
				Id = Id,
				AuctionId = AuctionId,
				SalePrice = SalePrice,
				Seller = Seller?.Id == sellerId ? Seller : null,
				StartPrice = Seller?.Id == sellerId ? StartPrice : 0,
				EndPrice = Seller?.Id == sellerId ? EndPrice : 0,
				Items = Items
			};
		}

		/// <summary>
		/// Sets the suggested prices to sell for based on ramping up or down based on the start and end prices
		/// </summary>
		private void SetSalesOptions()
		{
			// Item check ensures this doesn't run for every item in the lot
			if (this.StartPrice > 0 && this.EndPrice > 0 && this.Items.Count == 0)
			{
				double step = 1;
				for (double price = StartPrice; price >= EndPrice; price -= step)
				{
					if (price >= EndPrice + 30 + 10)
					{
						SalesOptions.Add(price);
						step = 10;
						continue;
					}

					if (price >= EndPrice + 20 + 5)
					{
						SalesOptions.Add(price);
						step = 5;
						continue;
					}

					if (price >= EndPrice + 10 + 3 && price % 2 == 1)
					{
						SalesOptions.Add(price);
						step = 3;
						continue;
					}

					if (price >= EndPrice + 10 + 2 && price % 2 == 0)
					{
						SalesOptions.Add(price);
						step = 2;
						continue;
					}

					step = 1;
					SalesOptions.Add(price);
				}
			}
		}
	}
}