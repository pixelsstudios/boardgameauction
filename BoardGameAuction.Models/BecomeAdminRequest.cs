﻿using System.ComponentModel.DataAnnotations;

namespace BoardGameAuction.Models
{
	public class BecomeAdminRequest
	{
		[Required(ErrorMessage = "We've kept security simple, but we do need a password")]
		[RegularExpression(@"^.{4,}$", ErrorMessage = "The minimum length for a password is 4 characters")]
		[StringLength(25, ErrorMessage = "There's a 100 charachter limit on passwords")]
		public string Password { get; set; }
	}
}