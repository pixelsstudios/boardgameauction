﻿using BoardGameAuction.Models.Types;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using System;
using System.Threading.Tasks;

namespace BoardGameAuction.Models.Authorisation
{
    public class IsAdminRequirement : IAuthorizationRequirement
    {
        public class IsAdminHandler : AuthorizationHandler<IsAdminRequirement>
        {
            private readonly IHttpContextAccessor _httpContextAccessor;

            public IsAdminHandler(IHttpContextAccessor httpContextAccessor)
            {
                _httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
            }

            protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, IsAdminRequirement requirement)
            {
                // Check there's a claim for role, auction id and code
                if (context.User.HasClaim(c => c.Type == "role") && context.User.HasClaim(c => c.Type == "auction") && context.User.HasClaim(c => c.Type == "auctionCode"))
                {
                    int roleId;
                    // Check the role in the claim is a number
                    if (int.TryParse(context.User.FindFirst("role").Value, out roleId))
                    {
                        if ((RoleType)roleId == RoleType.AuctionAdmin)
                        {
                            context.Succeed(requirement);
                            return Task.CompletedTask;
                        }
                    }
                }

                // The user is not an admin, so redirect to the login page
                var redirectUrl = Uri.EscapeUriString($"/account/becomeadmin?redirecturl={_httpContextAccessor.HttpContext?.Request?.GetEncodedPathAndQuery()}");
                _httpContextAccessor.HttpContext.Response.Redirect(redirectUrl);

                context.Succeed(requirement);

                return Task.CompletedTask;
            }
        }
    }
}
