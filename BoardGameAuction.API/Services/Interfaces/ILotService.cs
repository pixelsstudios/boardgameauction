﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BoardGameAuction.Models;

namespace BoardGameAuction.API.Services.Interfaces
{
	public interface ILotService
	{
		Task<Lot> Add(Lot lot);
		Task<Lot> Edit(Lot lot);
		Task<Lot> Get(int id, bool isAdmin, int sellerId);
		Task<List<Lot>> GetAll(int auctionId, bool isAdmin, int sellerId);
		Task<bool> MakeActive(int id, int auctionId);
		Task<Lot> Sell(int id, double price);
		Task<bool> Delete(int auctionId, int lotId);
	}
}