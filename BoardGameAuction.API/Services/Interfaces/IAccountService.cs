﻿using BoardGameAuction.Models;
using System.Threading.Tasks;

namespace BoardGameAuction.API.Services.Interfaces
{
	public interface IAccountService
	{
		Task<(int Id, string Message)> Add(string forename, string surname, string password);
		Task<int> GetId(string forename, string surname, string password);
		Task<Person> Get(int id);
	}
}