﻿using BoardGameAuction.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BoardGameAuction.API.Services.Interfaces
{
    public interface IItemService
    {
        Task<Item> Add(Item item, int auctionId);
        Task<Item> Edit(Item item, int auctionId);
        Task<Item> Get(int id);
        Task<List<Item>> GetAll(int lotId);
        Task<bool> Delete(int id, int lotId);
    }
}