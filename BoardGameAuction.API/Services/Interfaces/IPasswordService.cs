﻿namespace BoardGameAuction.API.Services.Interfaces
{
	public interface IPasswordService
	{
		string HashPassword(string username, string password);
		bool VerifyHashedPassword(string username, string hashedPassword, string providedPassword);
	}
}
