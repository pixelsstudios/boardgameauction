﻿using System.Collections.Generic;
using System.Security.Claims;

namespace BoardGameAuction.API.Services.Interfaces
{
	public interface IAuthenticationService
	{
		string CreateToken(IEnumerable<Claim> claims);
	}
}