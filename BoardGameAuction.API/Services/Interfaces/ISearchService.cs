﻿using BoardGameAuction.API.Repositories;
using BoardGameAuction.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BoardGameAuction.API.Services.Interfaces
{
	public interface ISearchService
	{
		Task<NameSearchResponse> Search(string query);
		Task<IdSearchResponse> Search(List<int> bggIds);
		List<ProductTable> PrepareForDatabase(IEnumerable<Product> products);
	}
}