﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoardGameAuction.Models;
using BoardGameAuction.Models.Types;

namespace BoardGameAuction.API.Services.Interfaces
{
	public interface IAuctionService
	{
		Task<Auction> Add(string password);
		Task<Auction> Get(int id, RoleType role, int sellerId);
		Task<List<Auction>> GetAll(RoleType role, int sellerId);
		Task<Auction> GetByCode(string code, RoleType role, int sellerId);
		Task<Auction> Edit(Auction auction);
		Task<Auction> Start(int id, DateTime start);
		Task<Auction> Stop(int id, DateTime end);
		Task<Auction> Summary(int id, RoleType role, int sellerId);
		Task<Auction> Join(int id);
		Task<bool> AuthenticateAdmin(int id, string password);
		Task<int> Leave(int id);
	}
}