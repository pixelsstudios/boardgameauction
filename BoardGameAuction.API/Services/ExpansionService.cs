﻿using BoardGameAuction.Models;
using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.API.Services.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BoardGameAuction.API.Services
{
    public class ExpansionService : IExpansionService
    {
        private readonly AppSettings _appSettings;
        private readonly IExpansionRepository _repository;

        public ExpansionService(IOptions<AppSettings> appSettings, IExpansionRepository repository)
        {
            _appSettings = appSettings.Value;
            _repository = repository;
        }

        public async Task<Expansion> Add(Expansion expansion)
        {
            if (expansion.ItemId <= 0 || expansion.BGGId <= 0) { throw new ArgumentNullException(); }

            try
            {
                var newExpansion = await _repository.Add(expansion);

                if (newExpansion == null || newExpansion.Id == 0) { throw new ArgumentException(); }

                expansion.Id = newExpansion.Id;
                return expansion;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
        }

        public async Task<Expansion> Edit(Expansion expansion)
        {
            if (expansion.Id <= 0 || expansion.ItemId <= 0 || expansion.BGGId <= 0) { throw new ArgumentNullException(); }

            return await _repository.Edit(expansion);
        }

        public async Task<Expansion> Get(int id)
        {
            if (id <= 0) { return null; }

            return await _repository.Get(id);
        }

        public async Task<List<Expansion>> GetAll(int itemId)
        {
            if (itemId <= 0) { return null; }

            return await _repository.GetAll(itemId);
        }
    }
}
