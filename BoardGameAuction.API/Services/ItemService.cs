﻿using BoardGameAuction.Models;
using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.API.Services.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BoardGameAuction.API.Services
{
    public class ItemService : IItemService
    {
        private readonly AppSettings _appSettings;
        private readonly IItemRepository _repository;
        private readonly IExpansionService _expansionService;
        private readonly IAuctionService _auctionService;

        public ItemService(IOptions<AppSettings> appSettings, IItemRepository repository, IExpansionService expansionService, IAuctionService auctionService)
        {
            _appSettings = appSettings.Value;
            _repository = repository;
            _expansionService = expansionService;
            _auctionService = auctionService;
        }

        public async Task<Item> Add(Item item, int auctionId)
        {
            if (item.LotId <= 0 || item.BGGId <= 0) { throw new ArgumentNullException(); }

            try
            {
                var auction = await _auctionService.Get(auctionId, Models.Types.RoleType.Attendee, 0);
                if (auction == null) { throw new ArgumentOutOfRangeException("auctionId", "Auction not found"); }

                if (auction.Status != Models.Types.AuctionStatus.NotStarted)
                {
                    throw new Exception("Auction has already started");
                }

                var newItem = await _repository.Add(item);

                if (newItem == null || newItem.Id == 0) { throw new ArgumentException(); }

                item.Id = newItem.Id;
                foreach (var expansion in item.Expansions)
                {
                    expansion.ItemId = newItem.Id;
                    await _expansionService.Add(expansion);
                }
                return item;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
        }

        public async Task<Item> Edit(Item item, int auctionId)
        {
            if (item.Id <= 0 || item.LotId <= 0 || item.BGGId <= 0) { throw new ArgumentNullException(); }

            var auction = await _auctionService.Get(auctionId, Models.Types.RoleType.Attendee, 0);
            if (auction == null) { throw new ArgumentOutOfRangeException("auctionId", "Auction not found"); }

            if (auction.Status != Models.Types.AuctionStatus.NotStarted)
            {
                throw new Exception("Auction has already started");
            }

            return await _repository.Edit(item);
        }

        public async Task<Item> Get(int id)
        {
            if (id <= 0) { return null; }

            return await _repository.Get(id);
        }

        public async Task<List<Item>> GetAll(int lotId)
        {
            if (lotId <= 0) { return null; }

            return await _repository.GetAll(lotId);
        }

        public async Task<bool> Delete(int id, int lotId)
        {
            if (id == 0) { return false; }

            return await _repository.Delete(id, lotId) > 0;
        }
    }
}
