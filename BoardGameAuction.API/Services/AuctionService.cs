﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoardGameAuction.Models;
using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.API.Services.Interfaces;
using Microsoft.Extensions.Options;
using BoardGameAuction.Models.Types;

namespace BoardGameAuction.API.Services
{
	public class AuctionService : IAuctionService
	{
		private readonly AppSettings _appSettings;
		private readonly IAuctionRepository _repo;
		private readonly IPasswordService _passwordService;

		public AuctionService(IOptions<AppSettings> appSettings, IAuctionRepository repo, IPasswordService passwordService)
		{
			_appSettings = appSettings.Value;
			_repo = repo;
			_passwordService = passwordService;
		}

		public async Task<Auction> Add(string password)
		{
			int safetyCatch = 0;
			const string chars = "BCDFGHJKLMNPQRSTUVWXYZ";
			string newCode;
			do
			{
				Random random = new Random();
				newCode = new string(Enumerable.Repeat(chars, 4).Select(s => s[random.Next(s.Length)]).ToArray());
				if (await _repo.GetByCode(newCode) == null)
				{
					var auction = new Auction { Code = newCode, Password = ""};
					var result = await _repo.Add(auction);
					result.Password = _passwordService.HashPassword(result.Id.ToString(), password);
					return await _repo.Update(result);
				}
				newCode = "";

				safetyCatch++;
			} while (newCode == "" && safetyCatch < 10);

			throw new Exception ("Tried 10 times to create a unique auction code and failed. Strange things are afoot.");
		}

		public async Task<Auction> Get(int id, RoleType role, int sellerId)
		{
			if (id <= 0) { return null; }

			var auction = await _repo.Get(id);

			if (auction == null) { return null; }

			if (auction.Status == AuctionStatus.NotStarted && role != RoleType.AuctionAdmin && role != RoleType.SiteAdmin)
			{
				// Before an auction begins: 
				// - admins can see all
				// - sellers can see their lots
				// - other can't see anything
				auction.Lots = auction.Lots.Where(l => l.Seller.Id == sellerId).ToList();
			}

			auction.Lots = auction?.Lots.Select(l => 
			{
				l.Items = l.Items.Select(i =>
				{
					i.ImageUrl = i.ImageUrl != null ? _appSettings.BGGImgUrlRoot + i.ImageUrl : null;
					return i;
				}).ToList(); 
				return l;
			}).OrderBy(o => o?.Items.FirstOrDefault()?.Name ?? "ZZZZZZ").ToList();

			if (role == RoleType.SiteAdmin || role == RoleType.AuctionAdmin) { return auction; }

			//// Return a restricted object without private info
			return auction.ForNonAdmins(sellerId); 
		}

		public async Task<List<Auction>> GetAll(RoleType role, int sellerId)
		{
			var auctions = await _repo.GetAll();
			auctions = auctions.Select(a => { 
				a.Lots = a?.Lots.Select(l =>
				{
					l.Items = l.Items.Select(i =>
					{
						i.ImageUrl = i.ImageUrl != null ? _appSettings.BGGImgUrlRoot + i.ImageUrl : null;
						return i;
					}).ToList();
					return l;
				}).ToList();
				return a;
			}).ToList();

			if (role == RoleType.SiteAdmin || role == RoleType.AuctionAdmin) { return auctions; }

			// Return a restricted object without private info
			return auctions.Select(a => a.ForNonAdmins(sellerId)).ToList();
		}

		public async Task<Auction> GetByCode(string code, RoleType role, int sellerId)
		{
			if (code.Length != 4) { throw new ArgumentException("Invalid Auction code"); }

			var auction = await _repo.GetByCode(code);

			if (auction == null) { return auction; }

			auction.Lots = auction?.Lots.Select(l =>
			{
				l.Items = l.Items.Select(i =>
				{
					i.ImageUrl = i.ImageUrl != null ? _appSettings.BGGImgUrlRoot + i.ImageUrl : null;
					return i;
				}).ToList();
				return l;
			}).ToList();

			if (role == RoleType.SiteAdmin || role == RoleType.AuctionAdmin) { return auction; }

			return auction.ForNonAdmins(sellerId);
		}

		public async Task<Auction> Edit(Auction auction)
		{
			if (auction.Id == 0) { throw new ArgumentNullException("Id"); }

			// [DoNotUpdatePassword] is a magic string that will leave the password
			// as it is and save me creating a separate Auction model without it
			auction.Password = auction.Password.Replace("[DoNotUpdatePassword]", "");
			if (string.IsNullOrEmpty(auction.Password) == false)
			{
				auction.Password = _passwordService.HashPassword(auction.Id.ToString(), auction.Password);
			}

			return await _repo.Update(auction);
		}

		public async Task<Auction> Start(int id, DateTime start)
		{
			var auction = await _repo.Get(id);
			auction.Start = start;
			return await _repo.Update(auction);
		}

		public async Task<Auction> Stop(int id, DateTime end)
		{
			var auction = await _repo.Get(id);
			auction.End = end;
			return await _repo.Update(auction);
		}

		public async Task<Auction> Summary(int auctionId, RoleType role, int sellerId)
		{
			if (auctionId <= 0) { return null; }

			var auction = await _repo.Summary(auctionId);

			if (role == RoleType.SiteAdmin || role == RoleType.AuctionAdmin) { return auction; }

			// Return a restricted object without private info
			return auction.ForNonAdmins(sellerId);
		}

		public async Task<Auction> Join(int id)
		{
			return await _repo.Join(id);
		}

		public async Task<int> Leave(int id)
		{
			return await _repo.Leave(id);
		}

		public async Task<bool> AuthenticateAdmin(int id, string password)
		{
			var auction = await _repo.Get(id);
			return _passwordService.VerifyHashedPassword(id.ToString(), auction.Password, password);
		}
	}
}
