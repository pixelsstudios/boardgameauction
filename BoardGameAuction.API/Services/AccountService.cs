﻿using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.API.Services.Interfaces;
using BoardGameAuction.Models;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace BoardGameAuction.API.Services
{
	public class AccountService : IAccountService
	{
		private readonly AppSettings _appSettings;
		private readonly IAccountRepository _repo;
		private readonly IPasswordService _passwordService;

		public AccountService(IOptions<AppSettings> appSettings, IAccountRepository repo, IPasswordService passwordService)
		{
			_appSettings = appSettings.Value;
			_repo = repo;
			_passwordService = passwordService;
		}

		/// <summary>
		/// Adds an account if it does not already exists, and retuns the Id in either case
		/// </summary>
		/// <param name="forename">Users forename, to be combined with surname to create the Name field</param>
		/// <param name="surname">Users surname, to be combined with forename to create the Name field</param>
		/// <param name="password">The password for the account</param>
		/// <returns>A named tuple of the account Id and the Message to return to the client</returns>
		public async Task<(int Id, string Message)> Add(string forename, string surname, string password)
		{
			// Do all the time consuming calls no matter the logic flow so that the timings for failures are the same
			var person = await _repo.Get(forename, surname);
			var hashedPassword = _passwordService.HashPassword($"{forename}{surname}", password);
			int existingId = await GetId(forename, surname, password);

			if (person != null && string.IsNullOrEmpty(person.Name) == false)
			{
				// An account exists with that name
				if (existingId == 0)
				{
					// The password did not match. Give vague error so as not to help brute force attacks
					return (0, "Registration failed. The account might exist with a different password, or the details entered might not meet the minimum criteria");
				}

				// The account exists with the same details, so add a message to inform and carry on
				return (person.Id, "Account already exists");
			} 

			return (await _repo.Add(forename, surname, hashedPassword), string.Empty);
		}

		/// <summary>
		/// Authenticates the details provided match a user
		/// </summary>
		/// <param name="forename">The forename for the account</param>
		/// <param name="surname">The surname for the account</param>
		/// <param name="password">The password for the account</param>
		/// <returns>A bool on whether the account was not found and the credentials were correct</returns>
		public async Task<int> GetId(string forename, string surname, string password)
		{
			var account = await _repo.Get(forename, surname);
			return _passwordService.VerifyHashedPassword($"{forename}{surname}", account?.HashedPassword, password) ? account.Id : 0;
		}

		public async Task<Person> Get(int id)
		{
			return await _repo.Get(id);
		}
	}
}