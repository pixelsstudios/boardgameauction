﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoardGameAuction.Models;
using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.API.Services.Interfaces;
using Microsoft.Extensions.Options;
using System.Linq;

namespace BoardGameAuction.API.Services
{
	public class LotService : ILotService
	{
		private readonly AppSettings _appSettings;
		private readonly ILotRepository _repo;
		private readonly IItemService _itemService;
		private readonly IAuctionService _auctionService;

		public LotService(IOptions<AppSettings> appSettings, ILotRepository repo, IItemService itemService, IAuctionService auctionService)
		{
			_appSettings = appSettings.Value;
			_repo = repo;
			_itemService = itemService;
			_auctionService = auctionService;
		}

		public async Task<Lot> Add(Lot lot)
		{
			if (lot.AuctionId <= 0) { throw new ArgumentNullException(); }

			try
			{
				var auction = await _auctionService.Get(lot.AuctionId, Models.Types.RoleType.Attendee, 0);
				if (auction == null) { throw new ArgumentOutOfRangeException("auctionId", "Auction not found"); }

				if (auction.Status != Models.Types.AuctionStatus.NotStarted)
				{
					throw new Exception("Auction has already started");
				}

				var newLot = await _repo.Add(lot);
				
				if (newLot == null || newLot.Id == 0) { throw new ArgumentException(); }

				lot.Id = newLot.Id;
				foreach(var item in lot.Items)
				{
					item.LotId = newLot.Id;

					// Create a copy to trim image file url root off, we don't need to store that every time
					var dbItem = item.Clone();
					dbItem.ImageUrl = dbItem.ImageUrl != null ? dbItem.ImageUrl.Replace(_appSettings.BGGImgUrlRoot, "") : null;
					dbItem.Expansions = dbItem.Expansions.Select(e => { e.ImageUrl = e.ImageUrl != null ? e.ImageUrl.Replace(_appSettings.BGGImgUrlRoot, "") : null; return e; }).ToList();

					await _itemService.Add(dbItem, lot.AuctionId);
				}
				return lot;
			}
			catch (ArgumentException ex)
			{
				throw ex;
			}

		}

		public async Task<bool> Delete(int auctionId, int lotId)
		{
			return await _repo.Delete(auctionId, lotId);
		}

		public async Task<Lot> Edit(Lot lot)
		{
			if (lot.Id <= 0 || lot.AuctionId <= 0) { throw new ArgumentNullException(); }

			var auction = await _auctionService.Get(lot.AuctionId, Models.Types.RoleType.Attendee, 0);
			if (auction == null) { throw new ArgumentOutOfRangeException("auctionId", "Auction not found"); }

			if (auction.Status != Models.Types.AuctionStatus.NotStarted)
			{
				throw new Exception("Auction has already started");
			}

			// Remove image url root before storing
			lot.Items = lot?.Items.Select(r => { 
				r.ImageUrl = r.ImageUrl != null ? r.ImageUrl.Replace(_appSettings.BGGImgUrlRoot, "") : null;
				r.Expansions = r.Expansions.Select(e => { e.ImageUrl = e.ImageUrl != null ? r.ImageUrl.Replace(_appSettings.BGGImgUrlRoot, "") : null; return e; }).ToList();
				return r; 
			}).ToList();

			var result = await _repo.Edit(lot);
			
			// Re-attach the url root
			if (result?.Items != null)
			{
				result.Items = result.Items.Select(r => { 
					r.ImageUrl = r.ImageUrl != null ? _appSettings.BGGImgUrlRoot + r.ImageUrl : null;
					r.Expansions = r.Expansions.Select(e => { e.ImageUrl = e.ImageUrl != null ? _appSettings.BGGImgUrlRoot + e.ImageUrl : null; return e; }).ToList();
					return r; 
				}).ToList();
			}

			return result;
		}

		public async Task<Lot> Get(int id, bool isAdmin, int sellerId)
		{
			if (id <= 0) { return null; }

			var result = await _repo.Get(id);

			// Attach the url root so clients don't have to know where they are stored
			if (result?.Items != null)
			{
				result.Items = result?.Items.Select(r =>
				{
					r.ImageUrl = _appSettings.BGGImgUrlRoot + r.ImageUrl;
					r.Expansions = r.Expansions.Select(e => { e.ImageUrl = e.ImageUrl != null ? _appSettings.BGGImgUrlRoot + e.ImageUrl : null; return e; }).ToList();
					return r;
				}).ToList();
			}

			return isAdmin ? result : result.ForNonAdmins(sellerId);
		}

		public async Task<List<Lot>> GetAll(int auctionId, bool isAdmin, int sellerId)
		{
			if (auctionId <= 0) { return null; }

			var result = await _repo.GetAll(auctionId);

			// Attach the url root so clients don't have to know where they are stored
			result = result.Select(i => { 
				i.Items.Select(r => { 
					r.ImageUrl = _appSettings.BGGImgUrlRoot + r.ImageUrl;
					r.Expansions = r.Expansions.Select(e => { e.ImageUrl = e.ImageUrl != null ? _appSettings.BGGImgUrlRoot + e.ImageUrl : null; return e; }).ToList();
					return r; 
				}).ToList(); 
				return i; 
			}).ToList();

			return isAdmin ? result : result.Select(x => x.ForNonAdmins(sellerId)).ToList();
		}

		public async Task<bool> MakeActive(int id, int auctionId)
		{
			if (id <= 0) { throw new ArgumentException("Invalid lot id", "id"); }
			if (auctionId <= 0) { throw new ArgumentException("Invalid auction id", "auctionId"); }

			return await _repo.MakeActive(id, auctionId);
		}

		public async Task<Lot> Sell(int id, double price)
		{
			if (id <= 0 || price <= 0) { throw new ArgumentException("Invalid price", "price"); }

			return await _repo.Sell(id, price);
		}
	}
}
