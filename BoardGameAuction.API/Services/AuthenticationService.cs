﻿using BoardGameAuction.API.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace BoardGameAuction.API.Services
{
	public class AuthenticationService : IAuthenticationService
	{
		private readonly ILogger<AuthenticationService> _logger;
		private readonly AppSettings _appSettings;

		public AuthenticationService(ILogger<AuthenticationService> logger, IOptions<AppSettings> appSettings)
		{
			_logger = logger;
			_appSettings = appSettings.Value;
		}

		public string CreateToken(IEnumerable<Claim> claims)
		{
			// Encrypt and return bearer
			var tokenHandler = new JwtSecurityTokenHandler();
			var key = Encoding.ASCII.GetBytes(_appSettings.JWTTokenSecret);
			var tokenDescriptor = new SecurityTokenDescriptor
			{
				Subject = new ClaimsIdentity(claims.ToArray()),
				Issuer = _appSettings.JWTIssuer,
				Audience = _appSettings.JWTAudience,
				IssuedAt = DateTime.UtcNow,
				Expires = DateTime.UtcNow.AddDays(7),
				SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
			};
			var token = tokenHandler.CreateToken(tokenDescriptor);

			return tokenHandler.WriteToken(token);
		}
	}
}