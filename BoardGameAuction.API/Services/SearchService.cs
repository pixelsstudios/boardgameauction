﻿using BoardGameAuction.API.Repositories;
using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.API.Services.Interfaces;
using BoardGameAuction.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BoardGameAuction.API.Services
{
	public class SearchService : ISearchService
	{
		private readonly AppSettings _appSettings;
		private readonly ISearchRepository _searchRepository;
		private readonly IProductRepository _productRepository;

		public SearchService(IOptions<AppSettings> appSettings, ISearchRepository searchRepository, IProductRepository productRepository)
		{
			this._appSettings = appSettings.Value;
			this._searchRepository = searchRepository;
			this._productRepository = productRepository;
		}

		public async Task<NameSearchResponse> Search(string query)
		{
			return await _searchRepository.GetByName(query);
		}

		/// <summary>
		/// Looks up the supplied BGG Ids in the local database and reduces them down
		/// to those that were not found or at least one item in the database has not
		/// been checked recently enough. These Ids are then used to get up-to-date
		/// data from BGG and update the local database.
		/// </summary>
		/// <param name="bggIds"></param>
		/// <returns></returns>
		public async Task<IdSearchResponse> Search(List<int> bggIds)
		{
			int maxDataAge = _appSettings.MaxDataAge;
			string bggItemUrl = _appSettings.BGGItemUrl;

			// Get all the items that are already in the database and within the time limit
			var responseItems = await _productRepository.Get(bggIds, DateTime.UtcNow.Date.AddDays(maxDataAge * -1));

			// Query BGG with a list of the Ids that need data
			var listOfIds = bggIds.Except(responseItems.Select(i => i.BGGId));
			var updatedItems = (await _searchRepository.GetByBGGId(listOfIds))?.Items;

			if (updatedItems == null || updatedItems.Any() == false)
			{
				return new IdSearchResponse { Items = PrepareForResult(responseItems).ToList() };
			}

			// The item in the database is too old. Update the result
			var toUpdate = await _productRepository.Get(updatedItems.Select(i => i.BGGId));
			var better = PrepareForDatabase(toUpdate);
			await _productRepository.Edit(better);

			// The item could not be found in the database. Store the result
			var toAdd = updatedItems.Where(l2 => toUpdate.All(l1 => l1.BGGId != l2.BGGId));
			await _productRepository.Add(PrepareForDatabase(toAdd));

			return new IdSearchResponse { Items = PrepareForResult(responseItems.ToList().Concat(updatedItems)).ToList() };
		}

		/// <summary>
		/// Adds the static image url root stored in the settings to all
		/// images, ready for serializing in results
		/// </summary>
		/// <param name="products">The list of Products to be prepared</param>
		/// <returns></returns>
		public IEnumerable<Product> PrepareForResult(IEnumerable<Product> products)
		{
			var root = _appSettings.BGGImgUrlRoot;

			return products.Select(p =>
			{
				p.ImageUrl = p.ImageUrl == null ? null : p.ImageUrl.StartsWith(root) ? p.ImageUrl : root + p.ImageUrl;
				p.Expansions = p.Expansions.Select(e =>
				{
					e.ImageUrl = e.ImageUrl == null ? null : e.ImageUrl.StartsWith(root) ? e.ImageUrl : root + e.ImageUrl;
					e.Versions = e.Versions.Select(v =>
					{
						v.ImageUrl = v.ImageUrl == null ? null : v.ImageUrl.StartsWith(root) ? v.ImageUrl : root + v.ImageUrl;
						return v;
					}).OrderBy(o => o.VersionName).ThenBy(o => o.BGGId).ToList();
					return e;
				}).OrderBy(o => o.Name).ThenBy(o => o.BGGId).ToList();
				p.Versions = p.Versions.Select(v =>
				{
					v.ImageUrl = v.ImageUrl == null ? null : v.ImageUrl.StartsWith(root) ? v.ImageUrl : root + v.ImageUrl;
					return v;
				}).OrderBy(o => o.VersionName).ThenBy(o => o.BGGId).ToList();
				return p;
			}).OrderBy(o => o.Name).ThenBy(o => o.BGGId);
		}

		public List<ProductTable> PrepareForDatabase(IEnumerable<Product> products)
		{
			// Add each version of the product
			List<ProductTable> flatMap = products.SelectMany(i => i.Versions, (i, v) => new ProductTable
			{
				BGGId = i.BGGId,
				VersionId = v.VersionId,
				Name = i.Name,
				VersionName = v.VersionName,
				ImageUrl = v.ImageUrl,
				Year = v.Year,
				Rating = v.Rating,
				Weight = v.Weight,
				LastChecked = DateTime.UtcNow.Date
			}).ToList();

			// If the main product has a version that is not listed in Versions, add it
			if (products.Any(p => p.VersionId > 0 && p.Versions.All(v => v.VersionId != p.VersionId)))
			{
				flatMap.AddRange(products.Where(p => p.VersionId > 0 && p.Versions.All(v => v.VersionId != p.VersionId)).Select(p => new ProductTable
				{
					BGGId = p.BGGId,
					VersionId = p.VersionId,
					Name = p.Name,
					VersionName = p.VersionName,
					ImageUrl = p.ImageUrl,
					Year = p.Year,
					Rating = p.Rating,
					Weight = p.Weight,
					LastChecked = DateTime.UtcNow.Date
				}).ToList());
			}

			foreach (var product in products)
			{
				if (product.Expansions?.Count > 0)
				{
					// Add each version of each expansion
					flatMap.AddRange(product.Expansions.SelectMany(e => e.Versions, (e, v) => new ProductTable
					{
						ParentBGGId = product.BGGId,
						BGGId = e.BGGId,
						VersionId = v.VersionId,
						Name = e.Name,
						VersionName = v.VersionName,
						ImageUrl = v.ImageUrl,
						Year = v.Year != 0 ? v.Year : e.Year,
						Rating = v.Rating,
						Weight = v.Weight,
						LastChecked = DateTime.UtcNow.Date
					}).ToList());

					// If the expansion has a version that is not listed in Versions, add it
					if (product.Expansions.Any(e => e.VersionId > 0 && e.Versions.All(v => v.VersionId != e.VersionId)))
					{
						flatMap.AddRange(product.Expansions.Where(e => e.VersionId > 0 && e.Versions.All(v => v.VersionId != e.VersionId)).Select(e => new ProductTable
						{
							ParentBGGId = product.BGGId,
							BGGId = e.BGGId,
							VersionId = e.VersionId,
							Name = e.Name,
							VersionName = e.VersionName,
							ImageUrl = e.ImageUrl,
							Year = e.Year,
							Rating = e.Rating,
							Weight = e.Weight,
							LastChecked = DateTime.UtcNow.Date
						}).ToList());
					}
				}
			}

			flatMap = flatMap.Select(r =>
			{
				// Remove root from Urls before they are stored
				r.ImageUrl = r.ImageUrl == null ? r.ImageUrl : r.ImageUrl.Replace(_appSettings.BGGImgUrlRoot, "");
				return r;
			}).ToList();

			return flatMap;
		}
	}
}