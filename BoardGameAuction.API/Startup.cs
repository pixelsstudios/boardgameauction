using BoardGameAuction.API.Repositories;
using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.API.Services;
using BoardGameAuction.API.Services.Interfaces;
using BoardGameAuction.Models.Authorisation;
using BoardGameAuction.Models.Types;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Polly;
using Polly.Extensions.Http;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Claims;
using System.Text;

namespace BoardGameAuction.API
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			var appSettingsSection = Configuration.GetSection("AppSettings");
			services.Configure<AppSettings>(appSettingsSection);

			// configure jwt authentication
			var appSettings = appSettingsSection.Get<AppSettings>();
			var key = Encoding.ASCII.GetBytes(appSettings.JWTTokenSecret);
			services.AddAuthentication(x =>
			{
				x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			})
			.AddJwtBearer(x =>
			{
				x.RequireHttpsMetadata = false;
				x.SaveToken = true;
				x.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuerSigningKey = true,
					IssuerSigningKey = new SymmetricSecurityKey(key),
					ValidateIssuer = true,
					ValidIssuer = appSettingsSection.GetValue<string>("JWTIssuer"),
					ValidateAudience = true,
					ValidAudience = appSettingsSection.GetValue<string>("JWTAudience"),
				};
			});
			
			services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();

			services.AddScoped<IAccountRepository, AccountRepository>();
			services.AddScoped<IAuctionRepository, AuctionRepository>();
			services.AddScoped<ILotRepository, LotRepository>();
			services.AddScoped<IItemRepository, ItemRepository>();
			services.AddScoped<IExpansionRepository, ExpansionRepository>();
			services.AddScoped<IProductRepository, ProductRepository>();
			services.AddHttpClient<ISearchRepository, SearchRepository>(GetRequestConfig())
				.SetHandlerLifetime(TimeSpan.FromMinutes(5))
				.AddPolicyHandler(GetRetryPolicy())
				.AddPolicyHandler(GetCircuitBreakerPolicy());

			services.AddScoped<IAuthenticationService, AuthenticationService>();
			services.AddScoped<IPasswordService, PasswordService>();
			services.AddScoped<IAccountService, AccountService>();
			services.AddScoped<IAuctionService, AuctionService>();
			services.AddScoped<ILotService, LotService>();
			services.AddScoped<IItemService, ItemService>();
			services.AddScoped<IExpansionService, ExpansionService>();
			services.AddScoped<ISearchService, SearchService>();

			services.AddControllers();

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo
				{
					Title = "Board Game Auction API",
					Version = "v1",
					Description = $""
				});
				c.CustomSchemaIds(x => x.FullName);

				// Set the comments path for the Swagger JSON and UI.
				var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
				var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
				c.IncludeXmlComments(xmlPath);
			});

			services.AddControllers(mvcOptions => mvcOptions.EnableEndpointRouting = false)
				.AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

			services.AddAuthorization(options => {
				options.AddPolicy("IsSeller", policy => policy.Requirements.Add(new IsSellerRequirement()));
				options.AddPolicy("IsAdmin", policy => policy.Requirements.Add(new IsAdminRequirement()));
			});
			services.AddSingleton<IAuthorizationHandler, IsSellerRequirement.IsSellerHandler>();
			services.AddSingleton<IAuthorizationHandler, IsAdminRequirement.IsAdminHandler>();
		}

		static Action<HttpClient> GetRequestConfig()
		{
			return config =>
			{
				config.DefaultRequestHeaders.Add(HttpRequestHeader.Accept.ToString(), "application/json");
			};
		}

		static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
		{
			Random jitterer = new Random();
			return HttpPolicyExtensions
				.HandleTransientHttpError()
				.OrResult(msg => msg.StatusCode == HttpStatusCode.NotFound)
				.WaitAndRetryAsync(6,    // exponential back-off plus some jitter
					retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))
								  + TimeSpan.FromMilliseconds(jitterer.Next(0, 100))
				);
		}

		static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
		{
			return HttpPolicyExtensions
				.HandleTransientHttpError()
				.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30));
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				IdentityModelEventSource.ShowPII = true;
			}

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseAuthentication();
			app.UseAuthorization();

			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "Board Game Auction API");
				c.RoutePrefix = string.Empty;
			});

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
