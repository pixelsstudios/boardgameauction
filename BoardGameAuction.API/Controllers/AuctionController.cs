﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BoardGameAuction.API.Services.Interfaces;
using BoardGameAuction.Models;
using BoardGameAuction.Models.Types;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace BoardGameAuction.API.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class AuctionController : ControllerBase
    {
	    private readonly IAuctionService _service;
		private readonly IAuthenticationService _authenticationService;
		private readonly AppSettings _appSettings;

		public AuctionController(IAuctionService service, IAuthenticationService authenticationService, IOptions<AppSettings> appSettings)
	    {
		    _service = service;
			_authenticationService = authenticationService;
			_appSettings = appSettings.Value;
		}

		/// <summary>
		/// Creates a new auction and sets the current user as an administrator
		/// </summary>
		/// <param name="password">A password to join the auction as an administrator</param>
		/// <returns>The bearer token to use in future calls containing claims for the auction and the users role</returns>
		[HttpPost]
	    [AllowAnonymous]
		[Route("add")]
	    public async Task<IActionResult> Add(PasswordRequest password)
	    {
			var auction = await _service.Add(password.Password);

			var claims = User.Claims.ToList();
			claims.Add(new Claim("auction", auction.Id.ToString()));
			claims.Add(new Claim("auctionCode", auction.Code));
			claims.Add(new Claim(ClaimTypes.Role, ((int)RoleType.AuctionAdmin).ToString()));

			return Ok(_authenticationService.CreateToken(claims));
	    }

		/// <summary>
		/// Updates an new auction to the provided details. Empty and null values are left as original value
		/// </summary>
		/// <param name="auction">An auction object with the properties set that want to be changed</param>
		/// <returns>The updated auction</returns>
		[HttpPut]
		[Authorize(Policy = "IsAdmin")]
		public async Task<IActionResult> Edit(Auction auction)
		{
			var auctionClaim = User.FindFirst("auction");
			if (auctionClaim == null || auctionClaim.Value != auction.Id.ToString())
			{
				return BadRequest("Auction Id and claim do not match");
			}
			
			var result = await _service.Edit(auction);
			return Ok(result);
		}

		/// <summary>
		/// Gets the auction stored in the users claims
		/// If they have an administrator role claim it will contain private data
		/// </summary>
		/// <returns>An Auction object with nested lists populated</returns>
	    [HttpGet]
	    public async Task<IActionResult> Get()
	    {
			var auctionId = User.FindFirst("auction");
			var role = User.FindFirst(ClaimTypes.Role);
			var seller = User.FindFirst(ClaimTypes.NameIdentifier);
			if (auctionId != null)
			{
				var result = await _service.Get(
					int.Parse(auctionId.Value), 
					role == null ? 0 : (RoleType)int.Parse(role.Value),
					seller == null ? 0 : int.Parse(seller.Value)
				);
				if (result != null) { return Ok(result); }
			}

			return BadRequest("You aren't in any auctions");
	    }

		/// <summary>
		/// Validates the auction code exists
		/// If true a bearer token is created with the claims
		/// </summary>
		/// <param name="code">The 4-letter auction code</param>
		/// <param name="password">(Optional) The administrator password to get administrator privileges </param>
		/// <returns>The bearer token to use in future calls containing claims for the auction and the users role</returns>
		[HttpGet]
		[AllowAnonymous]
        [Route("join")]
	    public async Task<IActionResult> Join(string code, string password)
	    {
			try
			{
				var seller = User.FindFirst(ClaimTypes.NameIdentifier);
				var auction = await _service.GetByCode(code, RoleType.AuctionAdmin, seller != null ? int.Parse(seller.Value) : 0);

				if (auction != null)
				{
					var claims = User.Claims.ToList();
					claims.Add(new Claim("auction", auction.Id.ToString()));
					claims.Add(new Claim("auctionCode", auction.Code));
					if ((await _service.Join(auction.Id)).Password == password)
					{
						claims.Add(new Claim(ClaimTypes.Role, ((int)RoleType.AuctionAdmin).ToString()));
					}
					else if (claims.Any(c => c.Type == ClaimTypes.NameIdentifier))
					{
						// If we already know the users name upgrade them to a seller
						claims.Add(new Claim(ClaimTypes.Role, ((int)RoleType.Seller).ToString()));
					}

					return Ok(_authenticationService.CreateToken(claims));
				}
			}
			catch (ArgumentException ex)
			{
				return new BadRequestObjectResult(ex);
			}

			return BadRequest($"There isn't an active auction with the code {code}");
		}

		/// <summary>
		/// Removes the auction and role claims from the current user and creates a new bearer token
		/// Other claims may still exist
		/// </summary>
		/// <returns>The bearer token to use in future calls</returns>
		[HttpPut]
		[Authorize]
		[Route("leave")]
	    public IActionResult Leave()
	    {
		    var identity = User.Identity as ClaimsIdentity;
		    foreach (var claim in identity.Claims.Where(c => c.Type == "auction" || c.Type == "auctionCode" || c.Type == ClaimTypes.Role).ToList())
		    {
			    identity.RemoveClaim(claim);
		    }
		    
			return Ok(_authenticationService.CreateToken(identity.Claims));
	    }

		/// <summary>
		/// Sets the start time for an auction. The auction will automatically be set to start at this time
		/// </summary>
		/// <param name="timestamp">The date and time the auction should start</param>
		/// <returns>The Auction object</returns>
		[HttpPut]
		[Authorize(Policy = "IsAdmin")]
		[Route("start")]
		public async Task<IActionResult> Start(DateTime timestamp)
	    {
			var auctionId = User.FindFirst("auction");
			if (auctionId != null)
			{
				var result = await _service.Start(int.Parse(auctionId.Value), timestamp);
				return Ok(result);
			}

			return NotFound("You aren't in any auctions");
		}

		/// <summary>
		/// Sets the end time for an auction. The auction will automatically be set to end at this time
		/// </summary>
		/// <param name="timestamp">The date and time the auction should end</param>
		/// <returns>The Auction object</returns>
		[HttpPut]
		[Authorize(Policy = "IsAdmin")]
		[Route("stop")]
		public async Task<IActionResult> Stop(DateTime timestamp)
	    {
			var auctionId = User.FindFirst("auction");
			if (auctionId != null)
			{
				var result = await _service.Stop(int.Parse(auctionId.Value), timestamp);
				return Ok(result);
			}

			return NotFound("You aren't in any auctions");
		}

		/// <summary>
		/// Gets a list of the sold and un-sold items in an auction
		/// If they have an administrator role claim it will contain private data
		/// </summary>
		/// <returns>An Auction object with nested lists populated</returns>
		[HttpGet]
		[Route("summary")]
		public async Task<IActionResult> Summary()
		{
			var auctionId = User.FindFirst("auction");
			var seller = User.FindFirst(ClaimTypes.NameIdentifier);
			var role = User.FindFirst(ClaimTypes.Role);
			if (auctionId != null)
			{
				var result = await _service.Summary(int.Parse(auctionId.Value), role == null ? 0 : (RoleType)int.Parse(role.Value), seller == null ? 0 : int.Parse(seller.Value));
				return Ok(result);
			}

			return NotFound("You aren't in any auctions");
		}
	}
}