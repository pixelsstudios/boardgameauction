﻿using BoardGameAuction.Models;
using BoardGameAuction.API.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using BoardGameAuction.Models.Types;

namespace BoardGameAuction.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
    public class LotController : ControllerBase
    {
	    private readonly ILotService _service;
		private readonly IAccountService _accountService;

		public LotController(ILotService service, IAccountService accountService)
	    {
		    _service = service;
			_accountService = accountService;
		}

		/// <summary>
		/// Gets all the lots for the current auction
		/// For admins and lot owners this will include all details
		/// For other users it will only contain public information on lots they don't own
		/// </summary>
		/// <returns>A List of Lot objects</returns>
	    [HttpGet]
	    public async Task<IActionResult> GetAll()
	    {
			int auctionId = int.Parse(User.FindFirst("auction").Value);
			var sellerId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
			bool isAdmin = User.FindFirst(x => x.Type == ClaimTypes.Role && x.Value == "admin") != null;
			var result = await _service.GetAll(auctionId, isAdmin, sellerId);
		    return Ok(result);
	    }

		/// <summary>
		/// Gets the specified lot
		/// For admins and lot owners this will include all details. 
		/// For other users it will only contain public information on lots they don't own
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpGet]
	    [Route("{id}")]
	    public async Task<IActionResult> Get(int id)
	    {
			int auctionId = int.Parse(User.FindFirst("auction").Value);
			var sellerId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier) ?? "0");
			bool isAdmin = User.FindFirst(x => x.Type == ClaimTypes.Role && x.Value == "2") != null;
			var result = await _service.Get(id, isAdmin, sellerId);
		    return Ok(result);
	    }

		/// <summary>
		/// Adds a new lot to the current auction
		/// Lots can be populated with child Items, and Items may have Expansions
		/// </summary>
		/// <param name="lot"></param>
		/// <returns>The added Lot with the Id populated</returns>
	    [HttpPost]
		[Authorize(Policy = "IsSeller")]
		[Authorize(Policy = "IsAdmin")]
		public async Task<IActionResult> Add(Lot lot)
	    {
			var auctionClaim = User.FindFirst("auction");
			var roleClaim = User.FindFirst(ClaimTypes.Role);
			var sellerClaim = User.FindFirst(ClaimTypes.NameIdentifier);

			if (auctionClaim == null) { return NotFound("You aren't in any auctions"); }
			lot.AuctionId = int.Parse(auctionClaim.Value);

			if ((RoleType)int.Parse(roleClaim.Value) != RoleType.Seller) { return Unauthorized("You must be logged in as a Seller to list items"); }

			if (sellerClaim == null || int.TryParse(sellerClaim.Value, out int myInt) == false) { return NotFound("Seller names are required before listing lots"); }

			var seller = await _accountService.Get(int.Parse(sellerClaim.Value));
			if (seller == null) { return NotFound($"There's no seller with an id of {sellerClaim}"); }

			// Add the Lot, Item(s) (and Expansions)
			lot.Seller = seller;
			var result = await _service.Add(lot);
			return Ok(result);			
	    }

		/// <summary>
		/// Updates an existing Lot
		/// </summary>
		/// <param name="lot">The Lot as it should be after update</param>
		/// <returns>The edited Lot</returns>
		[HttpPut]
		[Authorize(Policy = "IsSeller")]
		[Authorize(Policy = "IsAdmin")]
		public async Task<IActionResult> Edit(Lot lot)
		{
			// Edit the Lot
			var result = await _service.Edit(lot);
			return Ok(result);
		}

		/// <summary>
		/// Sets the id of the active Lot in an Auction
		/// </summary>
		/// <param name="id">The id of the Lot to be marked as the active Lot</param>
		/// <returns>A bool indicating whether any rows were updated. False may indicate the auction could not be found</returns>
		[HttpPut]
		[Route("{id}/makeactive")]
		[Authorize(Policy = "IsAdmin")]
		public async Task<IActionResult> MakeActive(int id)
		{
			var auctionClaim = User.FindFirst("auction");
			if (auctionClaim == null) { return BadRequest("You aren't in any auctions"); }

			var result = await _service.MakeActive(id, int.Parse(auctionClaim.Value));
			return Ok(result);
		}

		/// <summary>
		/// Marks an item as sold or no-sale. -1 is a no-sale.
		/// 0 and above are sale values
		/// </summary>
		/// <param name="id">The id of the lot being sold</param>
		/// <param name="price">The value sold for. -1 denotes a no-sale</param>
		/// <returns></returns>
		[HttpPut]
		[Route("{id}/sell")]
		[Authorize(Policy = "IsAdmin")]
		public async Task<IActionResult> Sell(int id, double price)
		{
			// Set the Lot sold price
			var result = await _service.Sell(id, price);
			return Ok(result);
		}

		/// <summary>
		/// Deletes a Lot
		/// </summary>
		/// <param name="auctionId">The Auction the Lot belongs too. This must match the active Auction</param>
		/// <param name="lotId">The Id of the Lot to delete</param>
		/// <returns></returns>
		[HttpDelete]
		[Route("{lotId}")]
		[Authorize(Policy = "IsSeller")]
		[Authorize(Policy = "IsAdmin")]
		public async Task<IActionResult> Delete(int auctionId, int lotId)
		{
			var auctionClaim = User.FindFirst("auction");
			var roleClaim = User.FindFirst(ClaimTypes.Role);
			var sellerClaim = User.FindFirst(ClaimTypes.NameIdentifier);

			if (auctionClaim == null) { return BadRequest("You aren't in any auctions"); }
			if (auctionClaim.Value != auctionId.ToString()) { return BadRequest("You must be part of the auction to delete lots from it"); }

			if ((RoleType)int.Parse(roleClaim.Value) != RoleType.Seller && (RoleType)int.Parse(roleClaim.Value) != RoleType.AuctionAdmin) { 
				return Unauthorized("You must be logged in as a Seller to delete lots"); 
			}

			if (sellerClaim == null || int.TryParse(sellerClaim.Value, out int myInt) == false) { return BadRequest("Seller ids are required before deleting lots"); }

			var seller = await _accountService.Get(int.Parse(sellerClaim.Value));
			if (seller == null) { return BadRequest($"There's no seller with an id of {sellerClaim.Value}"); }

			// Check the lot, auction and seller all match up
			var lot = await _service.Get(lotId, true, seller.Id);
			if ((RoleType)int.Parse(roleClaim.Value) != RoleType.Seller || seller.Id != lot.Seller?.Id) { return BadRequest("Only admins and the sellers of lots can delete them"); }
			
			var result = await _service.Delete(auctionId, lotId);
			return Ok(result);
		}
	}
}