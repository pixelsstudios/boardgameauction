﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BoardGameAuction.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class HeartbeatController : Controller
    {
        /// <summary>
        /// Returns a simple 200 OK if the service is running
        /// </summary>
        /// <returns>A 200 OK response with the word "Heartbeat"</returns>
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Index()
        {
            return Ok("Heartbeat");
        }
    }
}