﻿using System.Linq;
using System.Threading.Tasks;
using BoardGameAuction.API.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BoardGameAuction.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class SearchController : Controller
    {
		private readonly ISearchService _service;

		public SearchController(ISearchService service)
		{
			this._service = service;
		}

		/// <summary>
		/// Gets BGG items from their text search
		/// </summary>
		/// <param name="query">The search phrase</param>
		/// <returns>A NameSearchRespons of matches. Each item is the abstract of a game, no particular version</returns>
		[HttpGet]
        public async Task<IActionResult> Search(string query)
        {
			return Ok(await _service.Search(query));
        }

		/// <summary>
		/// Gets a BGG item by the BGGId
		/// </summary>
		/// <param name="id">The Id of an item on www.boardgamegeek.com</param>
		/// <returns>An IdSearchResponse of matches. Each item is an instance of the game, usually a version</returns>
		[HttpGet]
		[Route("byid")]
		public async Task<IActionResult> GetProducts([FromQuery] int[] id)
		{
			if (id.Length == 0)
			{
				return BadRequest("No ids provided");
			}
			return Ok(await _service.Search(id.ToList()));
		}
	}
}