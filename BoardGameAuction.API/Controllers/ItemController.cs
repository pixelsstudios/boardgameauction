﻿using BoardGameAuction.Models;
using BoardGameAuction.API.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using BoardGameAuction.Models.Types;

namespace BoardGameAuction.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class ItemController : ControllerBase
	{
		private readonly IItemService _service;
		private readonly IAccountService _accountService;

		public ItemController(IItemService service, IAccountService accountService)
		{
			_service = service;
			_accountService = accountService;
		}

		/// <summary>
		/// Adds an Item to a Lot. Items may include child Expansions
		/// </summary>
		/// <param name="item">The populated Item to add</param>
		/// <returns>The added Item with Id populated</returns>
		[HttpPost]
		[Authorize(Policy = "IsSeller")]
		[Authorize(Policy = "IsAdmin")]
		public async Task<IActionResult> Add(Item item)
		{
			var auctionClaim = User.FindFirst("auction");
			var roleClaim = User.FindFirst(ClaimTypes.Role);
			var sellerClaim = User.FindFirst(ClaimTypes.NameIdentifier);

			if (item.LotId == 0) { return BadRequest("An Item must be assigned to a Lot"); }

			if (auctionClaim == null) { return NotFound("You aren't in any auctions"); }

			if ((RoleType)int.Parse(roleClaim.Value) != RoleType.Seller) { return Unauthorized("You must be logged in as a Seller to list items"); }

			if (sellerClaim == null || int.TryParse(sellerClaim.Value, out int myInt) == false) { return NotFound("Seller names are required before listing lots"); }

			var seller = await _accountService.Get(int.Parse(sellerClaim.Value));
			if (seller == null) { return NotFound($"There's no seller with an id of {sellerClaim}"); }

			// Add the Item (and Expansions)
			var result = await _service.Add(item, int.Parse(auctionClaim.Value));
			return Ok(result);
		}

		/// <summary>
		/// Deletes an Item from a Lot. The LotId is required to ensure that it matches the lot of the Item
		/// </summary>
		/// <param name="id">The Id of the Item to delete</param>
		/// <param name="lotId">The Id of the Lot</param>
		/// <returns>True is any rows were deleted. Otherwise False</returns>
		[HttpDelete]
		[Authorize(Policy = "IsSeller")]
		[Authorize(Policy = "IsAdmin")]
		[Route("{id}")]
		public async Task<IActionResult> Delete(int id, int lotId)
		{
			var auctionClaim = User.FindFirst("auction");
			var roleClaim = User.FindFirst(ClaimTypes.Role);
			var sellerClaim = User.FindFirst(ClaimTypes.NameIdentifier);

			if (auctionClaim == null) { return NotFound("You aren't in any auctions"); }

			if ((RoleType)int.Parse(roleClaim.Value) != RoleType.Seller) { return Unauthorized("You must be logged in as a Seller to edit items"); }

			if (sellerClaim == null || int.TryParse(sellerClaim.Value, out int myInt) == false) { return NotFound("Seller names are required to edit lots"); }

			var seller = await _accountService.Get(int.Parse(sellerClaim.Value));
			if (seller == null) { return NotFound($"There's no seller with an id of {sellerClaim}"); }

			var result = await _service.Delete(id, lotId);
			return Ok(result);
		}
	}
}