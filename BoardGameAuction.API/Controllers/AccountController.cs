﻿using BoardGameAuction.API.Services.Interfaces;
using BoardGameAuction.Models;
using BoardGameAuction.Models.Types;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BoardGameAuction.API.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
	{
		private readonly IAccountService _service;
		private readonly IAuctionService _auctionService;
		private readonly IAuthenticationService _authenticationService;
		private readonly AppSettings _appSettings;

		public AccountController(IAccountService service, IAuctionService auctionService, IAuthenticationService authenticationService, IOptions<AppSettings> appSettings)
		{
			_service = service;
			_auctionService = auctionService;
			_authenticationService = authenticationService;
			_appSettings = appSettings.Value;
		}

		/// <summary>
		/// Registers the user as a Seller. Admins are currently a
		/// shared password attached to the auction
		/// </summary>
		/// <param name="model">The name and password to register with</param>
		/// <returns>A bearer token to be used on all subsequent requests 
		/// containing the new credentials and roles</returns>
		[HttpPost]
		[AllowAnonymous]
		[Route("register")]
		public async Task<IActionResult> Register([FromBody] RegisterRequest model)
		{
			if (model.Password != model.ConfirmPassword)
			{
				ModelState.AddModelError("ConfirmPassword", "The passwords do not match");
			}
			
			if (ModelState.IsValid == false)
			{
				var allErrors = ModelState.Values.SelectMany(v => v.Errors);
				return BadRequest(allErrors);
			}

			try
			{
				var response = await _service.Add(model.Forename, model.Surname, model.Password);
				return Ok(new RegisterResponse { Id = response.Id, Message = response.Message });
			}
			catch
			{
				throw new Exception("User creation failed");
			}
		}

		/// <summary>
		/// Logs in as an existing user
		/// </summary>
		/// <param name="model">The name and password to login with</param>
		/// <returns>A bearer token to be used on all subsequent requests 
		/// containing the new credentials and roles</returns>
		[HttpPost]
		[AllowAnonymous]
		[Route("login")]
		public async Task<IActionResult> Login([FromBody] LoginRequest model)
		{
			// Do all the processes that could take time up-front to prevent request time 
			// sniffing to identify valid accounts with incorrect passwords
			var userId = await _service.GetId(model.Forename, model.Surname, model.Password);
			var user = await _service.Get(userId);

			if (ModelState.IsValid == false)
			{
				var allErrors = ModelState.Values.SelectMany(v => v.Errors);
				return BadRequest(allErrors);
			}
			if (userId == 0)
			{
				return Unauthorized("Wrong name and/or password");
			}

			var claims = User.Claims.Where(c => c.Type != ClaimTypes.Name && c.Type != ClaimTypes.NameIdentifier).ToList();
			claims.Add(new Claim(ClaimTypes.Name, $"{model.Forename} {model.Surname}"));
			claims.Add(new Claim(ClaimTypes.NameIdentifier, userId.ToString()));

			// If the user is already in an auction, updgrade them to a seller role
			if (claims.Any(c => c.Type == "auction"))
			{
				if (claims.Any(c => c.Type == ClaimTypes.Role))
				{
					claims.RemoveAll(c => c.Type == ClaimTypes.Role);
				}
				claims.Add(new Claim(ClaimTypes.Role, ((int)RoleType.Seller).ToString()));
			}
			return Ok(_authenticationService.CreateToken(claims));
		}

		/// <summary>
		/// Upgrades a user in an auction to an admin
		/// </summary>
		/// <param name="model">The id of the auction and the password</param>
		/// <returns>A bearer token to be used on all subsequent requests 
		/// containing the new credentials and roles</returns>
		[HttpPost]
		[Authorize]
		[Route("becomeadmin")]
		public async Task<IActionResult> BecomeAdmin([FromBody] BecomeAdminRequest model)
		{
			// Do all the processes that could take time up-front to prevent request time 
			// sniffing to identify valid accounts with incorrect passwords
			var auction = await _auctionService.Get(int.Parse(User.FindFirst(c => c.Type == "auction")?.Value), RoleType.AuctionAdmin, 0);
			bool authenticated = await _auctionService.AuthenticateAdmin(auction?.Id ?? 0, model.Password);

			if (User.HasClaim(c => c.Type == "auction") == false)
			{
				return Unauthorized("You must be in an auction to become an admin of it");
			}

			if (auction == null || auction.Password == null)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, new { ReasonPhrase = "Failed to retrieve the auction" });
			}

			if (authenticated == false)
			{
				return Unauthorized("Wrong password");
			}

			if (ModelState.IsValid == false)
			{
				var allErrors = ModelState.Values.SelectMany(v => v.Errors);
				return BadRequest(allErrors);
			}

			var claims = User.Claims.Where(c => c.Type != ClaimTypes.Role).ToList();
			claims.Add(new Claim(ClaimTypes.Role, ((int)RoleType.AuctionAdmin).ToString()));

			return Ok(_authenticationService.CreateToken(claims));
		}

		/// <summary>
		/// Gets the balance for a seller in the current auction
		/// </summary>
		/// <param name="sellerId">The Id of the seller whose balance will be returned</param>
		/// <returns>The total of all sales of lots owned by the specified user in the current auction</returns>
		[HttpGet]
		[Authorize(Policy = "IsAdmin")]
		[Route("balance/{sellerId}")]
		public async Task<IActionResult> Balance(int sellerId)
		{
			if (User.HasClaim(c => c.Type == "auction") == false)
			{
				return Unauthorized("You must be in an auction to see the balance");
			}

			var auctionId = int.Parse(User.FindFirst(c => c.Type == "auction").Value);
			var summary = await _auctionService.Summary(auctionId, RoleType.AuctionAdmin, sellerId);

			if (summary?.Lots?.Count == 0)
			{
				return Ok(0);
			}

			return Ok(summary.Lots.Where(x => x?.Seller?.Id == sellerId && x.SalePrice != null && x.SalePrice >= 0).Sum(s => s.SalePrice));
		}

		/// <summary>
		/// Gets the balance for the current user in the current auction
		/// </summary>
		/// <returns>The total of all sales of lots owned by the current user in the current auction</returns>
		[HttpGet]
		[Authorize(Policy = "IsSeller")]
		[Route("balance")]
		public async Task<IActionResult> Balance()
		{
			if (User.HasClaim(c => c.Type == "auction") == false)
			{
				return Unauthorized("You must be in an auction to see your balance");
			}

			if (User.HasClaim(c => c.Type == ClaimTypes.Role && c.Value == ((int)RoleType.Seller).ToString()) == false)
			{
				return Unauthorized("Only sellers can see their balance");
			}

			if (User.HasClaim(c => c.Type == ClaimTypes.NameIdentifier) == false)
			{
				return Unauthorized("Could not find your seller Id");
			}

			var auctionId = int.Parse(User.FindFirst(c => c.Type == "auction").Value);
			var sellerId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
			var summary = await _auctionService.Summary(auctionId, RoleType.AuctionAdmin, sellerId);

			if (summary?.Lots?.Count == 0)
			{
				return Ok(0);
			}

			return Ok(summary.Lots.Where(x => x?.Seller?.Id == sellerId && x.SalePrice != null && x.SalePrice >= 0).Sum(s => s.SalePrice));
		}
	}
}