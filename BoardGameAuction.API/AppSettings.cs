﻿namespace BoardGameAuction.API
{
    public class AppSettings
    {
        public string JWTTokenSecret { get; set; }
        public string JWTIssuer { get; set; }
        public string JWTAudience { get; set; }
        public string ConnectionString { get; set; }
        public int MaxDataAge { get; set; }
        public string BGGSearchUrl { get; set; }
        public string BGGItemUrl { get; set; }
        public string BGGImgUrlRoot { get; set; }
    }
}