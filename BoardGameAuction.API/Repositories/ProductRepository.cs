﻿using BoardGameAuction.Models;
using BoardGameAuction.API.Repositories.Interfaces;
using Dapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Data;

namespace BoardGameAuction.API.Repositories
{
	public class ProductRepository : IProductRepository
	{
		private readonly AppSettings _appSettings;

		public ProductRepository(IOptions<AppSettings> appSettings)
		{
			this._appSettings = appSettings.Value;
		}

		public async Task<List<Product>> Add(IEnumerable<ProductTable> products)
		{
			if (products.Count() == 0) { return new List<Product>(); }

			DataTable productList = new DataTable();
			productList.Columns.Add(new DataColumn("ParentBGGId", typeof(Int32)));
			productList.Columns.Add(new DataColumn("BGGId", typeof(Int32)));
			productList.Columns.Add(new DataColumn("VersionId", typeof(Int32)));
			productList.Columns.Add(new DataColumn("Name", typeof(string)));
			productList.Columns.Add(new DataColumn("VersionName", typeof(string)));
			productList.Columns.Add(new DataColumn("ImageUrl", typeof(string)));
			productList.Columns.Add(new DataColumn("Year", typeof(Int32)));
			productList.Columns.Add(new DataColumn("Rating", typeof(decimal)));
			productList.Columns.Add(new DataColumn("Weight", typeof(decimal)));
			productList.Columns.Add(new DataColumn("LastChecked", typeof(DateTime)));
			var today = DateTime.Today;
			foreach (var product in products)
			{
				productList.Rows.Add(product.BGGId, product.VersionId, product.ParentBGGId, product.Name, product.VersionName, product.ImageUrl, product.Year, product.Rating, product.Weight, today);
			}

			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				IEnumerable<Product> result;
				try
				{
					result = await connection.QueryAsync<Product>(
						"[dbo].[ProductAdd]",
						new { product = productList },
						commandType: CommandType.StoredProcedure
					);

					return result.ToList();
				}
				catch (SqlException ex)
				{
					// Permissions and other SQL issues are easy to miss, so this helps
					throw ex;
				}
			}
		}

		public async Task<List<Product>> Edit(IEnumerable<ProductTable> products)
		{
			if (products.Count() == 0) { return new List<Product>(); }

			DataTable productList = new DataTable();
			productList.Columns.Add(new DataColumn("ParentBGGId", typeof(Int32)));
			productList.Columns.Add(new DataColumn("BGGId", typeof(Int32)));
			productList.Columns.Add(new DataColumn("VersionId", typeof(Int32)));
			productList.Columns.Add(new DataColumn("Name", typeof(string)));
			productList.Columns.Add(new DataColumn("VersionName", typeof(string)));
			productList.Columns.Add(new DataColumn("ImageUrl", typeof(string)));
			productList.Columns.Add(new DataColumn("Year", typeof(Int32)));
			productList.Columns.Add(new DataColumn("Rating", typeof(decimal)));
			productList.Columns.Add(new DataColumn("Weight", typeof(decimal)));
			productList.Columns.Add(new DataColumn("LastChecked", typeof(DateTime)));
			var today = DateTime.Today;
			foreach (var product in products)
			{
				productList.Rows.Add(product.BGGId, product.VersionId, product.ParentBGGId, product.Name, product.VersionName, product.ImageUrl, product.Year, product.Rating, product.Weight, today);
			}

			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				IEnumerable<Product> result;
				try
				{
					result = await connection.QueryAsync<Product>(
						"[dbo].[ProductUpdate]",
						new { product = productList },
						commandType: CommandType.StoredProcedure
					);

					return result.ToList();
				}
				catch (SqlException ex)
				{
					// Permissions and other SQL issues are easy to miss, so this helps
					throw ex;
				}
			}
		}

		/// <summary>
		/// Returns items from the database matching the supplied BGG Ids that
		/// were checked within the number of days specified in appSettings
		/// </summary>
		/// <param name="ids">A list of BGG Ids</param>
		/// <returns>A List of items that were found in the database</returns>
		public async Task<List<Product>> Get(IEnumerable<int> ids)
		{
			DataTable idList = new DataTable();
			idList.Columns.Add(new DataColumn("Value"));
			foreach (var id in ids)
			{
				idList.Rows.Add(id);
			}

			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = new List<Product>();
				try
				{
					await connection.QueryAsync<Product>(
						"[dbo].[ProductGet]",
						new Type[]
						{
							typeof(int?),
							typeof(Product)
						},
						MapResult(result),
						new { Ids = idList },
						commandType: CommandType.StoredProcedure,
						splitOn: "BGGId"
					);

					return result.ToList();
				}
				catch (SqlException ex)
				{
					// Permissions and other SQL issues are easy to miss, so this helps
					throw ex;
				}
			}
		}

		/// <summary>
		/// Returns items from the database matching the supplied BGG Ids that
		/// were checked before the date given. The whole Product will be 
		/// returned if any Version of the Product or any Version of an  
		/// Expansion of the Product are past the date 
		/// </summary>
		/// <param name="ids">A list of BGG Ids</param>
		/// <param name="noOlderThanDate">The date since when they must have been 
		/// checked in order to be returned</param>
		/// <returns>A List of items that were found and have been checked since the specified date</returns>
		public async Task<List<Product>> Get(IEnumerable<int> ids, DateTime noOlderThanDate)
		{
			DataTable idList = new DataTable();
			idList.Columns.Add(new DataColumn("Value"));
			foreach (var id in ids)
			{
				idList.Rows.Add(id);
			}

			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = new List<Product>();
				try
				{
					await connection.QueryAsync<Product>(
						"[dbo].[ProductGetLimitedToDate]",
						new Type[]
						{
							typeof(int?),
							typeof(Product)
						},
						MapResult(result),
						new { 
							Ids = idList,
							NoOlderThanDate = noOlderThanDate
						},
						commandType: CommandType.StoredProcedure,
						splitOn: "BGGId"
					);

					return result.ToList();
				}
				catch (SqlException ex)
				{
					// Permissions and other SQL issues are easy to miss, so this helps
					throw ex;
				}
			}
		}

		/// <summary>
		/// Maps the flat data from the database to a hierarchical object
		/// Data must be ordered so that parents are before children
		/// </summary>
		/// <param name="result"></param>
		/// <returns></returns>
		private Func<object[], Product> MapResult(List<Product> result)
		{
			return (obj) =>
			{
				int? parentId = (int?)obj[0];
				Product thing = (Product)obj[1];

				Product product;
				if (thing != null)
				{
					if (result.Any(f => f.BGGId == thing.BGGId))
					{
						// Set product as this instances product
						product = result.First(f => f.BGGId == thing.BGGId);
					}
					else if (result.Any(f => f.BGGId == parentId))
					{
						// Set product as the expansion
						if (result.First(p => p.BGGId == parentId).Expansions.Any(e => e.BGGId == thing.BGGId))
						{
							product = result.First(f => f.BGGId == parentId).Expansions.First(e => e.BGGId == thing.BGGId);
						}
						else
						{
							product = thing.Clone();
							result.First(p => p.BGGId == parentId).Expansions.Add(product);
						}
					}
					else
					{
						// Create product from game or expansion
						product = thing.Clone();
						result.Add(product);
					}

					// A game version
					if (product.Versions.Any(v => v.VersionId == thing.VersionId))
					{
						thing = product.Versions.First(v => v.VersionId == thing.VersionId);
					}
					else
					{
						product.Versions.Add(thing);
					}
				}
				
				return null;
			};
		}
	}

	public class ProductTable : Product
	{
		public int? ParentBGGId { get; set; }
		public DateTime LastChecked { get; set; }
	}
}