﻿using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BoardGameAuction.API.Repositories
{
	public class SearchRepository : ISearchRepository
	{
		private readonly HttpClient _httpClient;
		private readonly AppSettings _appSettings;

		public SearchRepository(HttpClient httpClient, IOptions<AppSettings> appSettings)
		{
			_httpClient = httpClient;
			this._appSettings = appSettings.Value;
		}

		public async Task<NameSearchResponse> GetByName(string query)
		{
			query = query.Replace(" ", "+");

			// Search BGG
			var response = await _httpClient.GetAsync(string.Format(_appSettings.BGGSearchUrl, query));

			if (response.IsSuccessStatusCode == false)
			{
				throw new Exception(response.ReasonPhrase);
			}

			XmlSerializer serializer = new XmlSerializer(typeof(List<Models.BGG.Item>), new XmlRootAttribute("items"));
			var stringReader = new StringReader(await response.Content.ReadAsStringAsync());
			var itemList = (List<Models.BGG.Item>)serializer.Deserialize(stringReader);

			var result = itemList
				.Where(i => i.Id.ToString() != query)
				.Select(i => new ProductBase
				{
					BGGId = int.Parse(i.Id),
					Name = i.Name.Value,
					Year = i.YearPublished != null ? int.Parse(i.YearPublished?.Value) : default
				}
			);

			return new NameSearchResponse { Items = result.ToList() };
		}

		public async Task<IdSearchResponse> GetByBGGId(IEnumerable<int> BGGIds)
		{
			if (BGGIds.Count() == 0) { return null; }

			var response = await _httpClient.GetAsync(string.Format(_appSettings.BGGItemUrl, string.Join(",", BGGIds)));

			if (response.IsSuccessStatusCode == false)
			{
				throw new Exception(response.ReasonPhrase);
			}

			XmlSerializer serializer = new XmlSerializer(typeof(List<Models.BGG.Item>), new XmlRootAttribute("items"));
			var stringReader = new StringReader(await response.Content.ReadAsStringAsync());
			var itemList = (List<Models.BGG.Item>)serializer.Deserialize(stringReader);

			var result = itemList.Select(i => new Product
			{
				BGGId = int.Parse(i.Id),
				Name = i.Name.Value,
				Year = i.YearPublished != null ? int.Parse(i.YearPublished?.Value) : default,
				VersionId = i.Versions.Item.Any() ? int.Parse(i.Versions.Item.First().Id) : default,
				VersionName = i.Versions.Item.Any() ? i.Versions.Item.First().Name.Value : default,
				Versions = i.Versions.Item.Select(v => new Product
				{
					BGGId = int.Parse(i.Id),
					VersionId = int.Parse(v.Id),
					Name = i.Name.Value,
					VersionName = v.Name.Value,
					Year = v.YearPublished != null ? int.Parse(v.YearPublished?.Value) : default,
					ImageUrl = string.IsNullOrWhiteSpace(v.Thumbnail) ? i.Thumbnail : v.Thumbnail,
					Rating = v?.Statistics?.Ratings?.Rating == null || v.Statistics.Ratings.Rating.Value == "0"
						? i?.Statistics?.Ratings?.Rating == null ? 0 : Math.Round(decimal.Parse(i.Statistics?.Ratings?.Rating?.Value), 1)
						: decimal.Parse(v.Statistics.Ratings.Rating.Value),
					Weight = v?.Statistics?.Ratings?.Weight == null || v.Statistics.Ratings.Weight.Value == "0"
						? i?.Statistics?.Ratings?.Weight == null ? 0 : Math.Round(decimal.Parse(i.Statistics?.Ratings?.Weight?.Value), 2)
						: decimal.Parse(v.Statistics.Ratings.Weight.Value)
				}).OrderBy(o => o.Name).ThenBy(o => o.BGGId).ToList(),
				Expansions = 
					i.Type == "boardgame" 
						? i.Link.Where(l => l.Type == "boardgameexpansion")
							.Select(l => new Product
							{
								BGGId = int.Parse(l.Id),
								Name = l.Value,
							}).ToList()
						: null
			});

			var expansionIds = result.Where(i => i.Expansions != null && i.Expansions.Any()).SelectMany(i => i.Expansions.Select(e => e.BGGId));
			if (expansionIds.Count() > 0)
			{
				// Create a list of updated expansions from all items
				var expansions = (await GetByBGGId(expansionIds))?.Items;

				result = result.Select(i =>
				{
					if (i.Expansions != null && i.Expansions.Any())
					{
						// Update expansions to contain all the details
						i.Expansions = expansions.Join(
							i.Expansions.ToList(), 
							a => a.BGGId, 
							b => b.BGGId, 
							(a, b) => new Product 
							{ 
								BGGId = a.BGGId,
								Name = a.Name, 
								Year = a.Year,
								ImageUrl = a?.Versions.Any() ?? false ? a.Versions.First().ImageUrl : a.ImageUrl,
								VersionName = a?.Versions.Any() ?? false ? a.Versions.First().VersionName : "[No Version Data]",
								VersionId = a?.Versions.Any() ?? false ? a.Versions.First().VersionId : 0,
								Rating = a?.Versions.Any() ?? false ? a.Versions.First().Rating : a.Rating,
								Weight = a?.Versions.Any() ?? false ? a.Versions.First().Weight : a.Weight,
								Versions = a?.Versions.Any() ?? false ? a.Versions : new List<Product> { new Product { 
									// If no version was supplied by BGG, create one
									BGGId = a.BGGId,
									Name = a.Name,
									Year = a.Year,
									ImageUrl = a.ImageUrl,
									VersionId = 0,
									VersionName = "[No Version Data]",
									Rating = a.Rating,
									Weight = a.Weight
								} }
							}
						).OrderBy(o => o.Name).ThenBy(o => o.BGGId).ToList();
					}
					return i;
				});
			}

			return new IdSearchResponse { Items = result.ToList() };
		}
	}
}