﻿using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.Models;
using Dapper;
using Microsoft.Extensions.Options;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Data;

namespace BoardGameAuction.API.Repositories
{
	public class AccountRepository : IAccountRepository
	{
		private readonly AppSettings _appSettings;

		public AccountRepository(IOptions<AppSettings> appSettings)
		{
			_appSettings = appSettings.Value;
		}

		public async Task<int> Add(string forename, string surname, string password)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleOrDefaultAsync<int>(
					"[dbo].[AccountAdd]",
					new
					{
						Name = $"{forename} {surname}",
						PasswordHash = password
					},
					commandType: CommandType.StoredProcedure
				);

				return result;
			}
		}

		public async Task<Person> Get(string forename, string surname)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleOrDefaultAsync<Person>(
					"[dbo].[AccountGetByName]",
					new
					{
						Name = $"{forename} {surname}"
					},
					commandType: CommandType.StoredProcedure
				);

				return result;
			}
		}

		public async Task<Person> Get(int id)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleOrDefaultAsync<Person>(
					"[dbo].[AccountGetById]",
					new { Id = id },
					commandType: CommandType.StoredProcedure
				);

				return result;
			}
		}
	}
}