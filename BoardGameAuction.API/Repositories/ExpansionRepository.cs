﻿using BoardGameAuction.Models;
using BoardGameAuction.API.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.Data.SqlClient;
using Dapper;
using System.Data;

namespace BoardGameAuction.API.Repositories
{
    public class ExpansionRepository : IExpansionRepository
    {
		private readonly AppSettings _appSettings;

		public ExpansionRepository(IOptions<AppSettings> appSettings)
		{
			_appSettings = appSettings.Value;
		}

		public async Task<Expansion> Add(Expansion expansion)
        {
			try
			{
				using (var connection = new SqlConnection(_appSettings.ConnectionString))
				{
					await connection.OpenAsync();
					var result = await connection.QuerySingleOrDefaultAsync<int>(
						"[dbo].[ExpansionAdd]",
						new
						{
							ItemId = expansion.ItemId,
							BGGId = expansion.BGGId,
							VersionId = expansion.VersionId,
							Notes = expansion.Notes
						},
						commandType: CommandType.StoredProcedure
					);

					if (result == 0)
					{
						throw new ArgumentException("Could not add Expansion to the database", $"ItemId = {expansion.ItemId}, BGGId = {expansion.BGGId}, VersionId = {expansion.VersionId}, Notes = {expansion.Notes}");
					}

					expansion.Id = result;
					return expansion;
				}
			}
			catch (SqlException ex)
			{
				throw ex;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public async Task<Expansion> Edit(Expansion expansion)
        {
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleOrDefaultAsync<int>(
					"[dbo].[ExpansionUpdate]",
					new
					{
						Id = expansion.Id,
						Notes = expansion.Notes
					},
						commandType: CommandType.StoredProcedure
					);

				if (result == 0)
				{
					throw new ArgumentException("Could not update Item to the database", $"Id = {expansion.Id}, Notes = {expansion.Notes}");
				}

				expansion.Id = result;
				return expansion;
			}
		}

        public async Task<Expansion> Get(int id)
        {
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleOrDefaultAsync<Expansion>(
					"[dbo].[ExpansionGetById]",
					new { Id = id },
					commandType: CommandType.StoredProcedure
				);

			return result;
			}
		}

        public async Task<List<Expansion>> GetAll(int itemId)
        {
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QueryAsync<Expansion>(
					"[dbo].[ExpansionGetByItemId]",
					new { ItemId = itemId },
					commandType: CommandType.StoredProcedure
				);

				return result.ToList();
			}
		}
    }
}