﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BoardGameAuction.Models;
using BoardGameAuction.API.Repositories.Interfaces;
using Dapper;
using Microsoft.Extensions.Options;
using System.Data;

namespace BoardGameAuction.API.Repositories
{
	public class AuctionRepository : IAuctionRepository
	{
		private readonly AppSettings _appSettings;

		public AuctionRepository(IOptions<AppSettings> appSettings)
		{
			_appSettings = appSettings.Value;
		}

		public async Task<Auction> Add(Auction auction)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleOrDefaultAsync<int>(
					"[dbo].[AuctionAdd]",
					new { 
						auction.Code, 
						Type = (short)auction.Type,
						Start = auction.Start,
						End = auction.End,
						auction.Password
					},
					commandType: CommandType.StoredProcedure
				);

				auction.Id = result;
				return auction;
			}
		}

		public async Task<Auction> Get(int id)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = new List<Auction>();
				await connection.QueryAsync(
					"[dbo].[AuctionGetById]",
					new Type[] {
						typeof(Auction),
						typeof(Lot),
						typeof(Person),
						typeof(Item),
						typeof(Expansion)
					},
					MapResult(result),
					new { id },
					commandType: CommandType.StoredProcedure
				);
				return result.FirstOrDefault();
			}
		}

		public async Task<List<Auction>> GetAll()
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = new List<Auction>();
				return (await connection.QueryAsync(
					"[dbo].[AuctionGet]",
					new Type[] {
						typeof(Auction),
						typeof(Lot),
						typeof(Person),
						typeof(Item),
						typeof(Expansion)
					},
					MapResult(result),
					commandType: CommandType.StoredProcedure
				)).ToList();
			}
		}

		public async Task<Auction> Summary(int id)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = new List<Auction>();
				await connection.QueryAsync("[dbo].[AuctionGetSummary]",
					new Type[] {
						typeof(Auction),
						typeof(Lot),
						typeof(Person),
						typeof(Item),
						typeof(Expansion)
					},
					MapResult(result),
					new { id },
					commandType: CommandType.StoredProcedure
					);
				return result.FirstOrDefault();
			}
		}

		private Func<object[], Auction> MapResult(List<Auction> result)
		{
			return (obj) =>
			{
				Auction auction = (Auction)obj[0];
				Lot lot = (Lot)obj[1];
				Person seller = (Person)obj[2];
				Item item = (Item)obj[3];
				Expansion expansion = (Expansion)obj[4];

				if (auction != null)
				{
					if (result.Any(a => a.Id == auction.Id))
					{
						auction = result.First(a => a.Id == auction.Id);
					}
					else
					{
						result.Add(auction);
					}
				}

				if (lot != null)
				{
					if (auction.Lots.Any(l => l.Id == lot.Id))
					{
						lot = auction.Lots.First(l => l.Id == lot.Id);
					}
					else
					{
						auction.Lots.Add(lot);
					}
				}

				if (seller != null)
				{
					lot.Seller = seller;
				}

				if (item != null)
				{
					if (lot.Items.Any(i => i.Id == item.Id))
					{
						item = lot.Items.First(i => i.Id == item.Id);
					}
					else
					{
						lot.Items.Add(item);
					}
				}

				if (expansion != null)
				{
					if (item.Expansions.Any(e => e.Id == expansion.Id) == false)
					{
						item.Expansions.Add(expansion);
					}
				}

				return null;
			};
		}

		public async Task<Auction> GetByCode(string code)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				try
				{
					await connection.OpenAsync();
					return await connection.QuerySingleOrDefaultAsync<Auction>(
						"[dbo].[AuctionGetByCode]", 
						new { code },
						commandType: CommandType.StoredProcedure
					);
				}
				catch (SqlException ex)
				{
					throw ex;
				}
			}
		}

		public async Task<Auction> Update(Auction auction)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				return await connection.QuerySingleOrDefaultAsync<Auction>(
					"[dbo].[AuctionUpdate]",
					new { 
						Id = auction.Id,
						Password = auction.Password,
						Type = auction.Type,
						Start = auction.Start,
						End = auction.End
					},
					commandType: CommandType.StoredProcedure
				);
			}
		}

		public async Task<Auction> Join(int id)
		{
			var auction = await Get(id);
			return auction;

			// TODO: Save auction participants and display current count
			/*using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				return await connection.QuerySingleOrDefaultAsync<Auction>($@"
					INSERT INTO [AuctionPerson] ([AuctionId], [PersonId])
					VALUES (@{nameof(auctionId)}, @{nameof(personId)});

					SELECT [Id], [Code], [Type], [Start], [End], [Password]
					FROM [Auction]
					WHERE [Id] = @{nameof(auctionId)}", 
					new { auctionId, personId });
			}*/
		}

		public async Task<int> Leave(int id)
		{
			return 0;

			// TODO: Save auction participants and display current count
			/*using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleOrDefaultAsync<Auction>($@"
					DELETE FROM [AuctionPerson]
					WHERE ([AuctionId] = @{nameof(auctionId)} AND [PersonId] = @{nameof(personId)});
					SELECT @@ROWCOUNT",
					new {auctionId, personId});
				return result;
			}*/
		}
	}
}
