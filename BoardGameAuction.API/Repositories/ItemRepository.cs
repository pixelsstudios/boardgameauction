﻿using BoardGameAuction.Models;
using BoardGameAuction.API.Repositories.Interfaces;
using Dapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Data;

namespace BoardGameAuction.API.Repositories
{
    public class ItemRepository : IItemRepository
    {
		private readonly AppSettings _appSettings;

		public ItemRepository(IOptions<AppSettings> appSettings)
		{
			_appSettings = appSettings.Value;
		}

		public async Task<Item> Add(Item item)
        {
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleOrDefaultAsync<int>(
					"[dbo].[ItemAdd]",
					new
					{
						LotId = item.LotId,
						BGGId = item.BGGId,
						VersionId =item.VersionId,
						Notes = item.Notes
					},
					commandType: CommandType.StoredProcedure
					);

				if (result == 0)
				{
					throw new ArgumentException("Could not add Item to the database", $"LotId = {item.LotId}, BGGId = {item.BGGId}, VersionId = {item.VersionId}, Notes = {item.Notes}");
				}

				item.Id = result;
				return item;
			}
		}

        public async Task<Item> Edit(Item item)
        {
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleOrDefaultAsync<int>(
					"[dbo].[ItemUpdate]",
					new
					{
						Id = item.Id,
						Notes = item.Notes,
					});

				if (result == 0)
				{
					throw new ArgumentException("Could not update Item to the database", $"Id = {item.Id}, Notes = {item.Notes}");
				}

				item.Id = result;
				return item;
			}
		}

        public async Task<Item> Get(int id)
        {
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleOrDefaultAsync<Item>(
					"[dbo].[ItemGetById]",
					new { Id = id },
					commandType: CommandType.StoredProcedure
				);

				if (result == null || result.Id == 0)
				{
					throw new ArgumentException("Could not get Item from the database", $"Id = {id}");
				}

				return result;
			}
		}

        public async Task<List<Item>> GetAll(int lotId)
        {
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QueryAsync<Item>(
					"[dbo].[ItemGetByLotId]",
					new { LotId = lotId },
					commandType: CommandType.StoredProcedure
				);

				if (result == null || result.Count() == 0)
				{
					throw new ArgumentException("Could not get Items for Lot from the database", $"LotId = {lotId}");
				}

				return result.ToList();
			}
		}

		public async Task<int> Delete(int id, int lotId)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleAsync<int>(
					"[dbo].[ItemDelete]",
					new { LotId = lotId, ItemId = id },
					commandType: CommandType.StoredProcedure
				);

				return result;
			}
		}
	}
}
