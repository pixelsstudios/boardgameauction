﻿using BoardGameAuction.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BoardGameAuction.API.Repositories.Interfaces
{
	public interface ISearchRepository
	{
		Task<NameSearchResponse> GetByName(string query);

		Task<IdSearchResponse> GetByBGGId(IEnumerable<int> BGGIds);
	}
}