﻿using BoardGameAuction.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BoardGameAuction.API.Repositories.Interfaces
{
	public interface IProductRepository
	{
		Task<List<Product>> Get(IEnumerable<int> Ids);
		Task<List<Product>> Get(IEnumerable<int> Ids, DateTime timestamp);
		Task<List<Product>> Add(IEnumerable<ProductTable> products);
		Task<List<Product>> Edit(IEnumerable<ProductTable> products);
	}
}