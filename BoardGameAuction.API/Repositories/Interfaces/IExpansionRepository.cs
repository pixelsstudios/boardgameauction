﻿using BoardGameAuction.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BoardGameAuction.API.Repositories.Interfaces
{
    public interface IExpansionRepository
    {
        Task<Expansion> Add(Expansion expansion);
        Task<Expansion> Edit(Expansion expansion);
        Task<Expansion> Get(int id);
        Task<List<Expansion>> GetAll(int itemId);
    }
}