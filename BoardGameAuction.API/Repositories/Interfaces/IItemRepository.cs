﻿using BoardGameAuction.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BoardGameAuction.API.Repositories.Interfaces
{
    public interface IItemRepository
    {
        Task<Item> Add(Item item);
        Task<Item> Edit(Item item);
        Task<Item> Get(int id);
        Task<List<Item>> GetAll(int lotId);
        Task<int> Delete(int id, int lotId);
    }
}
