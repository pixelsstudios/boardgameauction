﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoardGameAuction.Models;

namespace BoardGameAuction.API.Repositories.Interfaces
{
	public interface IAuctionRepository
	{
		Task<Auction> Add(Auction auction);
		Task<Auction> Get(int id);
		Task<List<Auction>> GetAll();
		Task<Auction> GetByCode(string code);
		Task<Auction> Update(Auction auction);
		Task<Auction> Summary(int id);
		Task<Auction> Join(int id);
		Task<int> Leave(int id);
	}
}