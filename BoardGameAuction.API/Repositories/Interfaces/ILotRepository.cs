﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BoardGameAuction.Models;

namespace BoardGameAuction.API.Repositories.Interfaces
{
	public interface ILotRepository
	{
		Task<Lot> Add(Lot lot);
		Task<Lot> Edit(Lot lot);
		Task<Lot> Get(int id);
		Task<List<Lot>> GetAll(int auctionId);
		Task<bool> MakeActive(int id, int auctionId);
		Task<Lot> Sell(int id, double price);
		Task<bool> Delete(int auctionId, int lotId);
	}
}