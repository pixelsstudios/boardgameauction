﻿using BoardGameAuction.Models;
using System.Threading.Tasks;

namespace BoardGameAuction.API.Repositories.Interfaces
{
	public interface IAccountRepository
	{
		Task<int> Add(string forename, string surname, string password);
		Task<Person> Get(string forename, string surname);
		Task<Person> Get(int id);
	}
}