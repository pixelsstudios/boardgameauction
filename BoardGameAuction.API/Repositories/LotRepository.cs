﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BoardGameAuction.Models;
using BoardGameAuction.API.Repositories.Interfaces;
using Dapper;
using Microsoft.Extensions.Options;
using System.Data;

namespace BoardGameAuction.API.Repositories
{
	public class LotRepository : ILotRepository
	{
		private readonly AppSettings _appSettings;

		public LotRepository(IOptions<AppSettings> appSettings)
		{
			this._appSettings = appSettings.Value;
		}

		public async Task<Lot> Add(Lot lot)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleOrDefaultAsync<int>(
					"[dbo].[LotAdd]",
					new
					{
						AuctionId = lot.AuctionId,
						SellerId = lot.Seller.Id,
						StartPrice = lot.StartPrice,
						EndPrice = lot.EndPrice,
					},
					commandType: CommandType.StoredProcedure
				);

				if (result == 0) { 
					throw new ArgumentException("Could not add Lot to the database", $"AuctionId = {lot.AuctionId}, Seller = {lot.Seller}, StartPrice = {lot.StartPrice}, EndPrice = {lot.EndPrice}"); 
				}

				lot.Id = result;
				return lot;
			}
		}

		public async Task<Lot> Edit(Lot lot)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				try
				{
					await connection.OpenAsync();
					var result = await connection.QuerySingleOrDefaultAsync<int>(
						"[dbo].[LotUpdate]",
						new
						{
							Id = lot.Id,
							AuctionId = lot.AuctionId,
							SellerId = lot.Seller.Id,
							StartPrice = lot.StartPrice,
							EndPrice = lot.EndPrice
						},
						commandType: CommandType.StoredProcedure
					);

					if (result == 0)
					{
						throw new ArgumentException("Could not update Lot in the database", $"Id = {lot.Id}, AuctionId = {lot.AuctionId}, Seller = {lot.Seller}, StartPrice = {lot.StartPrice}, EndPrice = {lot.EndPrice}");
					}

					return lot;
				}
				catch (SqlException ex)
				{
					throw ex;
				}
				catch (Exception ex)
				{
					throw ex;
				}
			}
		}

		public async Task<Lot> Get(int id)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = new List<Lot>();
				await connection.QueryAsync<Lot>(
					"[dbo].[LotGetById]",
					new Type[] {
						typeof(Lot),
						typeof(Person),
						typeof(Item),
						typeof(Expansion)
					},
					MapResult(result), 
					new { Id = id },
					commandType: CommandType.StoredProcedure
				);

				return result.FirstOrDefault();
			}
		}

		public async Task<List<Lot>> GetAll(int auctionId)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = new List<Lot>();
				await connection.QueryAsync(
					"[dbo].[LotGetByAuctionId]",
					new Type[] {
						typeof(Lot),
						typeof(Person),
						typeof(Item),
						typeof(Expansion)
					}, 
					MapResult(result),
					new { Id = auctionId },
					commandType: CommandType.StoredProcedure
				);

				return result.ToList();
			}
		}

		public async Task<bool> MakeActive(int id, int auctionId)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleAsync<int>(
					"[dbo].[LotMakeActive]",
					new { Id = id, AuctionId = auctionId },
					commandType: CommandType.StoredProcedure
				);

				return result == 1;
			}
		}

		public async Task<Lot> Sell(int id, double price)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				await connection.OpenAsync();
				var result = await connection.QuerySingleOrDefaultAsync<Lot>(
					"[dbo].[LotSell]",
					new
					{
						Id = id,
						Price = price
					},
					commandType: CommandType.StoredProcedure
				);

				if (result == null || result.Id == 0)
				{
					throw new ArgumentException("Could not update Lot price in the database", $"Id = {id}, Price = {price}");
				}

				return result;
			}
		}

		public async Task<bool> Delete(int auctionId, int lotId)
		{
			using (var connection = new SqlConnection(_appSettings.ConnectionString))
			{
				try
				{
					await connection.OpenAsync();
					var result = await connection.QuerySingleOrDefaultAsync<int>(
						"[dbo].[LotDelete]",
						new
						{
							AuctionId = auctionId,
							LotId = lotId
						},
						commandType: CommandType.StoredProcedure
					);

					return result == 1;
				}
				catch (SqlException ex)
				{
					throw ex;
				}
			}
		}

		private Func<object[], Lot> MapResult(List<Lot> result)
		{
			return (obj) =>
			{
				Lot lot = (Lot)obj[0];
				Person seller = (Person)obj[1];
				Item item = (Item)obj[2];
				Expansion expansion = (Expansion)obj[3];

				if (lot != null)
				{
					if (result.Any(a => a.Id == lot.Id))
					{
						lot = result.First(a => a.Id == lot.Id);
					}
					else
					{
						result.Add(lot);
					}
				}

				if (seller != null)
				{
					lot.Seller = seller;
				}

				if (item != null)
				{
					if (lot.Items.Any(i => i.Id == item.Id))
					{
						item = lot.Items.First(i => i.Id == item.Id);
					}
					else
					{
						lot.Items.Add(item);
					}
				}

				if (expansion != null)
				{
					if (item.Expansions.Any(e => e.Id == expansion.Id) == false)
					{
						item.Expansions.Add(expansion);
					}
				}

				return null;
			};
		}
	}
}