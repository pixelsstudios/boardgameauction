This was a project I undertook during the COVID furlough. I attend a board gaming event a couple of times a year that has a Dutch Auction for used board games. This is an auction where the bid starts high and lowers towards the reserve and it's a game of chicken to see how low you let the game go to get the best price, while risking losing out to the reserve or another bidder.

The site is written as a Database-First API and ASP.NET Core MVC Frontend. The API architecture is N-Layer with Controllers, Services and Repositories, written in a DDD (v1) style and NUnit TDD. On the frontend SignalR is used to update bidders live during the auction. I have used as little JavaScript as possible in the project, including in SignalR where I was able to move a lot of the traditional JavaScript into Hubs in the API. This made is a lot more compile safe, is written once for many frontends, and made a lot more sense than having a frontend ask the backend to setup a hub when it was responding the events that happen in the API initially.

The problem to solve was the massive amout of administration to keep track of during an auction. 
 - Before the auction games must be registered and details added to a note attached to the game, which must be visible to the auctioneer, but hidden from the bidders as it includes the reserve
 - During the auction games (or bundles of) are being sold for cash, which must be taken, change given, and values must be allocated to the owner of that game. The idea of a Dutch Auction is to be fast and a game is sold about once every 30 seconds and usually lasts for about an hour
 - After the auction the sales must be grouped into sellers to work out the total that is owed to them

 This site intends to reduce the administration by:
 - allowing sellers to use the site on their phone to log their lots themselves, using www.boardgamegeek.com to search for and retrieve information on the game, including the specific version, and easily add on expansions it may have, as well as adding notes. All this is done without sticky notes that can become detached
 - giving the auctioneer an alphabetical list of all the lots so that they can easily find the game they have picked up from the table in order to see the details, starting price and reserve. It also gives buttons for typical increments within the price range
 - summarises all the sales and no-sales in a post-auction summary to make the tallying a trivial task

 As well as solving problems, the site also adds new advantages:
 - sellers can register their games any time they want before the auction
 - bidders don't need to get to the front to see what lots are in the auction as they can view them on their phone
 - when the auctioneer marks a lot as "On the block" it jumps to the top of bidders lists on their phones
 - bidders can look at the lot details themselves, including a link to more information on it on the worlds largest board game site
 - sellers are given a running total of what they are owed at the end of the auction, letting them know how much money they have to spend on other games
