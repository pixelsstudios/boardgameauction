﻿function ajaxPartial(url, selector, replaceSelector) {
    $('#loader').addClass('show');
    if (replaceSelector == true) {
        $.get(url, function (r) {
            $(selector).replaceWith(r);
        }).always(function () {
            $('#loader').removeClass('show');
        });
    } else {
        $(selector).load(url, function () {
            $('#loader').removeClass('show');
        });
    }
}

function searchByName(e, url, selector) {
    if (e.value.length >= 3) {
        ajaxPartial(url + "?query=" + encodeURIComponent(e.value), selector, false);
    }
}