﻿namespace BoardGameAuction.Models
{
	public class ItemViewModel : Item
	{
		public string LinkUrl { get; set; }
		public int ExpansionsCount { get; set; }
	}
}