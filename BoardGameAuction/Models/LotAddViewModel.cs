﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BoardGameAuction.Models
{
	public class LotAddViewModel
	{
		public int LotId { get; set; }

		[Required(ErrorMessage = "A lot needs a price")]
		[Range(1.00, 200, ErrorMessage = "A lot needs a price")]
		public double? StartPrice { get; set; }

		[Required(ErrorMessage = "A lot needs a price")]
		[Range(1.00, 200, ErrorMessage = "A lot needs a price")] 
		public double? EndPrice { get; set; }
		
		public List<ProductViewModel> Items { get; set; } = new List<ProductViewModel>();
	}
}