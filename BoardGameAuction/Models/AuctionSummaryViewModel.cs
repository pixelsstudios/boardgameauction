﻿using System.Collections.Generic;

namespace BoardGameAuction.Models
{
	public class AuctionSummaryViewModel
	{
		public int AuctionId { get; set; }
		public int CountLotsSold { get; set; }
		public int CountItemsSold { get; set; }
		public double ValueSold { get; set; }
		public IEnumerable<AuctionSummaryItem> Sellers { get; set; }
	}

	public class AuctionSummaryItem
	{
		public Person Seller { get; set; }
		public double Total { get; set; }
		public IEnumerable<Lot> Lots { get; set; }
	}
}