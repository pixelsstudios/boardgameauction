﻿namespace BoardGameAuction.Models
{
	public class BecomeAdminViewModel : BecomeAdminRequest
	{
		public string RedirectUrl { get; set; }
	}
}