﻿using System.ComponentModel.DataAnnotations;

namespace BoardGameAuction.Models
{
    public class AuctionRequestModel
    {
        public int Id { get; set; }

        [MinLength(4, ErrorMessage = "That's not a real Auction code")]
        [MaxLength(4, ErrorMessage = "That's not a real Auction code")]
        public string Code { get; set; }

        [MinLength(4, ErrorMessage = "The minimum length for a password is 4 characters")]
        public string Password { get; set; }

        public bool CreateAuction { get; set; }
    }
}