﻿namespace BoardGameAuction.Models
{
	public class LoginOrRegisterViewModel
	{
		public bool IsRegistering { get; set; }
		public string RedirectUrl { get; set; }
		public LoginRequest Login { get; set; }
		public RegisterRequest Register { get; set; }
	}
}