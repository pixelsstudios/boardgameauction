﻿using System.ComponentModel.DataAnnotations;

namespace BoardGameAuction.Models
{
	public class AuctionEditViewModel : Auction
	{
		public AuctionEditViewModel() { }
		
		public AuctionEditViewModel(Auction auction) 
		{
			Id = auction.Id;
			Code = auction.Code;
			Password = auction.Password;
			Start = auction.Start;
			End = auction.End;
			Type = auction.Type;
		}
		
		[Required(ErrorMessage = "A password is needed so you can get back in if there's an issue")]
		[MinLength(4, ErrorMessage = "The minimum length for a password is 4 characters")]
		[Display(Name = "Confirm Password")]
		public string ConfirmPassword { get; set; }
	}
}