﻿using System.Collections.Generic;
using System.Linq;

namespace BoardGameAuction.Models
{
	/// <summary>
	/// A ViewModel of the BGG concept of a game
	/// Includes an included bool for views that 
	/// select products from lists
	/// </summary>
	public class ProductViewModel : Product
	{
		public ProductViewModel() { }

		public ProductViewModel(Product product)
		{
			Id = product.Id;
			BGGId = product.BGGId;
			Name = product.Name;
			Year = product.Year;
			ImageUrl = product.ImageUrl;
			Rating = product.Rating;
			Weight = product.Weight;
			VersionId = product.VersionId;
			VersionName = product.VersionName;
			Versions = product.Versions.Select(v => new ProductViewModel(v)).ToList();
			Expansions = product.Expansions.Select(e => new ProductViewModel(e)).ToList();
		}

		public bool IsChecked { get; set; }
		public string Notes { get; set; }
		public new List<ProductViewModel> Versions { get; set; } = new List<ProductViewModel>();
		public new List<ProductViewModel> Expansions { get; set; } = new List<ProductViewModel>();
	}
}