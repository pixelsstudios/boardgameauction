using BoardGameAuction.Hubs;
using BoardGameAuction.Models.Authorisation;
using BoardGameAuction.Services;
using BoardGameAuction.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Polly;
using Polly.Extensions.Http;
using System;
using System.Globalization;
using System.Net;
using System.Net.Http;

namespace BoardGameAuction
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();

			var appSettingsSection = Configuration.GetSection("AppSettings");
			services.Configure<AppSettings>(appSettingsSection);

			var baseUrl = appSettingsSection.GetValue<string>("apiUrl");
			services.AddTransient<AccountHub, AccountHub>();
			services.AddTransient<AuctionHub, AuctionHub>();
			services.AddHttpClient<IAccountService, AccountService>(GetRequestConfig(baseUrl))
				.SetHandlerLifetime(TimeSpan.FromMinutes(5))
				.AddPolicyHandler(GetRetryPolicy())
				.AddPolicyHandler(GetCircuitBreakerPolicy());
			services.AddHttpClient<IAuctionService, AuctionService>(GetRequestConfig(baseUrl))
				.SetHandlerLifetime(TimeSpan.FromMinutes(5))
				.AddPolicyHandler(GetRetryPolicy())
				.AddPolicyHandler(GetCircuitBreakerPolicy());
			services.AddHttpClient<ILotService, LotService>(GetRequestConfig(baseUrl))
				.SetHandlerLifetime(TimeSpan.FromMinutes(5))
				.AddPolicyHandler(GetRetryPolicy())
				.AddPolicyHandler(GetCircuitBreakerPolicy());
			services.AddHttpClient<IItemService, ItemService>(GetRequestConfig(baseUrl))
				.SetHandlerLifetime(TimeSpan.FromMinutes(5))
				.AddPolicyHandler(GetRetryPolicy())
				.AddPolicyHandler(GetCircuitBreakerPolicy());
			services.AddHttpClient<ISearchService, SearchService>(GetRequestConfig(baseUrl))
				.SetHandlerLifetime(TimeSpan.FromMinutes(5))
				.AddPolicyHandler(GetRetryPolicy())
				.AddPolicyHandler(GetCircuitBreakerPolicy());

			services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
				.AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, act => 
				{
				   act.LoginPath = @"/account/login";
				   act.AccessDeniedPath = @"/";
				   act.SlidingExpiration = true;
				});

			services.AddAuthorization(options => {
				options.AddPolicy("IsSeller", policy => policy.Requirements.Add(new IsSellerRequirement()));
				options.AddPolicy("IsAdmin", policy => policy.Requirements.Add(new IsAdminRequirement()));
			});
			services.AddSingleton<IAuthorizationHandler, IsSellerRequirement.IsSellerHandler>();
			services.AddSingleton<IAuthorizationHandler, IsAdminRequirement.IsAdminHandler>();

			var cultureInfo = new CultureInfo("en-GB");
			cultureInfo.NumberFormat.CurrencySymbol = "�";

			CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
			CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;

			services.AddControllersWithViews();
			services.AddSignalR();
			services.AddRazorPages().AddRazorRuntimeCompilation();
		}

		static Action<HttpClient> GetRequestConfig(string baseUrl)
		{
			return config =>
			{
				config.BaseAddress = new Uri(baseUrl);
				config.DefaultRequestHeaders.Add(HttpRequestHeader.Accept.ToString(), "application/json");
			};
		}

		static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
		{
			Random jitterer = new Random();
			return HttpPolicyExtensions
				.HandleTransientHttpError()
				.WaitAndRetryAsync(6,    // exponential back-off plus some jitter
					retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))
								  + TimeSpan.FromMilliseconds(jitterer.Next(0, 100))
				);
		}

		static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
		{
			return HttpPolicyExtensions
				.HandleTransientHttpError()
				.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30));
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}
			app.UseHttpsRedirection();
			app.UseStaticFiles();

			app.UseRouting();

			app.UseAuthentication();
			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapHub<AccountHub>("/account/balance");
				endpoints.MapHub<AuctionHub>("/auction/hub");
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
			});
		}
	}
}
