﻿using BoardGameAuction.Models;
using BoardGameAuction.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BoardGameAuction.Services
{
	public class AuctionService : IAuctionService
	{
		private readonly HttpClient _httpClient;
		private readonly ILogger<AuctionService> _logger;
		private readonly AppSettings _appSettings;

		public AuctionService(HttpClient httpClient, ILogger<AuctionService> logger, IOptions<AppSettings> appSettings)
		{
			_httpClient = httpClient;
			_logger = logger;
			_appSettings = appSettings.Value;
		}

		public async Task<Auction> Get(string bearer)
		{
			if (_httpClient.DefaultRequestHeaders.Contains("Authorization") == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.GetAsync("auction");

			if (response.IsSuccessStatusCode == false) { throw new Exception(response.ReasonPhrase); }

			var auction = JsonConvert.DeserializeObject<Auction>(await response.Content.ReadAsStringAsync());

			return auction;
		}

		public async Task<Auction> Edit(Auction auction, string bearer)
		{
			if (_httpClient.DefaultRequestHeaders.Contains("Authorization") == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var httpContent = new StringContent(JsonConvert.SerializeObject(auction), Encoding.UTF8, "application/json");
			var response = await _httpClient.PutAsync("auction", httpContent);

			if (response.IsSuccessStatusCode == false) { throw new Exception(response.ReasonPhrase); }

			var result = JsonConvert.DeserializeObject<Auction>(await response.Content.ReadAsStringAsync());

			return result;
		}

		/// <summary>
		/// Requests to join the auction as an attendee
		/// If successful a bearer token will be issued to be used in subsequent
		/// requests and will determine the auction, role and user
		/// </summary>
		/// <param name="code">The 4-letter code for the auction</param>
		/// <param name="bearer">The existing bearer token that might store user details</param>
		/// <returns>A bearer code to be used in all subsequent requests</returns>
		public async Task<string> Join(string code, string bearer)
		{
			if (string.IsNullOrWhiteSpace(bearer) == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.GetAsync($"auction/join?code={code}");

			if (response.IsSuccessStatusCode == false) { 
				throw new Exception(response.ReasonPhrase, new Exception(await response.Content.ReadAsStringAsync())); 
			}

			return await response.Content.ReadAsAsync<string>();
		}

		/// <summary>
		/// Creates an auction with the current user as an administrator
		/// If successful a bearer token will be issued to be used in subsequent
		/// requests and will determine the auction, role and user
		/// </summary>
		/// <param name="password">The password to access the admin role for the auction</param>
		/// <param name="bearer">The existing bearer token that might store user details</param>
		/// <returns>A bearer code to be used in all subsequent requests</returns>
		public async Task<string> Add(string password, string bearer)
		{
			if (string.IsNullOrWhiteSpace(bearer) == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var content = new PasswordRequest { Password = password };
			var httpContent = new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json");
			var response = await _httpClient.PostAsync($"auction/add", httpContent);

			if (response.IsSuccessStatusCode == false)
			{
				throw new Exception(response.ReasonPhrase, new Exception(await response.Content.ReadAsStringAsync()));
			}

			return await response.Content.ReadAsAsync<string>();
		}

		public async Task<string> Leave(string bearer)
		{
			if (_httpClient.DefaultRequestHeaders.Contains("Authorization") == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.PutAsync($"auction/leave", null);

			if (response.IsSuccessStatusCode == false)
			{
				throw new Exception(response.ReasonPhrase, new Exception(await response.Content.ReadAsStringAsync()));
			}

			return await response.Content.ReadAsAsync<string>();
		}

		public async Task<Auction> Summary(string bearer)
		{
			if (_httpClient.DefaultRequestHeaders.Contains("Authorization") == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.GetAsync("auction/summary");

			if (response.IsSuccessStatusCode == false) { throw new Exception(response.ReasonPhrase); }

			var auction = JsonConvert.DeserializeObject<Auction>(await response.Content.ReadAsStringAsync());

			return auction;
		}

		public async Task Start(string bearer)
		{
			if (_httpClient.DefaultRequestHeaders.Contains("Authorization") == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.PutAsync($"auction/start?timestamp={DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss")}", null);

			if (response.IsSuccessStatusCode == false) { throw new Exception(response.ReasonPhrase); }
		}

		public async Task Stop(string bearer)
		{
			if (_httpClient.DefaultRequestHeaders.Contains("Authorization") == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.PutAsync($"auction/stop?timestamp={DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss")}", null);

			if (response.IsSuccessStatusCode == false) { throw new Exception(response.ReasonPhrase); }
		}
	}
}