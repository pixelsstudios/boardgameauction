﻿using BoardGameAuction.Models;
using BoardGameAuction.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace BoardGameAuction.Services
{
	public class AccountService : IAccountService
	{
		private readonly HttpClient _httpClient;
		private readonly ILogger<AuctionService> _logger;
		private readonly AppSettings _appSettings;

		public AccountService(HttpClient httpClient, ILogger<AuctionService> logger, IOptions<AppSettings> appSettings)
		{
			_httpClient = httpClient;
			_logger = logger;
			_appSettings = appSettings.Value;
		}

		public async Task<double> Balance(string bearer)
		{
			if (string.IsNullOrWhiteSpace(bearer) == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.GetAsync("account/balance");

			if (response.IsSuccessStatusCode == false) { throw new Exception(await response.Content.ReadAsAsync<string>() ?? response.ReasonPhrase); }

			var text = await response.Content.ReadAsAsync<string>();
			return double.Parse(text);
		}

		public async Task<string> BecomeAdmin(BecomeAdminViewModel model, string bearer)
		{
			if (string.IsNullOrWhiteSpace(bearer) == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.PostAsJsonAsync("account/becomeadmin", model);

			if (response.IsSuccessStatusCode == false) { throw new Exception(await response.Content.ReadAsAsync<string>() ?? response.ReasonPhrase); }

			return await response.Content.ReadAsAsync<string>();
		}

		public async Task<string> Login(LoginRequest model, string bearer)
		{
			if (string.IsNullOrWhiteSpace(bearer) == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.PostAsJsonAsync("account/login", model);

			if (response.IsSuccessStatusCode == false) { throw new Exception(await response.Content.ReadAsAsync<string>() ?? response.ReasonPhrase); }

			return await response.Content.ReadAsAsync<string>();
		}

		public async Task<RegisterResponse> Register(RegisterRequest model)
		{
			var response = await _httpClient.PostAsJsonAsync("account/register", model);

			if (response.IsSuccessStatusCode == false) { throw new Exception(await response.Content.ReadAsAsync<string>() ?? response.ReasonPhrase); }

			return JsonConvert.DeserializeObject<RegisterResponse>(await response.Content.ReadAsStringAsync());
		}
	}
}
