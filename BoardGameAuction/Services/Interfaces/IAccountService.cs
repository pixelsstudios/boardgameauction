﻿using BoardGameAuction.Models;
using System.Threading.Tasks;

namespace BoardGameAuction.Services.Interfaces
{
	public interface IAccountService
	{
		Task<RegisterResponse> Register(RegisterRequest model);
		Task<string> Login(LoginRequest model, string bearer);
		Task<string> BecomeAdmin(BecomeAdminViewModel model, string bearer);
		Task<double> Balance(string bearer);
	}
}