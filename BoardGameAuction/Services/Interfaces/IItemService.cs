﻿using BoardGameAuction.Models;
using System.Threading.Tasks;

namespace BoardGameAuction.Services.Interfaces
{
	public interface IItemService
	{
		Task<Item> Add(int lotId, Product item, string bearer);
		Task<bool> Delete(int itemId, int lotId, string bearer);
	}
}