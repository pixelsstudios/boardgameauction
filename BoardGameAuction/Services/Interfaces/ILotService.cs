﻿using BoardGameAuction.Models;
using System.Threading.Tasks;

namespace BoardGameAuction.Services.Interfaces
{
	public interface ILotService
	{
		Task<Lot> Get(int id, string bearer);
		Task<Lot> Add(Lot lot, string bearer);
		Task<Lot> Edit(Lot lot, string bearer);
		Task MakeActive(int id, int auctionId, string bearer);
		Task Sell(int id, int auctionId, double price, int sellerId, string bearer);
		Task Delete(int auctionId, int lotId, string bearer);
	}
}