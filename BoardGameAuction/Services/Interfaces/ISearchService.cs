﻿using BoardGameAuction.Models;
using System.Threading.Tasks;

namespace BoardGameAuction.Services.Interfaces
{
	public interface ISearchService
	{
		Task<NameSearchResponse> ByName(string name, string bearer);
		Task<IdSearchResponse> ById(int[] id, string bearer);
	}
}