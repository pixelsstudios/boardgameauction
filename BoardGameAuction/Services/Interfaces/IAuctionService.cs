﻿using BoardGameAuction.Models;
using System.Threading.Tasks;

namespace BoardGameAuction.Services.Interfaces
{
	public interface IAuctionService
	{
		Task<Auction> Get(string bearer);
		Task<Auction> Edit(Auction auction, string bearer);
		Task<string> Join(string code, string bearer);
		Task<string> Add(string password, string bearer);
		Task<string> Leave(string bearer);
		Task Start(string bearer);
		Task Stop(string bearer);
		Task<Auction> Summary(string bearer);
	}
}