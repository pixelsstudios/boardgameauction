﻿using BoardGameAuction.Models;
using BoardGameAuction.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace BoardGameAuction.Services
{
	public class SearchService : ISearchService
	{
		private readonly HttpClient _httpClient;
		private readonly ILogger<SearchService> _logger;
		private readonly AppSettings _appSettings;

		public SearchService(HttpClient httpClient, ILogger<SearchService> logger, IOptions<AppSettings> appSettings)
		{
			_httpClient = httpClient;
			_logger = logger;
			_appSettings = appSettings.Value;
		}

		public async Task<IdSearchResponse> ById(int[] id, string bearer)
		{
			_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");

			var response = await _httpClient.GetAsync($"search/byid?id={string.Join("&id=", id)}");

			if (response.IsSuccessStatusCode == false)
			{
				throw new Exception(response.ReasonPhrase);
			}

			var searchResult = JsonConvert.DeserializeObject<IdSearchResponse>(await response.Content.ReadAsStringAsync());

			return searchResult;
		}

		public async Task<NameSearchResponse> ByName(string name, string bearer)
		{
			_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");

			var response = await _httpClient.GetAsync($"search?query={name}");

			if (response.IsSuccessStatusCode == false)
			{
				throw new Exception(response.ReasonPhrase);
			}

			var searchResult = JsonConvert.DeserializeObject<NameSearchResponse>(await response.Content.ReadAsStringAsync());

			return searchResult;
		}
	}
}
