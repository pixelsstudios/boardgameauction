﻿using BoardGameAuction.Models;
using BoardGameAuction.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BoardGameAuction.Services
{
	public class ItemService : IItemService
	{
		private readonly HttpClient _httpClient;
		private readonly ILogger<ItemService> _logger;
		private readonly AppSettings _appSettings;

		public ItemService(HttpClient httpClient, ILogger<ItemService> logger, IOptions<AppSettings> appSettings)
		{
			_httpClient = httpClient;
			_logger = logger;
			_appSettings = appSettings.Value;
		}

		public async Task<Item> Add(int lotId, Product product, string bearer)
		{
			if (lotId == 0) { throw new Exception("No lot provided for item to be added to"); }
			if (product == null) { throw new Exception("No product provided for item creation"); }
			if (product.BGGId == 0) { throw new Exception("No product data for item creation"); }

			var item = new Item(product);
			item.LotId = lotId;

			if (_httpClient.DefaultRequestHeaders.Contains("Authorization") == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var httpContent = new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8, "application/json");
			var response = await _httpClient.PostAsync("item", httpContent);

			if (response.IsSuccessStatusCode == false) { throw new Exception(response.ReasonPhrase); }

			var newItem = JsonConvert.DeserializeObject<Item>(await response.Content.ReadAsStringAsync());

			return newItem;
		}

		public async Task<bool> Delete(int itemId, int lotId, string bearer)
		{
			if (itemId == 0) { throw new Exception("No item id to find the item to delete"); }

			if (_httpClient.DefaultRequestHeaders.Contains("Authorization") == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.DeleteAsync($"item/{itemId}?lotid={lotId}");

			if (response.IsSuccessStatusCode == false) { throw new Exception(response.ReasonPhrase); }

			return JsonConvert.DeserializeObject<bool>(await response.Content.ReadAsStringAsync());
		}
	}
}