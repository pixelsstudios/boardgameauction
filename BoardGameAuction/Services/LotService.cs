﻿using BoardGameAuction.Hubs;
using BoardGameAuction.Models;
using BoardGameAuction.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BoardGameAuction.Services
{
	public class LotService : ILotService
	{
		private readonly HttpClient _httpClient;
		private readonly IAccountService _accountService;
		private readonly AccountHub _accountHub;
		private readonly AuctionHub _auctionHub;
		private readonly ILogger<LotService> _logger;
		private readonly AppSettings _appSettings;

		public LotService(HttpClient httpClient, IAccountService accountService, AccountHub accountHub, AuctionHub auctionHub, ILogger<LotService> logger, IOptions<AppSettings> appSettings)
		{
			_httpClient = httpClient;
			_accountService = accountService;
			_accountHub = accountHub;
			_auctionHub = auctionHub;
			_logger = logger;
			_appSettings = appSettings.Value;
		}

		public async Task<Lot> Add(Lot lot, string bearer)
		{
			_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");

			var httpContent = new StringContent(JsonConvert.SerializeObject(lot), Encoding.UTF8, "application/json");
			var response = await _httpClient.PostAsync($"lot", httpContent);

			if (response.IsSuccessStatusCode == false) { throw new Exception(response.ReasonPhrase); }

			var newLot = JsonConvert.DeserializeObject<Lot>(await response.Content.ReadAsStringAsync());

			return newLot;
		}

		public async Task<Lot> Edit(Lot lot, string bearer)
		{
			if (_httpClient.DefaultRequestHeaders.Contains("Authorization") == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var httpContent = new StringContent(JsonConvert.SerializeObject(lot), Encoding.UTF8, "application/json");
			var response = await _httpClient.PutAsync($"lot", httpContent);

			if (response.IsSuccessStatusCode == false) { throw new Exception(response.ReasonPhrase); }

			var newLot = JsonConvert.DeserializeObject<Lot>(await response.Content.ReadAsStringAsync());

			return newLot;
		}

		public async Task<Lot> Get(int id, string bearer)
		{
			if (_httpClient.DefaultRequestHeaders.Contains("Authorization") == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.GetAsync($"lot/{id}");

			if (response.IsSuccessStatusCode == false) { throw new Exception(response.ReasonPhrase); }

			var lot = JsonConvert.DeserializeObject<Lot>(await response.Content.ReadAsStringAsync());

			return lot;
		}

		public async Task MakeActive(int id, int auctionId, string bearer)
		{
			if (_httpClient.DefaultRequestHeaders.Contains("Authorization") == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.PutAsync($"lot/{id}/makeactive", null);

			if (response.IsSuccessStatusCode == false) { throw new Exception(response.ReasonPhrase); }

			await _auctionHub.MakeLotActive(auctionId, id);

			return;
		}

		public async Task Sell(int id, int auctionId, double price, int sellerId, string bearer)
		{
			if (_httpClient.DefaultRequestHeaders.Contains("Authorization") == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.PutAsync($"lot/{id}/sell?price={price}", null);

			if (response.IsSuccessStatusCode == false) { throw new Exception(response.ReasonPhrase); }

			var balance = await _httpClient.GetAsync($"account/balance/{sellerId}");
			
			if (balance.IsSuccessStatusCode == false) { return; }

			var value = await balance.Content.ReadAsStringAsync();
			await _accountHub.SendBalance(sellerId, double.Parse(value));
			await _auctionHub.RemoveLot(auctionId, id);

			return;
		}

		public async Task Delete(int auctionId, int lotId, string bearer)
		{
			if (_httpClient.DefaultRequestHeaders.Contains("Authorization") == false)
			{
				_httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {bearer}");
			}

			var response = await _httpClient.DeleteAsync($"lot/{lotId}?auctionId={auctionId}");

			if (response.IsSuccessStatusCode == false) { throw new Exception(response.ReasonPhrase); }

			return;
		}
	}
}
