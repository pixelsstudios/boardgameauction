﻿using BoardGameAuction.Models;
using BoardGameAuction.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly.CircuitBreaker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BoardGameAuction.Controllers
{
	[Authorize]
	[Route("[controller]")]
    public class LotController : Controller
    {
		private readonly ILogger<LotController> _logger;
		private readonly AppSettings _appSettings;
		private readonly ILotService _service;
		private readonly IItemService _itemService;
		private readonly IAuctionService _auctionService;

		public LotController(ILogger<LotController> logger, IOptions<AppSettings> appSettings, ILotService service, IItemService itemService, IAuctionService auctionService)
		{
			_logger = logger;
			_appSettings = appSettings.Value;
			_service = service;
			_itemService = itemService;
			_auctionService = auctionService;
		}

		public async Task<IActionResult> Index(int id)
        {
			var bearer = User.FindFirst("Access_Token").Value;
			try
			{
				var lot = await _service.Get(id, bearer);
				var auction = await _auctionService.Get(bearer);
				ViewData.Add("allowEdits", auction.Status == Models.Types.AuctionStatus.NotStarted);
				return View(lot);
			}
			catch (BrokenCircuitException)
			{
				ModelState.AddModelError(string.Empty, "Lot Service inoperative");
				return View();
			}
		}

		[Authorize(Policy = "IsSeller")]
		[Route("{id}/item/add")]
		public async Task<IActionResult> AddItem(int id)
		{
			if (id != 0)
			{
				var bearer = User.FindFirst("Access_Token").Value;
				var lot = await _service.Get(id, bearer);

				if (lot?.Seller == null || lot.Seller?.Id != int.Parse(User.FindFirst(c => c.Type == "nameid").Value))
				{
					return RedirectToAction("Index", "Home");
				}

				var model = new LotAddViewModel { 
					LotId = id,
					StartPrice = lot.StartPrice,
					EndPrice = lot.EndPrice,
					Items = lot.Items.Select(p => new ProductViewModel(p)).ToList()
				};
				return View(model);
			}

			return View(new LotAddViewModel { LotId = id });
		}

		[HttpPost]
		[RequestFormLimits(ValueCountLimit = 7500)]
		[Authorize(Policy = "IsSeller")]
		[Route("{id}/item/add")]
		public async Task<IActionResult> AddItem(LotAddViewModel model)
		{
			if (model.LotId == 0 && model.Items?.Count == 0)
			{
				ModelState.AddModelError(string.Empty, "Nothing was selected");
			}

			if (ModelState.IsValid == false)
			{
				return View(model);
			}

			var bearer = User.FindFirst("Access_Token").Value;

			// Although the viewmodel is ready for a list, we only implement it
			// as a single selection at present
			Item item = default;
			if (model.Items.Count(x => x.VersionId == x.VersionId) > 0)
			{
				var selectedItemVersion = model.Items.First().Versions.First(v => v.VersionId == model.Items.First().VersionId);
				item = new Item(selectedItemVersion);
				item.Notes = model.Items.First(x => x.Id == 0).Notes;

				foreach (var expansion in model.Items.First().Expansions.Where(e => e.IsChecked))
				{
					ProductViewModel selectedExpansionVersion;
					if (expansion?.Versions.Count > 1)
					{
						selectedExpansionVersion = expansion.Versions.FirstOrDefault(v => v.VersionId == expansion.VersionId);
					}
					else
					{
						selectedExpansionVersion = expansion.Versions.FirstOrDefault();
					}
					if (selectedItemVersion != null)
					{
						item.Expansions.Add(new Expansion(selectedExpansionVersion)
						{
							ItemId = item.Id,
							ProductId = selectedExpansionVersion.Id,
							Notes = expansion.Notes
						});
					}
				}
			}

			try
			{
				if (model.LotId == 0 && item != default)
				{
					var lot = new Lot
					{
						StartPrice = model.StartPrice.Value,
						EndPrice = model.EndPrice.Value,
						Items = new List<Item> { item }
					};
				
					var newLot = await _service.Add(lot, bearer);

					return View("AddConfirmation", newLot);
				} 
				else
				{
					if (item != default)
					{
						await _itemService.Add(model.LotId, item, bearer);
					}

					var lot = await _service.Get(model.LotId, bearer);

					if (lot.StartPrice != model.StartPrice || lot.EndPrice != model.EndPrice)
					{
						lot.StartPrice = model.StartPrice.Value;
						lot.EndPrice = model.EndPrice.Value;
						lot = await _service.Edit(lot, bearer);
					}
					return View("AddConfirmation", lot);
				}
			}
			catch (BrokenCircuitException)
			{
				ModelState.AddModelError(string.Empty, "Lot Service inoperative");
				return View();
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		[HttpPost]
		[Authorize(Policy = "IsSeller")]
		[Route("{id}/delete/{itemId}")]
		public async Task<IActionResult> DeleteItem(int id, int itemId)
		{
			if (id != 0)
			{
				var bearer = User.FindFirst("Access_Token").Value;

				var lot = await _service.Get(id, bearer);

				if (lot?.Seller == null || lot.Seller?.Id != int.Parse(User.FindFirst(c => c.Type == "nameid").Value))
				{
					return RedirectToAction("Index", "Home");
				}

				if (lot.Items.Count <= 1)
				{
					ModelState.AddModelError(string.Empty, "A lot cannot be empty. Add an item before deleting the last one");
					return View("EditItem", lot);
				}

				await _itemService.Delete(itemId, id, bearer);

				return RedirectToAction("Index", new { id = id });
			}

			return View(new LotAddViewModel { LotId = id });
		}

		[HttpPost]
		[Route("{id}/makeactive")]
		public async Task<IActionResult> MakeActive(int id)
		{
			try
			{
				var bearer = User.FindFirst("Access_Token").Value;
				if (ModelState.IsValid == false)
				{
					return new EmptyResult();
				}

				var auctionId = int.Parse(User.FindFirst("auction").Value);
				await _service.MakeActive(id, auctionId, bearer);
				return new EmptyResult();
			}
			catch (BrokenCircuitException)
			{
				ModelState.AddModelError(string.Empty, "Lot Service inoperative");
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError(string.Empty, ex.Message);
				return View();
			}
		}

		[HttpPost]
		[Route("sell")]
		public async Task<IActionResult> Sell(Lot model)
		{
			try
			{
				var bearer = User.FindFirst("Access_Token").Value;
				if (ModelState.IsValid == false)
				{
					// This is a rare-enough event that we can swallow the
					// cost of getting the model to avoid passing a load 
					// of hidden controls to the view
					var lot = await _service.Get(model.Id, bearer);
					lot.SalePrice = model.SalePrice;
					return PartialView("_Lot", lot);
				}

				var auctionId = int.Parse(User.FindFirst("auction").Value);
				await _service.Sell(model.Id, auctionId, model.SalePrice.Value, model.Seller.Id, bearer);
				return new EmptyResult();
			}
			catch (BrokenCircuitException)
			{
				ModelState.AddModelError(string.Empty, "Lot Service inoperative");
				return View();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError(string.Empty, ex.Message);
				return View();
			}
		}

		[HttpGet]
		[Route("{id}/delete")]
		public async Task<IActionResult> Delete(int id)
		{
			try
			{
				var bearer = User.FindFirst("Access_Token").Value;
				var auctionId = User.FindFirst("auction").Value;

				await _service.Delete(int.Parse(auctionId), id, bearer);
				return RedirectToAction("Index", "Auction");
			}
			catch (BrokenCircuitException)
			{
				ModelState.AddModelError(string.Empty, "Lot Service inoperative");
				return View("Index");
			}
			catch (Exception ex)
			{
				ModelState.AddModelError(string.Empty, ex.Message);
				return View("Index");
			}
		}
	}
}