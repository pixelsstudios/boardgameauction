﻿using BoardGameAuction.Models;
using BoardGameAuction.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BoardGameAuction.Controllers
{
    /// <summary>
    /// This controller handles Authentication but not Authorisation
    /// The Account Controller is different from most application in
    /// that a user can be signed in to an auction anonymously. As far
    /// as User.Identity.IsAuthenticated goes they are, but we do not
    /// know who they are. This controller is for adding and removing
    /// claims and roles to allow access to other roles.
    /// </summary>
    [Route("[controller]")]
    public class AccountController : Controller
    {
        private readonly ILogger<AuctionController> _logger;
        private readonly AppSettings _appSettings;
        private readonly IAccountService _service;

        public AccountController(ILogger<AuctionController> logger, IOptions<AppSettings> appSettings, IAccountService service)
        {
            _logger = logger;
            _appSettings = appSettings.Value;
            _service = service;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("register")]
        public IActionResult Register(string redirectUrl = "")
        {
            var model = new LoginOrRegisterViewModel { IsRegistering = true, RedirectUrl = redirectUrl };
            return View("LoginOrRegister", model);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("login")]
        public IActionResult Login(string redirectUrl = "")
        {
            var model = new LoginOrRegisterViewModel { RedirectUrl = redirectUrl };
            return View("LoginOrRegister", model);
        }

        [HttpPost]
        [Route("register")]
        [Route("login")]
        public async Task<IActionResult> LoginOrRegister(LoginOrRegisterViewModel model)
        {
            if (model.IsRegistering)
            {
                if (model.Register.Password != model.Register.ConfirmPassword)
                {
                    ModelState.AddModelError("Register_ConfirmPassword", "The passwords do not match");
                }
                if (ModelState.Any(e => e.Value.ValidationState == ModelValidationState.Invalid && e.Key.StartsWith("Register.")))
                {
                    var allErrors = ModelState.Where(e => e.Value.ValidationState == ModelValidationState.Invalid && e.Key.StartsWith("Register.")).SelectMany(v => v.Value.Errors);
                    return View(model);
                }

                try
                {
                    await _service.Register(model.Register);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                    return View(model);
                }

                // Login after a register
                model.IsRegistering = false;
                model.Login.Forename = model.Register.Forename;
                model.Login.Surname = model.Register.Surname;
                model.Login.Password = model.Register.Password;

                foreach(var error in ModelState.Where(e => e.Value.ValidationState == ModelValidationState.Invalid && e.Key.StartsWith("Login.")))
                {
                    ModelState.ClearValidationState(error.Key);
                }
            }

            if (ModelState.Any(e => e.Value.ValidationState == ModelValidationState.Invalid && e.Key.StartsWith("Login.")))
            {
                var allErrors = ModelState.Where(e => e.Value.ValidationState == ModelValidationState.Invalid && e.Key.StartsWith("Login.")).SelectMany(v => v.Value.Errors);
                return View(model);
            }

            try
            {
                var bearer = await _service.Login(model.Login, User.HasClaim(c => c.Type == "Access_Token") ? User.FindFirst(c => c.Type == "Access_Token").Value : "");

                if (bearer.Length == 0)
                {
                    ModelState.AddModelError(string.Empty, "Bearer Token creation failed");
                    return View(model);
                }

                try
                {
                    await TryUseToken(bearer);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, "Bearer Token validation failed");
                    _logger.LogError(ex, "Bearer Token validation failed", model.Login.Forename, model.Login.Surname);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(model);
            }

            if (string.IsNullOrEmpty(model.RedirectUrl) == false && Url.IsLocalUrl(model.RedirectUrl))
            {
                return Redirect(model.RedirectUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize]
        [Route("becomeadmin")]
        public IActionResult BecomeAdmin(string redirectUrl = "")
        {
            var model = new BecomeAdminViewModel { 
                RedirectUrl = redirectUrl 
            };
            return View(model);
        }

        [HttpPost]
        [Authorize]
        [Route("becomeadmin")]
        public async Task<IActionResult> BecomeAdmin(BecomeAdminViewModel model)
        {
            if (ModelState.IsValid == false)
            {
                return View(model);
            }

            try
            {
                var bearer = await _service.BecomeAdmin(model, User.FindFirst(c => c.Type == "Access_Token").Value);

                if (bearer.Length == 0)
                {
                    ModelState.AddModelError(string.Empty, "Bearer Token creation failed");
                    return View(model);
                }

                try
                {
                    await TryUseToken(bearer);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, "Bearer Token validation failed");
                    _logger.LogError(ex, "Bearer Token validation failed");
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Password", ex.Message);
                return View(model);
            }

            if (string.IsNullOrEmpty(model.RedirectUrl) == false && Url.IsLocalUrl(model.RedirectUrl))
            {
                return Redirect(model.RedirectUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        private async Task TryUseToken(string bearer)
        {
            var handler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.JWTTokenSecret);
            handler.ValidateToken(bearer, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = true,
                ValidIssuer = _appSettings.JWTIssuer,
                ValidateAudience = true,
                ValidAudience = _appSettings.JWTAudience,
            }, out SecurityToken validatedToken);

            var securityToken = handler.ReadToken(bearer) as JwtSecurityToken;
            var tokenClaims = (List<Claim>)securityToken.Claims;
            tokenClaims.Add(new Claim("Access_Token", bearer));
            var claimsIdentity = new ClaimsIdentity(tokenClaims, CookieAuthenticationDefaults.AuthenticationScheme);

            await HttpContext.SignInAsync(new ClaimsPrincipal(claimsIdentity));
        }

        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}