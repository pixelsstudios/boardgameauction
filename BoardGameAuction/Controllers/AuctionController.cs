﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BoardGameAuction.Models;
using BoardGameAuction.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Polly.CircuitBreaker;

namespace BoardGameAuction.Controllers
{
	public class AuctionController : Controller
    {
		private readonly ILogger<AuctionController> _logger;
		private readonly AppSettings _appSettings;
		private readonly IAuctionService _service;
		private readonly IAccountService _accountService;

		public AuctionController(ILogger<AuctionController> logger, IOptions<AppSettings> appSettings, IAuctionService service, IAccountService accountService)
		{
			_logger = logger;
			_appSettings = appSettings.Value;
			_service = service;
			_accountService = accountService;
		}

		[Authorize]
	    public async Task<IActionResult> Index()
	    {
			var bearer = User.FindFirst("Access_Token").Value;
			try
			{
				var auction = await _service.Get(bearer);

				if (auction.Status == Models.Types.AuctionStatus.Ended)
				{
					return RedirectToAction("Summary");
				}

				if (User.FindFirst("role")?.Value == ((int)Models.Types.RoleType.Seller).ToString())
				{
					var balance = await _accountService.Balance(bearer);
					ViewData.Add("balance", balance.ToString("C0"));
				}

				ViewData.Add("activeLotId", auction.ActiveLotId);
				return View(auction);
			}
			catch (BrokenCircuitException)
			{
				ModelState.AddModelError(string.Empty, "Auction Service inoperative");
				return View();
			}
			catch (Exception ex)
			{
				await HttpContext.SignOutAsync();
				return RedirectToAction("Index", "Home");
			}
	    }

		[HttpGet]
		[Authorize(Policy = "IsAdmin")]
		[Route("addconfirmation")]
		public async Task<IActionResult> AddConfirmation()
		{
			var bearer = User.FindFirst("Access_Token").Value;
			try
			{
				var auction = await _service.Get(bearer);
				var model = new AuctionEditViewModel(auction);

				return View(model);
			}
			catch (BrokenCircuitException)
			{
				ModelState.AddModelError(string.Empty, "Auction Service inoperative");
				return View();
			}
		}

		[HttpPost]
		[Authorize(Policy = "IsAdmin")]
		[Route("addconfirmation")]
		public async Task<IActionResult> AddConfirmation(AuctionEditViewModel model)
		{
			if (model.Password != model.ConfirmPassword)
			{
				ModelState.AddModelError("Password", "Passwords must match");
			}
			if (ModelState.IsValid == false) { return View(model); }

			var bearer = User.FindFirst("Access_Token").Value;
			try
			{
				var auction = await _service.Edit(model, bearer);
				return RedirectToAction("Index", "Auction");
			}
			catch (BrokenCircuitException)
			{
				ModelState.AddModelError(string.Empty, "Auction Service inoperative");
				return View();
			}
		}

		[HttpGet]
		[Authorize(Policy = "IsAdmin")]
		public async Task<IActionResult> Edit()
		{
			var bearer = User.FindFirst("Access_Token").Value;
			var auction = await _service.Get(bearer);
			return View(new AuctionEditViewModel(auction));
		}

		[HttpPost]
		[Authorize(Policy = "IsAdmin")]
		public async Task<IActionResult> Edit(AuctionEditViewModel model)
		{
			if (model.Password != model.ConfirmPassword)
			{
				ModelState.AddModelError("ConfirmPassword", "The passwords do not match");
			}
			if (ModelState.IsValid == false)
			{
				if (ModelState.Values
					.Where(v => v.ValidationState == Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Invalid)
					.All(e => e.Errors.Count == 1 && e.Errors[0].ErrorMessage.Contains("password is needed")) == false)
				{
					// We can allow missing passwords, but supplied ones 
					// must be valid
					return View(model);
				}
				else
				{
					// Use the magic string that will leave the password as it is
					model.Password = "[DoNotUpdatePassword]";
				}
			}

			var bearer = User.FindFirst("Access_Token").Value;

			var auction = new Auction
			{
				Id = model.Id,
				Code = model.Code,
				Password = model.Password,
				Type = model.Type,
				Start = model.Start,
				End = model.End
			};
			var response = await _service.Edit(auction, bearer);

			return RedirectToAction("Index", "Home");
		}

		[Authorize]
		[Route("summary")]
		public async Task<IActionResult> Summary()
		{
			var bearer = User.FindFirst("Access_Token").Value;

			var auction = await _service.Summary(bearer);

			AuctionSummaryViewModel model = new AuctionSummaryViewModel
			{
				AuctionId = auction.Id,
				CountItemsSold = auction.Lots.Where(l => l.SalePrice.HasValue && l.SalePrice != -1).Sum(l => l.Items.Count),
				CountLotsSold = auction.Lots.Count(l => l.SalePrice.HasValue && l.SalePrice != -1),
				ValueSold = auction.Lots.Where(l => l.SalePrice.HasValue && l.SalePrice != -1).Sum(l => l.SalePrice.Value),
				Sellers = auction.Lots
					.GroupBy(g => g.Seller?.Id)
					.Select(s => new AuctionSummaryItem
					{
						Seller = s.First().Seller,
						Total = s.Sum(p => p.SalePrice ?? 0),
						Lots = s.OrderBy(o => o.SalePrice.HasValue)
							.ThenBy(o => (o.SalePrice ?? 0) == -1)
							.ThenBy(o => string.Join(",", o.Items.Select(n => n.Name)))
					})
					.OrderBy(o => o.Seller?.Name)
					.ToList(),
			};
			return View("Summary", model);
		}

		[Authorize]
		public async Task<IActionResult> Leave()
		{
			var bearer = User.FindFirst("Access_Token").Value;
			try
			{
				bearer = await _service.Leave(bearer);
			}
			catch (BrokenCircuitException)
			{
				ModelState.AddModelError(string.Empty, "Auction Service inoperative");
				return View();
			}

			if (bearer.Length != 0)
			{
				var handler = new JwtSecurityTokenHandler();
				try
				{
					var key = Encoding.ASCII.GetBytes(_appSettings.JWTTokenSecret);
					handler.ValidateToken(bearer, new TokenValidationParameters
					{
						ValidateIssuerSigningKey = true,
						IssuerSigningKey = new SymmetricSecurityKey(key),
						ValidateIssuer = true,
						ValidIssuer = _appSettings.JWTIssuer,
						ValidateAudience = true,
						ValidAudience = _appSettings.JWTAudience,
					}, out SecurityToken validatedToken);

					var securityToken = handler.ReadToken(bearer) as JwtSecurityToken;
					var tokenClaims = (List<Claim>)securityToken.Claims;
					tokenClaims.Add(new Claim("Access_Token", bearer));

					var claimsIdentity = new ClaimsIdentity(tokenClaims, CookieAuthenticationDefaults.AuthenticationScheme);

					await HttpContext.SignInAsync(new ClaimsPrincipal(claimsIdentity));
				}
				catch (Exception ex)
				{
					return StatusCode(StatusCodes.Status500InternalServerError, new Exception("Bearer Token validation failed", ex));
				}
			}
			
			return RedirectToAction("Index", "Home");
		}

		[Authorize]
		[Authorize(Policy = "IsAdmin")]
		[Route("start")]
		public async Task<IActionResult> Start()
		{
			var bearer = User.FindFirst("Access_Token").Value;

			try
			{
				await _service.Start(bearer);
				return RedirectToActionPermanent("Index");
			}
			catch (Exception ex)
			{
				var auction = await _service.Get(bearer);
				var model = new AuctionEditViewModel(auction);
				ModelState.AddModelError(string.Empty, ex.ToString());
				return View("Edit", model);
			}
		}

		[Authorize]
		[Authorize(Policy = "IsAdmin")]
		[Route("stop")]
		public async Task<IActionResult> Stop()
		{
			var bearer = User.FindFirst("Access_Token").Value;

			try
			{
				await _service.Stop(bearer);
				return RedirectToActionPermanent("Summary");
			}
			catch (Exception ex)
			{
				var auction = await _service.Get(bearer);
				var model = new AuctionEditViewModel(auction);
				ModelState.AddModelError(string.Empty, ex.ToString());
				return View("Edit", model);
			}
		}
	}
}