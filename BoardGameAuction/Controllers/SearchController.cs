﻿using BoardGameAuction.Models;
using BoardGameAuction.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly.CircuitBreaker;
using System.Linq;
using System.Threading.Tasks;

namespace BoardGameAuction.Controllers
{
	[Route("[controller]")]
	public class SearchController : Controller
    {
        private readonly ILogger<SearchController> _logger;
        private readonly AppSettings _appSettings;
		private readonly ISearchService _service;
		
		public SearchController(ILogger<SearchController> logger, IOptions<AppSettings> appSettings, ISearchService service)
        {
            _logger = logger;
            _appSettings = appSettings.Value;
			_service = service;
        }

		[HttpGet]
		[Route("byname")]
        public async Task<IActionResult> ByName([FromQuery] string query)
        {
			var bearer = User.FindFirst("Access_Token").Value;

			try
			{
				var searchResult = await _service.ByName(query, bearer);

				if (searchResult == null || searchResult.Items.Count == 0)
				{
					ModelState.AddModelError(string.Empty, "No results found");
				}

				return PartialView("_NameSearchResult", searchResult);
			}
			catch (BrokenCircuitException)
			{
				var model = new NameSearchResponse();
				ModelState.AddModelError(string.Empty, "Search Service inoperative");
				return PartialView("_NameSearchResult", model);
			}
		}

		[HttpGet]
		[Route("byid")]
		public async Task<IActionResult> ById(int id)
		{
			if (id == 0)
			{
				ModelState.AddModelError(string.Empty, "No id provided");
				return View(id);
			}

			var bearer = User.FindFirst("Access_Token").Value;
			try
			{
				var searchResult = await _service.ById(new int[] { id }, bearer);
				if (searchResult.Items.Count == 0)
				{
					ModelState.AddModelError(string.Empty, $"No Results for {id}");
				}
				return PartialView("_IdSearchResult", new LotAddViewModel { Items = searchResult.Items.Select(p => new ProductViewModel(p)).ToList() });
			}
			catch (BrokenCircuitException)
			{
				var model = new LotAddViewModel();
				ModelState.AddModelError(string.Empty, "Search Service inoperative");
				return PartialView("_IdSearchResult", model);
			}
		}
	}
}