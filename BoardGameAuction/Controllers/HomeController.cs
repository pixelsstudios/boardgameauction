﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BoardGameAuction.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using Microsoft.Extensions.Options;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using BoardGameAuction.Services.Interfaces;
using Polly.CircuitBreaker;

namespace BoardGameAuction.Controllers
{
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;
		private readonly AppSettings _appSettings;
		private readonly IAuctionService _auctionService;

		public HomeController(ILogger<HomeController> logger, IOptions<AppSettings> appSettings, IAuctionService auctionService)
		{
			_logger = logger;
			_appSettings = appSettings.Value;
			_auctionService = auctionService;
		}

		public IActionResult Index()
		{
			if (User.HasClaim(c => c.Type == "Access_Token") == false) { 
				return View(); 
			}

			int auctionId;
			if (User.HasClaim(c => c.Type == "auction" && int.TryParse(c.Value, out auctionId) && auctionId > 0) == false) { 
				return View();
			}

			return RedirectToAction("Index", "Auction");
		}

		[HttpPost]
		public async Task<IActionResult> Index(AuctionRequestModel model)
		{
			if (model.CreateAuction == false && string.IsNullOrEmpty(model.Code))
			{
				ModelState.AddModelError("Code", "A Code is required to join an Auction");
			}
			if (model.CreateAuction && string.IsNullOrEmpty(model.Password))
			{
				ModelState.AddModelError("Password", "A password is required");
			}
			if (ModelState.IsValid == false) { return View(model); }

			string bearer;
			try
			{
				// The current bearer might contain user details
				var existingBearer = User.FindFirst(c => c.Type == "Access_Token");
				if (model.CreateAuction == false)
				{
					bearer = await _auctionService.Join(model.Code, existingBearer?.Value);
					if (string.IsNullOrEmpty(bearer))
					{
						ModelState.AddModelError("Code", "There isn't an active auction with that code");
					}
				}
				else
				{
					bearer = await _auctionService.Add(model.Password, existingBearer?.Value);
				}
				if (ModelState.IsValid == false) { return View(model); }
			}
			catch (BrokenCircuitException)
			{
				ModelState.AddModelError(string.Empty, "Auction service inoperable");
				return View(model);
			}
			catch (Exception ex)
			{
				var betterError = ex?.InnerException.Message;
				ModelState.AddModelError("Code", string.IsNullOrEmpty(betterError) ? "Couldn't find an auction with that code" : betterError);
				return View(model);
			}

			if (string.IsNullOrEmpty(bearer))
			{
				return RedirectToAction();
			}

			var handler = new JwtSecurityTokenHandler();
			try
			{
				var key = Encoding.ASCII.GetBytes(_appSettings.JWTTokenSecret);
				handler.ValidateToken(bearer, new TokenValidationParameters
				{
					ValidateIssuerSigningKey = true,
					IssuerSigningKey = new SymmetricSecurityKey(key),
					ValidateIssuer = true,
					ValidIssuer = _appSettings.JWTIssuer,
					ValidateAudience = true,
					ValidAudience = _appSettings.JWTAudience,
				}, out SecurityToken validatedToken);

				var securityToken = handler.ReadToken(bearer) as JwtSecurityToken;
				var tokenClaims = (List<Claim>)securityToken.Claims;
				tokenClaims.Add(new Claim("Access_Token", bearer));

				var claimsIdentity = new ClaimsIdentity(tokenClaims, CookieAuthenticationDefaults.AuthenticationScheme);

				await HttpContext.SignInAsync(new ClaimsPrincipal(claimsIdentity));
			}
			catch (Exception ex)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, new Exception("Bearer Token validation failed", ex));
			}
				
			if (model.CreateAuction)
			{
				return RedirectToAction("AddConfirmation", "Auction");
			}
			return RedirectToAction("Index", "Auction");
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		[Route("401")]
		public IActionResult Error401()
		{
			return View("Error401");
		}
	}
}
