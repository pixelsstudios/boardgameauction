﻿namespace BoardGameAuction
{
	public class AppSettings
	{
		public string apiUrl { get; set; }
		public string JWTTokenSecret { get; set; }
		public string JWTIssuer { get; set; }
		public string JWTAudience { get; set; }
	}
}