﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace BoardGameAuction.Hubs
{
	public class AccountHub : Hub
	{
		private readonly IHubContext<AccountHub> _context;

		public AccountHub(IHubContext<AccountHub> context)
		{
			_context = context;
		}

		public override Task OnConnectedAsync()
		{
			if (Context.User.Identity.IsAuthenticated)
			{
				if (Context.User.HasClaim(c => c.Type == "nameid") && Context.User.HasClaim(c => c.Type == "auctionCode"))
				{
					string nameId = Context.User.FindFirst("nameid").Value;
					_context.Groups.AddToGroupAsync(Context.ConnectionId, nameId);
				}
			}
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			if (Context.User.Identity.IsAuthenticated)
			{
				if (Context.User.HasClaim(c => c.Type == "nameid") && Context.User.HasClaim(c => c.Type == "auctionCode"))
				{
					string nameId = Context.User.FindFirst("nameid").Value;
					_context.Groups.RemoveFromGroupAsync(Context.ConnectionId, nameId);
				}
			}
			return base.OnDisconnectedAsync(exception);
		}

		public async Task SendBalance(int userId, double value)
		{
			try
			{
				await _context.Clients.Group(userId.ToString()).SendAsync("SendBalance", value.ToString("C0"));
			}
			catch (Exception ex)
			{
				// Catch all exceptions
				// Update messages should not interfere with regular running
				Console.WriteLine(ex.Message);
			}
		}
	}
}