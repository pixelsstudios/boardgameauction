﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace BoardGameAuction.Hubs
{
	public class AuctionHub : Hub
	{
		private readonly IHubContext<AuctionHub> _context;

		public AuctionHub(IHubContext<AuctionHub> context)
		{
			_context = context;
		}

		public override Task OnConnectedAsync()
		{
			if (Context.User.Identity.IsAuthenticated)
			{
				if (Context.User.HasClaim(c => c.Type == "auction"))
				{
					string auctionId = Context.User.FindFirst("auction").Value;
					_context.Groups.AddToGroupAsync(Context.ConnectionId, $"auction-{auctionId}");
				}
			}
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			if (Context.User.Identity.IsAuthenticated)
			{
				if (Context.User.HasClaim(c => c.Type == "auction"))
				{
					string auctionId = Context.User.FindFirst("auction").Value;
					_context.Groups.RemoveFromGroupAsync(Context.ConnectionId, $"auction-{auctionId}");
				}
			}
			return base.OnDisconnectedAsync(exception);
		}

		public async Task RemoveLot(int auctionId, int lotId)
		{
			try
			{
				await _context.Clients.Group($"auction-{auctionId}").SendAsync("RemoveLot", lotId.ToString());
			}
			catch (Exception ex)
			{
				// Catch all exceptions
				// Update messages should not interfere with regular running
				Console.WriteLine(ex.Message);
			}
		}

		public async Task MakeLotActive(int auctionId, int lotId)
		{
			try
			{
				await _context.Clients.Group($"auction-{auctionId}").SendAsync("MakeLotActive", lotId.ToString());
			}
			catch (Exception ex)
			{
				// Catch all exceptions
				// Update messages should not interfere with regular running
				Console.WriteLine(ex.Message);
			}
		}
	}
}
