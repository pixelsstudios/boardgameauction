﻿using NUnit.Framework;
using BoardGameAuction.Models;

namespace UnitTests.BoardGameAuction.API.ModelTests
{
    public class LotModelTests
	{
        [Test]
        [TestCase(0, ExpectedResult = null)]
        [TestCase(20, ExpectedResult = null)]
        [TestCase(123, ExpectedResult = "Scrooge McDuck")]
        public string Given_CallingForNonAdmins_ShouldShowSellersSellerDetails(int sellerId)
        {
            // Arrange
            var model = new Lot 
            {
                Seller = new Person { Id = 123, Name = "Scrooge McDuck" },
                StartPrice = 100,
                EndPrice = 50
            };


            // Act
            var result = model.ForNonAdmins(sellerId);

            // Assert
            return result.Seller?.Name;
        }

        [Test]
        [TestCase(0, ExpectedResult = 0)]
        [TestCase(20, ExpectedResult = 0)]
        [TestCase(123, ExpectedResult = 100)]
        public double Given_CallingForNonAdmins_ShouldShowSellersStartPrice(int sellerId)
        {
            // Arrange
            var model = new Lot
            {
                Seller = new Person { Id = 123, Name = "Scrooge McDuck" },
                StartPrice = 100,
                EndPrice = 50
            };


            // Act
            var result = model.ForNonAdmins(sellerId);

            // Assert
            return result.StartPrice;
        }

        [Test]
        [TestCase(0, ExpectedResult = 0)]
        [TestCase(20, ExpectedResult = 0)]
        [TestCase(123, ExpectedResult = 50)]
        public double Given_CallingForNonAdmins_ShouldShowSellersEndPrice(int sellerId)
        {
            // Arrange
            var model = new Lot
            {
                Seller = new Person { Id = 123, Name = "Scrooge McDuck" },
                StartPrice = 100,
                EndPrice = 50
            };


            // Act
            var result = model.ForNonAdmins(sellerId);

            // Assert
            return result.EndPrice;
        }
    }
}