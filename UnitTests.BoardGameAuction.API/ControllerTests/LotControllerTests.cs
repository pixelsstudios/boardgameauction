﻿using BoardGameAuction.API;
using BoardGameAuction.API.Services;
using BoardGameAuction.API.Services.Interfaces;
using Microsoft.Extensions.Options;
using NSubstitute;
using NUnit.Framework;
using System.Threading.Tasks;
using BoardGameAuction.API.Controllers;

namespace UnitTests.BoardGameAuction.API.ControllerTests
{
    public class LotControllerTests
	{
        private IOptions<AppSettings> _configuration;
        private ILotService _service;
        private IAccountService _accountService;
        private LotController _controller;

        [SetUp]
        public void Setup()
        {
            _configuration = Options.Create(new AppSettings
            {
                BGGImgUrlRoot = "http://www.somewhere.com/images/"
            });
            _service = Substitute.For<LotService>();
            _accountService = Substitute.For<AccountService>();
            _controller = new LotController(_service, _accountService);
        }

        [Test]
        [TestCase(3, 0, ExpectedResult = 0)]
        [TestCase(3, 50, ExpectedResult = 1)]
        [TestCase(3, 51, ExpectedResult = 0)]
        [TestCase(2, 0, ExpectedResult = 1)]
        [TestCase(2, 50, ExpectedResult = 1)]
        [TestCase(2, 51, ExpectedResult = 1)]
        public async Task Given_Deleting_ShouldOnlyDeleteIfAdminOrSellerOfLot(int roleId, int SellerId)
        {
            //// Arrange
            //const int auctionId = 4;
            //const int lotId = 6;
            //_service.Delete(auctionId, lotId).Returns(true);


            //// Act
            //var result = await _controller.Delete(passedAuction, passedLot);

            //// Assert
            //return _service.Received(1).Delete(auctionId, lotId);
        }
    }
}
