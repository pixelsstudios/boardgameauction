﻿using BoardGameAuction.API;
using BoardGameAuction.Models;
using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.API.Services;
using BoardGameAuction.API.Services.Interfaces;
using Microsoft.Extensions.Options;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace UnitTests.BoardGameAuction.API.ServiceTests
{
    class ExpansionServiceTests
    {
        private IOptions<AppSettings> _configuration;
        private IExpansionRepository _repository;
        private IExpansionService _service;

        [SetUp]
        public void Setup()
        {
            _configuration = Substitute.For<IOptions<AppSettings>>();
            _repository = Substitute.For<IExpansionRepository>();
            _service = new ExpansionService(_configuration, _repository);
        }

        #region Add
        [Test]
        public void Given_Adding_Then_ShouldThrowIfAlreadyExists()
        {
            // Arrange
            var theExpansion = new Expansion
            {
                ItemId = 4,
                BGGId = 123
            };
            _repository.Add(Arg.Any<Expansion>()).Returns(theExpansion);

            // Act / Assert
            Assert.ThrowsAsync<ArgumentException>(async () => await _service.Add(theExpansion));
        }

        [Test]
        [TestCase(0, 0)]
        [TestCase(0, 1)]
        [TestCase(1, 0)]
        public void Given_Adding_Then_ShouldErrorIfMissingIds(int itemId, int bggId)
        {
            // Act / Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await _service.Add(new Expansion() { ItemId = itemId, BGGId = bggId }));
        }

        [Test]
        public async Task Given_Adding_Then_ShouldAdd()
        {
            // Arrange
            _repository.Add(Arg.Any<Expansion>()).Returns(new Expansion { Id = 2, BGGId = 3 });

            // Act
            await _service.Add(new Expansion { ItemId = 123, BGGId = 456 });

            // Assert
            await _repository.Received(1).Add(Arg.Is<Expansion>(x => x.ItemId == 123 && x.BGGId == 456));
        }
        #endregion

        #region Edit
        [Test]
        [TestCase(0, 0, 0)]
        [TestCase(0, 1, 0)]
        [TestCase(0, 1, 1)]
        [TestCase(0, 0, 1)]
        [TestCase(1, 0, 1)]
        [TestCase(1, 0, 0)]
        [TestCase(1, 1, 0)]
        public void Given_Editing_Then_ShouldErrorIfMissingIds(int expansionId, int itemId, int bggId)
        {
            // Act / Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await _service.Edit(new Expansion() { Id = expansionId, ItemId = itemId, BGGId = bggId }));
        }

        [Test]
        public async Task Given_Editing_Then_ShouldEdit()
        {
            // Arrange
            const int expansionId = 3;
            const int itemId = 6;
            const int bggId = 20;

            // Act
            await _service.Edit(new Expansion() { Id = expansionId, ItemId = itemId, BGGId = bggId });

            // Assert
            await _repository.Received(1).Edit(Arg.Is<Expansion>(x => x.Id == expansionId && x.ItemId == itemId && x.BGGId == bggId));
        }
        #endregion

        #region Get
        [Test]
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 1)]
        public async Task Given_Getting_Then_ShouldShortcutRepoIfMissingId(int itemId, int callsExpected)
        {
            // Act 
            await _service.Get(itemId);

            // Assert
            await _repository.Received(callsExpected).Get(Arg.Any<int>());
        }

        [Test]
        public async Task Given_Getting_Then_ShouldGet()
        {
            // Arrange
            const int ItemId = 3;

            // Act
            await _service.Get(ItemId);

            // Assert
            await _repository.Received(1).Get(Arg.Is(ItemId));
        }
        #endregion

        #region GetAll
        [Test]
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 1)]
        public async Task Given_GettingAll_Then_ShouldShortcutRepoIfMissingId(int auctionId, int callsExpected)
        {
            // Act 
            await _service.GetAll(auctionId);

            // Assert
            await _repository.Received(callsExpected).GetAll(Arg.Is(auctionId));
        }

        [Test]
        public async Task Given_GettingAll_Then_ShouldGetAll()
        {
            // Arrange
            const int auctionId = 6;

            // Act
            await _service.GetAll(auctionId);

            // Assert
            await _repository.Received(1).GetAll(Arg.Is(auctionId));
        }
        #endregion
    }
}
