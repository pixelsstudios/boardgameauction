﻿using BoardGameAuction.API;
using BoardGameAuction.API.Repositories;
using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.API.Services;
using BoardGameAuction.Models;
using Microsoft.Extensions.Options;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UnitTests.BoardGameAuction.API.ServiceTests
{
    class SearchServiceTests
    {
        private IOptions<AppSettings> _configuration;
        private ISearchRepository _repository;
        private IProductRepository _productRepository;
        private SearchService _service;

        [SetUp]
        public void Setup()
        {
            _configuration = Options.Create(new AppSettings {
                MaxDataAge = 10, 
                BGGItemUrl = "http://www.somewhere.com"
            });
            _repository = Substitute.For<ISearchRepository>();
            _productRepository = Substitute.For<IProductRepository>();
            _service = new SearchService(_configuration, _repository, _productRepository);
        }

        [Test]
        [TestCase(new int[] { 1, 2, 3 }, new int[] { 1, 2 }, new int[] { 3 })]
        [TestCase(new int[] { 1, 2, 3 }, new int[] { 1 }, new int[] { 2, 3 })]
        [TestCase(new int[] { 1, 2, 3 }, new int[] {}, new int[] { 1, 2, 3 })]
        public async Task Given_Searching_Then_ShouldLookupIfMissing(int[] searchForIds, int[] databaseIds, int[] lookupIds)
        {
            // Arrange
            _productRepository.Get(Arg.Any<List<int>>(), Arg.Any<DateTime>()).Returns(databaseIds.Select(x => new Product { BGGId = x }).ToList());

            // Act
            var result = await _service.Search(searchForIds.ToList());

            // Assert
            await _repository.Received(1).GetByBGGId(Arg.Is<IEnumerable<int>>(x => string.Join("~~", x) == string.Join("~~", lookupIds)));
        }

        [Test]
        public async Task Given_Searching_Then_ShouldPassDateToRepo()
        {
            // Arrange
            const int maxAge = 17;
            DateTime maxDate = DateTime.UtcNow.Date.AddDays(-17);
            var appSettingsOptions = Options.Create(new AppSettings()
            {
                MaxDataAge = maxAge,
                BGGItemUrl = "http://www.somewhere.com"
            });
            _service = new SearchService(appSettingsOptions, _repository, _productRepository);

            // Act
            try
            {
                await _service.Search(new List<int> { 1 });
            }
            catch { }

            // Assert
            await _productRepository.Received(1).Get(Arg.Any<List<int>>(), Arg.Is(maxDate));
        }

        [Test]
        [TestCase(new int[] { 1, 2, 3 }, new int[] { 1, 2 }, new int[] { 1, 2 }, new int[] { 3 })]
        [TestCase(new int[] { 1, 2, 3 }, new int[] { 1 }, new int[] { 1 }, new int[] { 2, 3 })]
        [TestCase(new int[] { 1, 2, 3 }, new int[] { }, new int[] { }, new int[] { 1, 2, 3 })]
        public async Task Given_ItemsWereNotFound_Then_ShouldAddToDatabase(int[] searchForIds, int[] databaseIdsCurrent, int[] databaseIdsOld, int[] expectedToAdd) 
        {
            // Arrange
            // Database records that are in-date
            _productRepository.Get(Arg.Any<List<int>>(), Arg.Any<DateTime>()).Returns(databaseIdsCurrent.Select(x => new Product { BGGId = x }).ToList());
            // BGG Response
            _repository.GetByBGGId(Arg.Any<IEnumerable<int>>()).Returns(r => new IdSearchResponse { Items = ((IEnumerable<int>)r[0]).ToList().Select(x => new Product { BGGId = x, VersionId = x * 2 }).ToList() });
            // Database records regardless of date
            _productRepository.Get(Arg.Any<IEnumerable<int>>()).Returns(databaseIdsOld.Select(x => new Product { BGGId = x, VersionId = x * 2 }).ToList());

            // Act
            var result = await _service.Search(searchForIds.ToList());

            // Assert
            await _productRepository.Received(1).Add(Arg.Is<IEnumerable<ProductTable>>(x => string.Join("~~", x.Select(y => y.BGGId)) == string.Join("~~", expectedToAdd)));
        }

        [Test]
        [TestCase(new int[] { 1, 2, 3 }, new int[] { 1 })]
        public async Task Given_ItemsWereAddedToDatabase_Then_ShouldTrimRootFromImageUrl(int[] searchForIds, int[] databaseIds)
        {
            // Arrange
            const string imageRootUrl = "http://imagesplace.com/";
            // Database records that are in-date
            _productRepository.Get(Arg.Any<List<int>>(), Arg.Any<DateTime>()).Returns(databaseIds.Select(x => new Product { BGGId = x, ImageUrl = $"{x}.png" }).ToList());
            // BGG Response
            _repository.GetByBGGId(Arg.Any<IEnumerable<int>>()).Returns(r => new IdSearchResponse { Items = ((IEnumerable<int>)r[0]).ToList().Select(x => new Product { 
                BGGId = x, ImageUrl = $"{imageRootUrl}{x}.png" }).ToList() 
            });
            // Database records regardless of date
            _productRepository.Get(Arg.Any<IEnumerable<int>>()).Returns(databaseIds.Select(x => new Product { BGGId = x, ImageUrl = $"{x}.png" }).ToList());
            var appSettingsOptions = Options.Create(new AppSettings()
            {
                BGGImgUrlRoot = imageRootUrl
            });
            _service = new SearchService(appSettingsOptions, _repository, _productRepository);

            // Act
            var result = await _service.Search(searchForIds.ToList());

            // Assert
            await _productRepository.Received(1).Add(Arg.Is<IEnumerable<ProductTable>>(x => x.All(y => y.ImageUrl.Contains(imageRootUrl) == false)));
        }

        [Test]
        [TestCase(new int[] { 1, 2, 3 }, new int[] { 1, 2 }, new int[] { 3 }, new int[] { 3 })]
        [TestCase(new int[] { 1, 2, 3 }, new int[] { 1 }, new int[] { 2, 3 }, new int[] { 2, 3 })]
        [TestCase(new int[] { 1, 2, 3 }, new int[] { }, new int[] { 1, 2, 3 }, new int[] { 1, 2, 3 })]
        public async Task Given_ItemsWerePastDate_Then_ShouldUpdateDatabase(int[] searchForIds, int[] databaseIdsCurrent, int[] databaseIdsOld, int[] expectedToUpdate)
        {
            // Arrange
            // Database records that are in-date
            _productRepository.Get(Arg.Any<List<int>>(), Arg.Any<DateTime>()).Returns(databaseIdsCurrent.Select(x => new Product { BGGId = x }).ToList());
            // BGG Response
            _repository.GetByBGGId(Arg.Any<IEnumerable<int>>()).Returns(r => new IdSearchResponse { Items = ((IEnumerable<int>)r[0]).ToList().Select(x => new Product { BGGId = x, VersionId = x * 2 }).ToList() });
            // Database records regardless of date
            _productRepository.Get(Arg.Any<IEnumerable<int>>()).Returns(databaseIdsOld.Select(x => new Product { BGGId = x, VersionId = x * 2 }).ToList());

            // Act
            var result = await _service.Search(searchForIds.ToList());

            // Assert
            await _productRepository.Received(1).Edit(Arg.Is<IEnumerable<ProductTable>>(x => string.Join("~~", x.Select(y => y.BGGId)) == string.Join("~~", expectedToUpdate)));
        }

        [Test]
        [TestCase(new int[] { 1, 2, 3 }, new int[] { 1 }, new int[] { 2, 3 })]
        public async Task Given_ItemsWereUpdatedInDatabase_Then_ShouldTrimRootFromImageUrl(int[] searchForIds, int[] databaseIdsCurrent, int[] databaseIdsOld)
        {
            // Arrange
            const string imageRootUrl = "http://imagesplace.com/";
            // Database records that are in-date
            _productRepository.Get(Arg.Any<List<int>>(), Arg.Any<DateTime>()).Returns(databaseIdsCurrent.Select(x => new Product { BGGId = x, ImageUrl = $"{x}.png" }).ToList());
            // BGG Response
            _repository.GetByBGGId(Arg.Any<IEnumerable<int>>()).Returns(r => new IdSearchResponse
            {
                Items = ((IEnumerable<int>)r[0]).ToList().Select(x => new Product
                {
                    BGGId = x,
                    ImageUrl = $"{imageRootUrl}{x}.png"
                }).ToList()
            });
            // Database records regardless of date
            _productRepository.Get(Arg.Any<IEnumerable<int>>()).Returns(databaseIdsOld.Select(x => new Product { BGGId = x, ImageUrl = $"{x}.png" }).ToList());
            var appSettingsOptions = Options.Create(new AppSettings()
            {
                BGGImgUrlRoot = imageRootUrl
            });
            _service = new SearchService(appSettingsOptions, _repository, _productRepository);

            // Act
            var result = await _service.Search(searchForIds.ToList());

            // Assert
            await _productRepository.Received(1).Edit(Arg.Is<IEnumerable<ProductTable>>(x => x.All(y => y.ImageUrl.Contains(imageRootUrl) == false)));
        }

        [Test]
        public async Task Given_SearchingWithoutLookups_Then_ShouldGetResultsOrderedByNameThenBGGId()
        {
            // Arrange
            var ids = new int[] { 1, 2, 3, 4 };
            var things = new List<Product> {
                new Product { BGGId = ids[0], Name = "C" },
                new Product { BGGId = ids[1], Name = "A" },
                new Product { BGGId = ids[3], Name = "B" },
                new Product { BGGId = ids[2], Name = "B" }
            };
            _productRepository.Get(Arg.Any<List<int>>(), Arg.Any<DateTime>()).Returns(things);
            _repository.GetByBGGId(Arg.Any<IEnumerable<int>>()).Returns(r => new IdSearchResponse { Items = ((IEnumerable<int>)r[0]).ToList().Select(x => new Product { BGGId = x }).ToList() });

            // Act
            var result = await _service.Search(ids.ToList());

            // Assert
            Assert.AreEqual(new string[] { "A", "B", "B", "C" }, result.Items.Select(x => x.Name).ToArray());
            Assert.Greater(result.Items[2].BGGId, result.Items[1].BGGId);
        }

        [Test]
        public async Task Given_SearchingWithLookups_Then_ShouldGetResultsOrderedByNameThenBGGId()
        {
            // Arrange
            var databaseIds = new int[] { 10, 11, 12, 13 };
            var databaseThings = new List<Product> {
                new Product { BGGId = databaseIds[0], Name = "C" },
                new Product { BGGId = databaseIds[1], Name = "A" },
                new Product { BGGId = databaseIds[3], Name = "B" },
                new Product { BGGId = databaseIds[2], Name = "B" }
            };
            var bggId = new int[] { 9 };
            var product = new List<Product> {
                new Product { BGGId = bggId[0], Name = "C" }
            };
            _productRepository.Get(Arg.Any<List<int>>(), Arg.Any<DateTime>()).Returns(databaseThings);
            _repository.GetByBGGId(Arg.Is<IEnumerable<int>>(x => x.ToArray()[0] == bggId[0])).Returns(new IdSearchResponse { Items = product });
            _productRepository.Get(Arg.Any<IEnumerable<int>>()).Returns(new List<Product>());
            _productRepository.Edit(Arg.Any<IEnumerable<ProductTable>>()).Returns(new List<Product>());
            _productRepository.Add(Arg.Any<IEnumerable<ProductTable>>()).Returns(new List<Product>());
            
            // Act
            var result = await _service.Search(databaseIds.Concat(bggId).ToList());

            // Assert
            Assert.AreEqual(new string[] { "A", "B", "B", "C", "C" }, result.Items.Select(x => x.Name).ToArray());
            Assert.Greater(result.Items[2].BGGId, result.Items[1].BGGId);
            Assert.Greater(result.Items[4].BGGId, result.Items[3].BGGId);
        }

        [Test]
        [TestCase(new int[] { 1 }, ExpectedResult = 1)]
        [TestCase(new int[] { 4 }, ExpectedResult = 1)]
        [TestCase(new int[] { 1, 4 }, ExpectedResult = 2)]
        public async Task<int> Given_SearchingForTwoGames_Then_ShouldGetTwoItems(int[] searchForIds)
        {
            // Arrange
            var databaseThings = new List<Product> {
                new Product { 
                    BGGId = 1, Name = "Bus",
                    Versions = new List<Product>
                    {
                        new Product { BGGId = 2, Name = "Bus" },
                        new Product { BGGId = 3, Name = "Bus 20th Anniversary Edition - English Only" }
                    }
                },
                new Product { 
                    BGGId = 4, Name = "Alhambra",
                    Versions = new List<Product>
                    {
                        new Product { BGGId = 5, Name = "Alhambra" },
                        new Product { BGGId = 6, Name = "Alhambra: 15th Anniversary Revised Edition ‐ Queen multilingual edition 2019" }
                    }
                },
            };
            _productRepository.Get(Arg.Any<List<int>>(), Arg.Any<DateTime>()).Returns(r => databaseThings.Where(x => ((List<int>)r[0]).Contains(x.BGGId)).ToList());

            // Act
            var result = await _service.Search(searchForIds.ToList());

            // Assert
            return result.Items.Count;
        }

        [Test]
        public void Given_MappingVersionsForDatabase_Then_ShouldBecomeRows()
        {
            // Arrange
            const int bggId = 10;
            var products = new List<Product> { new Product { BGGId = bggId } };
            products[0].Versions = new List<Product>
            {
                new Product { BGGId = bggId, VersionId = 101 },
                new Product { BGGId = bggId, VersionId = 102 },
                new Product { BGGId = bggId, VersionId = 102 },
            };

            // Act
            var result = _service.PrepareForDatabase(products);

            // Assert
            Assert.AreEqual(3, result.Count(x => x.BGGId == bggId));
        }

        [Test]
        [TestCase(0, ExpectedResult = 0)]
        [TestCase(3, ExpectedResult = 3)]
        public int Given_MappingExpansionsForDatabase_Then_ShouldBecomeRows(int numberOfExpansions)
        {
            // Arrange
            const int bggId = 10;
            var products = new List<Product> { new Product 
            { 
                BGGId = bggId, 
                Expansions = numberOfExpansions > 0 ? new List<Product>() : null
            } };

            for (int expansion = 0; expansion < numberOfExpansions; expansion++)
            {
                products[0].Expansions.Add(new Product { BGGId = 100 + expansion, Versions = new List<Product> { new Product { VersionId = 1000 + expansion * 10 } } });
            }

            // Act
            var result = _service.PrepareForDatabase(products);

            // Assert
            return result.Count(x => x.ParentBGGId == bggId);
        }

        [Test]
        public void Given_MappingVersionsOfExpansionsForDatabase_Then_ShouldBecomeRows()
        {
            // Arrange
            const int parentBggId = 10;
            const int bggId = 101;
            var products = new List<Product> { new Product { BGGId = parentBggId } };
            products[0].Expansions = new List<Product> { new Product { BGGId = bggId } };
            products[0].Expansions[0].Versions = new List<Product> 
            { 
                new Product { BGGId = bggId, VersionId = 1011 },
                new Product { BGGId = bggId, VersionId = 1012 },
                new Product { BGGId = bggId, VersionId = 1013 } 
            };

            // Act
            var result = _service.PrepareForDatabase(products);

            // Assert
            Assert.AreEqual(3, result.Count(x => x.BGGId == bggId && x.ParentBGGId == parentBggId));
        }

        [Test]
        public void Given_MappingProductsWithNoVersionsForDatabase_Then_ShouldBecomeOneRow()
        {
            // Arrange
            const int bggId = 10;
            const int versionId = 101;
            var products = new List<Product> { 
                new Product { BGGId = bggId, VersionId = versionId } 
            };

            // Act
            var result = _service.PrepareForDatabase(products);

            // Assert
            Assert.AreEqual(1, result.Count(x => x.BGGId == bggId && x.VersionId == versionId));
        }

        [Test]
        public void Given_MappingExpansionsWithNoVersionsForDatabase_Then_ShouldBecomeOneRow()
        {
            // Arrange
            const int bggId = 10;
            var products = new List<Product> { new Product { BGGId = bggId } };
            products[0].Expansions = new List<Product> {
                new Product { BGGId = 101, VersionId = 1011 },
                new Product { BGGId = 102, VersionId = 1021 },
                new Product { BGGId = 103, VersionId = 1031 },
            };

            // Act
            var result = _service.PrepareForDatabase(products);

            // Assert
            Assert.AreEqual(3, result.Count(x => x.ParentBGGId == bggId));
        }
    }
}
