﻿using BoardGameAuction.API;
using BoardGameAuction.Models;
using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.API.Services;
using BoardGameAuction.API.Services.Interfaces;
using Microsoft.Extensions.Options;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoardGameAuction.Models.Types;

namespace UnitTests.BoardGameAuction.API.ServiceTests
{
    public class LotServiceTests
    {
        private IOptions<AppSettings> _configuration;
        private ILotRepository _repository;
        private IItemService _itemService;
        private ILotService _service;
        private IAuctionService _auctionService;

        [SetUp]
        public void Setup()
        {
            _configuration = Options.Create(new AppSettings
            {
                BGGImgUrlRoot = "http://www.somewhere.com/images/"
            });
            _repository = Substitute.For<ILotRepository>();
            _itemService = Substitute.For<IItemService>();
            _auctionService = Substitute.For<IAuctionService>();
            _service = new LotService(_configuration, _repository, _itemService, _auctionService);
        }

        #region Add
        [Test]
        public void Given_Adding_Then_ShouldThrowIfAlreadyExists()
        {
            // Arrange
            const int auctionId = 5;
            var theLot = new Lot
            {
                AuctionId = auctionId,
                Seller = new Person { Id = 10 },
                Items = new List<Item>
                    {
                        new Item { BGGId = 100 },
                        new Item { BGGId = 200 }
                    }
            };
            _auctionService.Get(auctionId, Arg.Any<RoleType>(), Arg.Any<int>()).Returns(new Auction { Start = DateTime.UtcNow.AddDays(1), End = DateTime.UtcNow.AddDays(2) });
            _repository.Add(Arg.Any<Lot>()).Returns(theLot);

            // Act / Assert
            Assert.ThrowsAsync<ArgumentException>(async () => await _service.Add(theLot));
        }

        [Test]
        public async Task Given_Adding_Then_ShouldAdd()
        {
            // Arrange
            _auctionService.Get(Arg.Any<int>(), Arg.Any<RoleType>(), Arg.Any<int>()).Returns(new Auction { Start = DateTime.UtcNow.AddDays(1), End = DateTime.UtcNow.AddDays(2) });
            _repository.Add(Arg.Any<Lot>()).Returns(new Lot { Id = 2, Items = new List<Item> { new Item() } });

            // Act
            await _service.Add(new Lot { AuctionId = 123 });

            // Assert
            await _repository.Received(1).Add(Arg.Is<Lot>(x => x.AuctionId == 123));
        }

        [Test]
        public void Given_Adding_Then_ShouldThrowIfNoAuctionId()
        {
            // Act / Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await _service.Add(new Lot() { AuctionId = 0 }));
        }

        [Test]
        [TestCase(0, 0)]
        [TestCase(0, 5)]
        [TestCase(5, 5)]
        [Ignore("Need validation rules")]
        public void Given_Adding_Then_ShouldThrowIfSalePricesAreWrong(double startPrice, double endPrice)
        {
            // TODO: Lots need to be able to independantly validate themseleves with no knowledge of the rules, which are determined by the auction
            // Arrange
            var theLot = new Lot
            {
                AuctionId = 6,
                Seller = new Person { Id = 27 },
                Items = new List<Item> { new Item() },
                StartPrice = startPrice,
                EndPrice = endPrice
            };

            // Act / Assert
            Assert.ThrowsAsync<ArgumentException>(async () => await _service.Add(theLot));
        }

        [Test]
        public async Task Given_Adding_Then_ShouldAddAllChildItemsToo()
        {
            // Arrange
            const int auctionId = 3;
            const int lotId = 5;
            var theLot = new Lot
            {
                AuctionId = auctionId,
                Seller = new Person { Id = 10 },
                Items = new List<Item>
                    {
                        new Item { BGGId = 100 },
                        new Item { BGGId = 200 }
                    }
            };
            _auctionService.Get(auctionId, Arg.Any<RoleType>(), Arg.Any<int>()).Returns(new Auction { Start = DateTime.UtcNow.AddDays(1), End = DateTime.UtcNow.AddDays(2) });
            _repository.Add(Arg.Any<Lot>()).Returns(new Lot { Id = lotId });

            // Act
            await _service.Add(theLot);

            // Assert
            await _itemService.Received(2).Add(Arg.Is<Item>(x => x.LotId == lotId), auctionId);
        }

        [Test]
        [TestCase("http://www.theinternet.com/images/", "http://www.theinternet.com/images/anImage.jpg", "anImage.jpg")]
        [TestCase("http://www.theinternet.com/images/", null, null)]
        public async Task Given_Adding_Then_ShouldRemoveImageRootFromUrls(string imgRootUrl, string fullImgUrl, string imgName)
        {
            // Arrange
            const int auctionId = 2;
            int lotId = 5;
            var appSettingsOptions = Options.Create(new AppSettings()
            {
                BGGImgUrlRoot = imgRootUrl
            });
            _service = new LotService(appSettingsOptions, _repository, _itemService, _auctionService);
            _auctionService.Get(auctionId, Arg.Any<RoleType>(), Arg.Any<int>()).Returns(new Auction { Start = DateTime.UtcNow.AddDays(1), End = DateTime.UtcNow.AddDays(2) });
            string imgUrl = _configuration.Value.BGGImgUrlRoot + imgName;

            var theLot = new Lot
            {
                AuctionId = auctionId,
                Seller = new Person(),
                Items = new List<Item> { new Item { BGGId = 100, ImageUrl = fullImgUrl } }
            };
            _repository.Add(Arg.Any<Lot>()).Returns(new Lot { Id = lotId });

            // Act
            await _service.Add(theLot);

            // Assert
            await _itemService.Received(1).Add(Arg.Is<Item>(x => x.ImageUrl == imgName), auctionId);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-5)]
        public void Given_Adding_Then_ShouldThrowIfAuctionStarted(int offsetFromNow)
        {
            // Arrange
            const int auctionId = 123;
            var theAuction = new Auction { Id = auctionId, Start = DateTime.UtcNow.AddMinutes(offsetFromNow), End = DateTime.UtcNow.AddDays(1) };
            _auctionService.Get(auctionId, Arg.Any<RoleType>(), Arg.Any<int>()).Returns(theAuction);
            var theLot = new Lot { AuctionId = auctionId };

            // Act / Assert
            Assert.ThrowsAsync<Exception>(async () => await _service.Add(theLot));
        }
        #endregion

        #region Edit
        [Test]
        [TestCase(0, 1)]
        [TestCase(0, 0)]
        [TestCase(1, 0)]
        public void Given_Editing_Then_ShouldErrorIfMissingIds(int lotId, int auctionId)
        {
            // Act / Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await _service.Edit(new Lot() { Id = lotId, AuctionId = auctionId }));
        }

        [Test]
        public async Task Given_Editing_Then_ShouldEdit()
        {
            // Arrange
            const int lotId = 3;
            const int auctionId = 6;
            _auctionService.Get(auctionId, Arg.Any<RoleType>(), Arg.Any<int>()).Returns(new Auction { Start = DateTime.UtcNow.AddDays(1), End = DateTime.UtcNow.AddDays(2) });

            // Act
            await _service.Edit(new Lot() { Id = lotId, AuctionId = auctionId });

            // Assert
            await _repository.Received(1).Edit(Arg.Is<Lot>(x => x.Id == lotId));
        }

        [Test]
        [TestCase(0)]
        [TestCase(-5)]
        public void Given_Editing_Then_ShouldThrowIfAuctionStarted(int offsetFromNow)
        {
            // Arrange
            const int auctionId = 123;
            var theAuction = new Auction { Id = auctionId, Start = DateTime.UtcNow.AddMinutes(offsetFromNow), End = DateTime.UtcNow.AddDays(1) };
            _auctionService.Get(auctionId, Arg.Any<RoleType>(), Arg.Any<int>()).Returns(theAuction);
            var theLot = new Lot { Id = 1, AuctionId = auctionId };

            // Act / Assert
            Assert.ThrowsAsync<Exception>(async () => await _service.Edit(theLot));
        }
        #endregion

        #region Get
        [Test]
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 1)]
        public async Task Given_Getting_Then_ShouldShortcutRepoIfMissingId(int lotId, int callsExpected)
        {
            // Act 
            await _service.Get(lotId, true, 0);

            // Assert
            await _repository.Received(callsExpected).Get(Arg.Any<int>());
        }

        [Test]
        public async Task Given_Getting_Then_ShouldGet()
        {
            // Arrange
            const int lotId = 3;

            // Act
            await _service.Get(lotId, true, 0);

            // Assert
            await _repository.Received(1).Get(Arg.Is(lotId));
        }

        [Test]
        [TestCase(true, 0)]
        [TestCase(false, 1)]
        public void Given_Getting_Then_ShouldOnlyShowAdminDataToAdmins(bool isAdmin, int expectedCalls)
        {
            // Arrange
            const int sellerId = 5;
            var lot = Substitute.For<Lot>();
            _repository.Get(Arg.Any<int>()).Returns(lot);

            // Act
            var result = _service.Get(1, isAdmin, sellerId);

            // Assert
            lot.Received(expectedCalls).ForNonAdmins(sellerId);
        }
        #endregion

        #region GetAll
        [Test]
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 1)]
        public async Task Given_GettingAll_Then_ShouldShortcutRepoIfMissingId(int auctionId, int callsExpected)
        {
            // Arrange
            _repository.GetAll(Arg.Any<int>()).Returns(new List<Lot> { new Lot() });

            // Act 
            await _service.GetAll(auctionId, false, 1);

            // Assert
            await _repository.Received(callsExpected).GetAll(Arg.Is(auctionId));
        }

        [Test]
        public async Task Given_GettingAll_Then_ShouldGetAll()
        {
            // Arrange
            const int auctionId = 6;
            _repository.GetAll(Arg.Any<int>()).Returns(new List<Lot> { new Lot() });

            // Act
            await _service.GetAll(auctionId, false, 1);

            // Assert
            await _repository.Received(1).GetAll(Arg.Is(auctionId));
        }
        #endregion

        #region Sell
        [Test]
        [TestCase(0)]
        [TestCase(0.0)]
        [TestCase(-1)]
        [TestCase(-1.0)]
        [TestCase(-0.1)]
        public void Given_Selling_Then_ShouldErrorIfPriceInvalid(double price)
        {
            // Act 
            Assert.ThrowsAsync<ArgumentException>(async () => await _service.Sell(10, price));
        }
        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-5)]
        public void Given_Selling_Then_ShouldShortcutRepoIfIdInvalid(int id)
        {
            // Act 
            Assert.ThrowsAsync<ArgumentException>(async () => await _service.Sell(id, 1.99));
        }

        [Test]
        public async Task Given_Selling_Then_ShouldSell()
        {
            // Arrange
            const int lotId = 6;
            const double price = 9.99;

            // Act
            await _service.Sell(lotId, price);

            // Assert
            await _repository.Received(1).Sell(Arg.Is(lotId), Arg.Is(price));
        }
        #endregion

        #region MakeActive
        [Test]
        [TestCase(0, 10)]
        [TestCase(-1, 10)]
        [TestCase(-1, 0)]
        [TestCase(-1, -1)]
        [TestCase(0, 0)]
        [TestCase(5, 0)]
        [TestCase(5, -1)]
        public void Given_MakingLotActive_Then_ShouldThrowIfIdsAreInvalid(int lotId, int auctionId)
        {
            // Act
            Assert.ThrowsAsync<ArgumentException>(async () => await _service.MakeActive(lotId, auctionId));
        }

        [Test]
        public async Task Given_MakingLotActive_Then_ShouldMakeLotActive()
        {
            // Arrange
            const int lotId = 6;
            const int auctionId = 9;

            // Act
            await _service.MakeActive(lotId, auctionId);

            // Assert
            await _repository.Received(1).MakeActive(lotId, auctionId);
        }
		#endregion

		#region Delete
		[Test]
        public async Task Given_Deleting_Then_ShouldDelete()
        {
            // Arrange
            const int auctionId = 4;
            const int lotId = 6;

            // Act
            await _service.Delete(auctionId, lotId);

            // Assert
            await _repository.Received(1).Delete(auctionId, lotId);
        }

        [Test]
        [TestCase(4, 6, ExpectedResult = true)]
        [TestCase(4, 7, ExpectedResult = false)]
        [TestCase(5, 6, ExpectedResult = false)]
        [TestCase(5, 7, ExpectedResult = false)]
        public async Task<bool> Given_Deleting_Then_ShouldReturnIfRowsWereDeleted(int passedAuction, int passedLot)
        {
            // Arrange
            const int auctionId = 4;
            const int lotId = 6;
            _repository.Delete(auctionId, lotId).Returns(true);

            // Act
            var result = await _service.Delete(passedAuction, passedLot);

            // Assert
            return result;
        }
        #endregion
    }
}