﻿using BoardGameAuction.API;
using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.API.Services;
using BoardGameAuction.API.Services.Interfaces;
using BoardGameAuction.Models;
using Microsoft.Extensions.Options;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace UnitTests.BoardGameAuction.API.ServiceTests
{
    public class AccountServiceTests
	{
        private IOptions<AppSettings> _configuration;
        private IAccountRepository _repository;
        private IPasswordService _passwordService;
        private AccountService _service;

        [SetUp]
        public void Setup()
        {
            _configuration = Substitute.For<IOptions<AppSettings>>();
            _repository = Substitute.For<IAccountRepository>();
            _passwordService = Substitute.For<IPasswordService>();
            _service = new AccountService(_configuration, _repository, _passwordService);
        }

		#region Register
        [Test]
        [TestCase("Dave", "Grohl", "Roswell")]
        [TestCase("Dave", "Grohl", "Studios")]
        [TestCase("Dave", "Grohl", "")]
        [TestCase("Taylor", "Hawkins", "Roswell")]
        public async Task Given_AnyRegisterParameters_Then_ShouldCallAllRepoEndPoints(string forename, string surname, string password)
        {
            // Arrange
            _repository.Get("Dave", "Grohl").Returns(new Person { Id = 606, Name = "Dave Grohl" });
            _repository.Get("Taylor", "Hawkins").Returns(new Person());
            _passwordService.VerifyHashedPassword("DaveGrohl", "Roswell", "Roswell").Returns(true);

            // Act
            try
            {
                await _service.Add(forename, surname, password);
            }
            catch { }

            // Assert
            await _repository.Received(2).Get(forename, surname);
            _passwordService.Received(1).HashPassword($"{forename}{surname}", password);
            _passwordService.Received(1).VerifyHashedPassword($"{forename}{surname}", Arg.Any<string>(), password);
        }

        [Test]
        public async Task Given_RegisteringWithExistingCredentials_Then_ShouldGetIdAndMessage()
        {
            // Arrange
            const int id = 606;
            const string forename = "Dave";
            const string surname = "Grohl";
            const string password = "Roswell";
            const string hashedPassword = "abcdefg";

            _repository.Get(forename, surname).Returns(new Person { Id = id, Name = $"{forename} {surname}", HashedPassword = hashedPassword });
            _passwordService.VerifyHashedPassword($"{forename}{surname}", hashedPassword, password).Returns(true);

            // Act
            var result = await _service.Add(forename, surname, password);

            // Assert
            Assert.AreEqual(id, result.Id);
            Assert.AreEqual("Account already exists", result.Message);
        }

        [Test]
        public async Task Given_RegisteringWithNewCredentials_Then_ShouldGetId()
        {
            // Arrange
            const int id = 606;
            const string forename = "Dave";
            const string surname = "Grohl";
            const string password = "Roswell";
            const string hashedPassword = "abcdefg";

            _repository.Get(forename, surname).Returns(new Person());
            _passwordService.HashPassword($"{forename}{surname}", password).Returns(hashedPassword);
            _repository.Add(forename, surname, hashedPassword).Returns(id);

            // Act
            var result = await _service.Add(forename, surname, password);

            // Assert
            Assert.AreEqual(id, result.Id);
            Assert.AreEqual("", result.Message);
        }

        [Test]
        public async Task Given_RegisteringWithExistingNameAndWrongPassword_Then_ShouldGetError()
        {
            // Arrange
            const int id = 606;
            const string forename = "Dave";
            const string surname = "Grohl";
            const string password = "Roswell";
            const string hashedPassword = "abcdefg";

            _repository.Get(forename, surname).Returns(new Person { Id = id, Name = $"{forename} {surname}", HashedPassword = hashedPassword });
            _passwordService.VerifyHashedPassword($"{forename}{surname}", hashedPassword, password).Returns(true);

            // Act
            var result = await _service.Add(forename, surname, "WrongPassword");

            // Assert
            Assert.AreEqual(0, result.Id);
			Assert.AreNotEqual("", result.Message);
		}
        #endregion
    }
}
