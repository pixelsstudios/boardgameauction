﻿using BoardGameAuction.API;
using BoardGameAuction.Models;
using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.API.Services;
using BoardGameAuction.API.Services.Interfaces;
using Microsoft.Extensions.Options;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoardGameAuction.Models.Types;

namespace UnitTests.BoardGameAuction.API.ServiceTests
{
    class ItemServiceTests
    {
        private IOptions<AppSettings> _configuration;
        private IItemRepository _repository;
        private IExpansionService _expansionService;
        private IItemService _service;
        private IAuctionService _auctionService;

        [SetUp]
        public void Setup()
        {
            _configuration = Substitute.For<IOptions<AppSettings>>();
            _repository = Substitute.For<IItemRepository>();
            _expansionService = Substitute.For<IExpansionService>();
            _auctionService = Substitute.For<IAuctionService>();
            _service = new ItemService(_configuration, _repository, _expansionService, _auctionService);
        }

        #region Add
        [Test]
        public void Given_Adding_Then_ShouldThrowIfAlreadyExists()
        {
            // Arrange
            var theItem = new Item
            {
                LotId = 4,
                BGGId = 123,
                Expansions = new List<Expansion>
                {
                    new Expansion { BGGId = 100 },
                    new Expansion { BGGId = 200 }
                }
            };
            _auctionService.Get(Arg.Any<int>(), Arg.Any<RoleType>(), Arg.Any<int>()).Returns(new Auction { Start = DateTime.UtcNow.AddDays(1), End = DateTime.UtcNow.AddDays(2) });
            _repository.Add(Arg.Any<Item>()).Returns(theItem);

            // Act / Assert
            Assert.ThrowsAsync<ArgumentException>(async () => await _service.Add(theItem, 10));
        }

        [Test]
        [TestCase(0, 0)]
        [TestCase(0, 1)]
        [TestCase(1, 0)]
        public void Given_Adding_Then_ShouldErrorIfMissingIds(int lotId, int bggId)
        {
            // Act / Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await _service.Add(new Item() { LotId = lotId, BGGId = bggId }, 10));
        }

        [Test]
        [TestCase(0)]
        [TestCase(-5)]
        public void Given_Adding_Then_ShouldThrowIfAuctionStarted(int offsetFromNow)
        {
            // Arrange
            const int auctionId = 123;
            const int lotId = 456;
            var theItem = new Item { LotId = lotId, BGGId = 112233 };
            var theLot = new Lot { AuctionId = auctionId };
            var theAuction = new Auction { Id = auctionId, Start = DateTime.UtcNow.AddMinutes(offsetFromNow), End = DateTime.UtcNow.AddDays(1) };
            _auctionService.Get(auctionId, Arg.Any<RoleType>(), Arg.Any<int>()).Returns(theAuction);

            // Act / Assert
            Assert.ThrowsAsync<Exception>(async () => await _service.Add(theItem, auctionId));
        }

        [Test]
        public async Task Given_Adding_Then_ShouldAdd()
        {
            // Arrange
            _auctionService.Get(Arg.Any<int>(), Arg.Any<RoleType>(), Arg.Any<int>()).Returns(new Auction { Start = DateTime.UtcNow.AddDays(1), End = DateTime.UtcNow.AddDays(2) });
            _repository.Add(Arg.Any<Item>()).Returns(new Item { Id = 2, Expansions = new List<Expansion> { new Expansion() } });

            // Act
            await _service.Add(new Item { LotId = 123, BGGId = 456 }, 10);

            // Assert
            await _repository.Received(1).Add(Arg.Is<Item>(x => x.LotId == 123 && x.BGGId == 456));
        }

        [Test]
        public async Task Given_Adding_Then_ShouldAddAllChildItemsToo()
        {
            // Arrange
            int itemId = 5;
            var theItem = new Item
            {
                LotId = 4,
                BGGId = 123,
                Expansions = new List<Expansion>
                {
                    new Expansion { BGGId = 100 },
                    new Expansion { BGGId = 200 }
                }
            };
            _auctionService.Get(Arg.Any<int>(), Arg.Any<RoleType>(), Arg.Any<int>()).Returns(new Auction { Start = DateTime.UtcNow.AddDays(1), End = DateTime.UtcNow.AddDays(2) });
            _repository.Add(Arg.Any<Item>()).Returns(new Item { Id = itemId });

            // Act
            await _service.Add(theItem, 10);

            // Assert
            await _expansionService.Received(2).Add(Arg.Is<Expansion>(x => x.ItemId == itemId));
        }
        #endregion

        #region Edit
        [Test]
        [TestCase(0, 0, 0)]
        [TestCase(0, 1, 0)]
        [TestCase(0, 1, 1)]
        [TestCase(0, 0, 1)]
        [TestCase(1, 0, 1)]
        [TestCase(1, 0, 0)]
        [TestCase(1, 1, 0)]
        public void Given_Editing_Then_ShouldErrorIfMissingIds(int itemId, int lotId, int bggId)
        {
            // Act / Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await _service.Edit(new Item() { Id = itemId, LotId = lotId, BGGId = bggId }, 100));
        }

        [Test]
        [TestCase(0)]
        [TestCase(-5)]
        public void Given_Editing_Then_ShouldThrowIfAuctionStarted(int offsetFromNow)
        {
            // Arrange
            const int auctionId = 123;
            const int lotId = 456;
            var theItem = new Item { Id = 1, LotId = lotId, BGGId = 112233 };
            var theLot = new Lot { AuctionId = auctionId };
            var theAuction = new Auction { Id = auctionId, Start = DateTime.UtcNow.AddMinutes(offsetFromNow), End = DateTime.UtcNow.AddDays(1) };
            _auctionService.Get(auctionId, Arg.Any<RoleType>(), Arg.Any<int>()).Returns(theAuction);

            // Act / Assert
            Assert.ThrowsAsync<Exception>(async () => await _service.Edit(theItem, auctionId));
        }

        [Test]
        public async Task Given_Editing_Then_ShouldEdit()
        {
            // Arrange
            const int itemId = 3;
            const int lotId = 6;
            const int bggId = 20;
            _auctionService.Get(Arg.Any<int>(), Arg.Any<RoleType>(), Arg.Any<int>()).Returns(new Auction { Start = DateTime.UtcNow.AddDays(1), End = DateTime.UtcNow.AddDays(2) });

            // Act
            await _service.Edit(new Item() { Id = itemId, LotId = lotId, BGGId = bggId }, 100);

            // Assert
            await _repository.Received(1).Edit(Arg.Is<Item>(x => x.Id == itemId && x.LotId == lotId && x.BGGId == bggId));
        }
        #endregion

        #region Get
        [Test]
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 1)]
        public async Task Given_Getting_Then_ShouldShortcutRepoIfMissingId(int itemId, int callsExpected)
        {
            // Act 
            await _service.Get(itemId);

            // Assert
            await _repository.Received(callsExpected).Get(Arg.Any<int>());
        }

        [Test]
        public async Task Given_Getting_Then_ShouldGet()
        {
            // Arrange
            const int ItemId = 3;

            // Act
            await _service.Get(ItemId);

            // Assert
            await _repository.Received(1).Get(Arg.Is(ItemId));
        }
        #endregion

        #region GetAll
        [Test]
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 1)]
        public async Task Given_GettingAll_Then_ShouldShortcutRepoIfMissingId(int auctionId, int callsExpected)
        {
            // Act 
            await _service.GetAll(auctionId);

            // Assert
            await _repository.Received(callsExpected).GetAll(Arg.Is(auctionId));
        }

        [Test]
        public async Task Given_GettingAll_Then_ShouldGetAll()
        {
            // Arrange
            const int auctionId = 6;

            // Act
            await _service.GetAll(auctionId);

            // Assert
            await _repository.Received(1).GetAll(Arg.Is(auctionId));
        }
        #endregion
    }
}
