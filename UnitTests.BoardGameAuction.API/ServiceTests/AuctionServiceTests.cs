using BoardGameAuction.API;
using BoardGameAuction.Models;
using BoardGameAuction.API.Repositories.Interfaces;
using BoardGameAuction.API.Services;
using Microsoft.Extensions.Options;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoardGameAuction.Models.Types;
using BoardGameAuction.API.Services.Interfaces;

namespace UnitTests.BoardGameAuction.API.ServiceTests
{
    public class AuctionServiceTests
    {
        private IOptions<AppSettings> _configuration;
        private IAuctionRepository _repository;
        private IPasswordService _passwordService;
        private AuctionService _service;

        [SetUp]
        public void Setup()
        {
            _configuration = Substitute.For<IOptions<AppSettings>>();
            _repository = Substitute.For<IAuctionRepository>();
            _passwordService = Substitute.For<IPasswordService>();
            _service = new AuctionService(_configuration, _repository, _passwordService);
        }

        #region Add
        [Test]
        public async Task Given_GettingACode_Then_ShouldRetryIfAlreadyExists()
        {
            // Arrange
            const string password = "myPassword";
            _repository.GetByCode(Arg.Any<string>()).Returns(
                new Auction { Code = "XONE" },
                new Auction { Code = "XTWO" },
                null
            );
            _repository.Add(Arg.Any<Auction>()).Returns(new Auction { Id = 123 });
            _passwordService.HashPassword(Arg.Any<string>(), password).Returns("hash");

            // Act
            var result = _service.Add(password).Result;

            // Assert
            await _repository.Received(3).GetByCode(Arg.Any<string>());
        }

        [Test]
        public async Task Given_GettingACodeReturnsResults10Times_Then_ShouldError()
        {
            // Arrange
            _repository.GetByCode(Arg.Any<string>()).Returns(new Auction());

            // Act

            // Assert
            Assert.ThrowsAsync<Exception>(async () => await _service.Add("myPassword"));
            await _repository.Received(10).GetByCode(Arg.Any<string>());
        }
        #endregion

        #region Get
        [Test]
        [TestCase(RoleType.AuctionAdmin, 0)]
        [TestCase(RoleType.SiteAdmin, 0)]
        [TestCase(RoleType.Seller, 1)]
        [TestCase(RoleType.Attendee, 1)]
        public void Given_GettingAuction_Then_ShouldOnlyShowAdminDataToAdmins(RoleType role, int expectedCalls)
        {
            // Arrange
            const int sellerId = 5;
            var auction = Substitute.For<Auction>();
            _repository.Get(Arg.Any<int>()).Returns(auction);

            // Act
            var result = _service.Get(1, role, sellerId).Result;

            // Assert
            auction.Received(expectedCalls).ForNonAdmins(sellerId);
        }

        [Test]
        [TestCase(55, 55, 1)]
        [TestCase(666, 55, 2)]
        public void Given_SellerGettingAuction_Then_ShouldOnlyShowAdminDataForTheirLots(int nameId, int sellerId, int expectedCalls)
        {
            // Arrange
            var auction = Substitute.For<Auction>();
            const int auctionId = 1;
            auction.Id = auctionId;
            auction.Lots = new List<Lot>
            {
                new Lot
                {
                    AuctionId = auctionId,
                    Id = 10,
                    Seller = new Person { Id = sellerId },
                    Items = new List<Item> { new Item { Name = "ABC" } }
                },
                new Lot
                {
                    AuctionId = auctionId,
                    Id = 20,
                    Seller = new Person { Id = sellerId + 1 },
                    Items = new List<Item> { new Item { Name = "ABC" } }
                }
            };
            _repository.Get(auctionId).Returns(auction);

            // Act
            var result = _service.Get(auctionId, RoleType.Seller, nameId).Result;

            // Assert
            auction.Received(expectedCalls).ForNonAdmins(sellerId);
        }

        [Test]
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 1)]
        public async Task Given_Getting_Then_ShouldShortcutRepoIfMissingId(int auctionId, int callsExpected)
        {
            // Arrange
            var auction = Substitute.For<Auction>();
            _repository.Get(Arg.Any<int>()).Returns(auction);

            // Act 
            await _service.Get(auctionId, RoleType.AuctionAdmin, 0);

            // Assert
            await _repository.Received(callsExpected).Get(Arg.Any<int>());
        }

        [Test]
        public async Task Given_Getting_Then_ShouldBeOrderedByFirstItemsName()
        {
            // Arrange
            const int auctionId = 1;
            var auction = new Auction
            {
                Lots = new List<Lot> {
                    new Lot { Items = new List<Item> { new Item { Name = "CCC" }, new Item { Name = "AAA" } } },
                    new Lot { Items = new List<Item> { new Item { Name = "BBB" } } },
                    new Lot { Items = new List<Item> { new Item { Name = "DDD" } } },
                    new Lot { Items = new List<Item> { new Item { Name = "AAA" } } }
                }
            };
            _repository.Get(auctionId).Returns(auction);

            // Act
            var result = await _service.Get(auctionId, RoleType.AuctionAdmin, 0);

            // Assert
            Assert.AreEqual("AAA", result.Lots[0].Items[0].Name);
            Assert.AreEqual("BBB", result.Lots[1].Items[0].Name);
            Assert.AreEqual("CCC", result.Lots[2].Items[0].Name);
            Assert.AreEqual("DDD", result.Lots[3].Items[0].Name);
        }

        [Test]
        public async Task Given_Getting_Then_ShouldBeReturnListIfEmptyLotsIncluded()
        {
            // Arrange
            const int auctionId = 1;
            var auction = new Auction
            {
                Lots = new List<Lot> {
                    new Lot { Items = new List<Item>() },
                    new Lot { Items = new List<Item> { new Item { Name = "AAA" } } }
                }
            };
            _repository.Get(auctionId).Returns(auction);

            // Act
            var result = await _service.Get(auctionId, RoleType.AuctionAdmin, 0);

            // Assert
            Assert.AreEqual("AAA", result.Lots[0].Items[0].Name);
            Assert.IsEmpty(result.Lots[1].Items);
        }

        [Test]
        [TestCase("AAA", "AAA")]
        [TestCase("CCC", "BBB")]
        [TestCase(null, "BBB")]
        public async Task Given_Getting_Then_ShouldBeOrderedByFirstItemsName(string name, string expectedFirstName)
        {
            // Arrange
            const int auctionId = 1;
            var auction = new Auction
            {
                Lots = new List<Lot> {
                    new Lot { Items = new List<Item> { new Item { Name = "BBB" } } },
                    new Lot { Items = new List<Item> { new Item { Name = name } } },
                }
            };
            _repository.Get(auctionId).Returns(auction);

            // Act
            var result = await _service.Get(auctionId, RoleType.AuctionAdmin, 0);

            // Assert
            Assert.AreEqual(expectedFirstName, result.Lots[0].Items[0].Name);
        }

        [Test]
        [TestCase(1, ExpectedResult = 3)]
        [TestCase(2, ExpectedResult = 1)]
        [TestCase(3, ExpectedResult = 0)]
        [TestCase(0, ExpectedResult = 0)]
        public async Task<int> Given_GettingAuctionBeforeItStarts_Then_ShouldOnlySeeOwnItems(int nameId)
        {
            // Arrange
            const int auctionId = 1234;
            var auction = new Auction { 
                Id = auctionId, 
                Start = DateTime.UtcNow.AddDays(1), 
                End = DateTime.UtcNow.AddDays(2),
                Lots = new List<Lot>
                {
                    new Lot { AuctionId = auctionId, Seller = new Person { Id = 1 }},
                    new Lot { AuctionId = auctionId, Seller = new Person { Id = 1 }},
                    new Lot { AuctionId = auctionId, Seller = new Person { Id = 1 }},
                    new Lot { AuctionId = auctionId, Seller = new Person { Id = 2 }},
                }
            };
            _repository.Get(auctionId).Returns(auction);

            // Act
            var result = await _service.Get(auctionId, RoleType.Seller, nameId);

            // Assert
            return result.Lots.Count;
        }
        #endregion

        #region GetAll
        [Test]
        [TestCase(RoleType.AuctionAdmin, 0)]
        [TestCase(RoleType.SiteAdmin, 0)]
        [TestCase(RoleType.Seller, 1)]
        [TestCase(RoleType.Attendee, 1)]
        [Ignore("Tests pass even if expectedCalls are changed")]
        public void Given_GettingAuctionList_Then_ShouldOnlyShowAdminDataToAdmins(RoleType role, int expectedCalls)
        {
            // Arrange
            var auctions = Substitute.For<List<Auction>>();
            const int sellerId = 4;
            _repository.GetAll().Returns(auctions);

            // Act
            var result = _service.GetAll(role, sellerId).Result;

            // Assert
            auctions.Received(expectedCalls).Select(x => x.ForNonAdmins(sellerId));
        }
        #endregion
    }
}