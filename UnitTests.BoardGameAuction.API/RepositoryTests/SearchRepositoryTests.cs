﻿using BoardGameAuction.API;
using BoardGameAuction.API.Repositories;
using Microsoft.Extensions.Options;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace UnitTests.BoardGameAuction.API.RepositoryTests
{
    class SearchRepositoryTests
	{
        private IOptions<AppSettings> _configuration;
        private HttpClient _httpClient;
        private SearchRepository _repo;

        [SetUp]
        public void Setup()
        {
            _configuration = Options.Create(new AppSettings
            {
                BGGItemUrl = "http://www.somewhere.com?search={0}",
                BGGSearchUrl = "http://www.somewhere.com?query={0}"
            });
            _httpClient = Substitute.For<HttpClient>();
            _repo = new SearchRepository(_httpClient, _configuration);
        }

        private static Task<HttpResponseMessage> OK(string message)
        {
            return Task.FromResult(new HttpResponseMessage
            {
                Content = new StringContent(message),
                StatusCode = HttpStatusCode.OK
            });
        }

        [Test]
        public async Task Given_ResultContainsExpansionWithNoVersions_Then_Should_CreateVersionFromProduct()
        {
            // Arrange
            const int gameId = 3699;
            const int expansionWithVersionsId = 146466;
            const int expansionWithoutVersionsId = 23215;
            var gameXml = @$"
                <items>
                    <item type=""boardgame"" id=""{gameId}"">
                        <thumbnail>https://picdump.com/{gameId}.jpg</thumbnail>
                        <name type=""primary"" sortindex=""1"" value=""Killer Bunnies and the Quest for the Magic Carrot""/>
                        <yearpublished value=""2002""/>
                        <minplayers value = ""2"" />
                        <maxplayers value = ""8"" />
                        <link type=""boardgameexpansion"" id=""{expansionWithVersionsId}"" value=""Killer Bunnies and the Quest for the Magic Carrot Fan Deck""/>
                        <link type=""boardgameexpansion"" id=""{expansionWithoutVersionsId}"" value=""Killer Bunnies Bunny Blanks #1""/>
                        <versions>
                            <item type=""boardgameversion"" id=""79312"">
                                <thumbnail>https://picdump.com/79312.jpg</thumbnail>
                                <name type=""primary"" sortindex=""1"" value=""French edition""/>
                                <yearpublished value=""2010""/>
                            </item>
                        </versions>
                        <statistics page=""1"">
                            <ratings>
                                <average value = ""5.48765""/>
                                <averageweight value=""1.8817""/>
                            </ratings>
                        </statistics>
                    </item>
                </items>";
            var expansionsXml = @$"
                <items>
                    <item type=""boardgame"" id=""{expansionWithVersionsId}"">
                        <thumbnail>https://picdump.com/{expansionWithVersionsId}.jpg</thumbnail>
                        <name type=""primary"" sortindex=""1"" value=""Killer Bunnies and the Quest for the Magic Carrot Fan Deck""/>
                        <yearpublished value=""2013""/>
                        <versions>
                            <item type=""boardgameversion"" id=""216574"">
                                <thumbnail>https://picdump.com/216574.jpg</thumbnail>
                                <name type=""primary"" sortindex=""1"" value=""First edition""/>
                                <yearpublished value=""2013""/>
                            </item>
                        </versions>
                        <statistics page=""1"">
                            <ratings>
                                <average value = ""7.3""/>
                                <averageweight value=""2""/>
                            </ratings>
                        </statistics>
                    </item>
                    <item type=""boardgame"" id=""{expansionWithoutVersionsId}"">
                        <thumbnail>https://picdump.com/{expansionWithoutVersionsId}.jpg</thumbnail>
                        <name type=""primary"" sortindex=""1"" value=""Killer Bunnies Bunny Blanks #1""/>
                        <yearpublished value=""2006""/>
                        <versions/>
                        <statistics page=""1"">
                            <ratings>
                                <average value = ""6.42364""/>
                                <averageweight value=""2.6667""/>
                            </ratings>
                        </statistics>
                    </item>
                </items>";

            var messageHandler = new MockHttpMessageHandler(new Uri(string.Format(_configuration.Value.BGGItemUrl, gameId)), HttpStatusCode.OK, gameXml);
            messageHandler.Returns(new Uri(string.Format(_configuration.Value.BGGItemUrl, $"{expansionWithVersionsId},{expansionWithoutVersionsId}")), HttpStatusCode.OK, expansionsXml);
            var httpClient = new HttpClient(messageHandler);
            var repo = new SearchRepository(httpClient, _configuration);

            // Act
            var result = await repo.GetByBGGId(new int[] { gameId });

            // Assert
            Assert.AreEqual(1, result.Items[0].Expansions[1].Versions.Count);
        }

        [Test]
        public async Task Given_SearchIsNumbers_Then_ShouldFilterResultsBecauseOfMatchingId()
        {
            // Arrange
            const int queryId = 2000;
            var resultXml = @$"
                <items total=""5"" termsofuse=""https://boardgamegeek.com/xmlapi/termsofuse"">
                    <item type=""boardgame"" id=""{queryId}"">
                        <name type=""primary"" value=""True or False""/>
                        <yearpublished value=""1994""/>
                    </item>
                    <item type=""boardgame"" id=""193534"">
                        <name type=""primary"" value=""20.000 Leghe Sotto i Mari""/>
                        <yearpublished value=""1955""/>
                    </item>
                    <item type=""boardgame"" id=""5985"">
                        <name type=""alternate"" value=""2000 AD Character Meeples""/>
                    </item>
                    <item type=""boardgame"" id=""33012"">
                        <name type=""primary"" value=""The 2000 Guineas""/>
                        <yearpublished value=""1947""/>
                    </item>
                    <item type=""boardgame"" id=""45844"">
                        <name type=""primary"" value=""2000 Jaar Later...""/>
                        <yearpublished value=""1993""/>
                    </item>
                </items>";

            var messageHandler = new MockHttpMessageHandler(new Uri(string.Format(_configuration.Value.BGGSearchUrl, queryId.ToString())), HttpStatusCode.OK, resultXml);
            messageHandler.Returns(new Uri(string.Format(_configuration.Value.BGGSearchUrl, queryId.ToString())), HttpStatusCode.OK, resultXml);
            var httpClient = new HttpClient(messageHandler);
            var repo = new SearchRepository(httpClient, _configuration);

            // Act
            var result = await repo.GetByName(queryId.ToString());

            // Assert
            Assert.AreEqual(4, result.Items.Count);
        }
    }
}
