﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace UnitTests.BoardGameAuction.API.RepositoryTests
{
    public class MockHttpMessageHandler : HttpMessageHandler
    {
        private List<(Uri Url, HttpStatusCode StatusCode, string Response)> _responses = new List<(Uri Url, HttpStatusCode StatusCode, string Response)>();

        public string Input { get; private set; }
        public int NumberOfCalls { get; private set; }

        public MockHttpMessageHandler(Uri url, HttpStatusCode statusCode, string response)
        {
            _responses.Add((url, statusCode, response));
        }

        public void Returns(Uri url, HttpStatusCode statusCode, string response)
        {
            // Allow multiple responses based on Uri
            _responses.Add((url, statusCode, response));
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            NumberOfCalls++;
            if (request.Content != null) // Could be a GET-request without a body
            {
                Input = await request.Content.ReadAsStringAsync();
            }
            return new HttpResponseMessage
            {
                StatusCode = _responses.Find(t => t.Url == request.RequestUri).StatusCode,
                Content = new StringContent(_responses.Find(t => t.Url == request.RequestUri).Response)
            };
        }
    }
}